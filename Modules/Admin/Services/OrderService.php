<?php

namespace Modules\Admin\Services;

use Illuminate\Http\Request;
use Modules\Admin\Repository\OrdersRepository;
use Auth;
use Modules\Admin\Entities\Product;

class OrderService {
	
    protected  $orderRepo;

    public function __construct()
    {
        $this->orderRepo =new OrdersRepository;
    }

    /**
        * tO GET all product in service function.
        *
        * @param  
        * @return product collection
    */
    public function listOrder()
    {   
        return $this->orderRepo->listOrder();
    }
    public function showDetails($id)
    {   
        return $this->orderRepo->showDetails($id);
    }
    public function updateDeliveryDate($formData)
    {   
        return $this->orderRepo->updateDeliveryDate($formData);
    }
    public function updateDeliveryStatus ($formData)
    {   
        return $this->orderRepo->updateDeliveryStatus ($formData);
    }
}