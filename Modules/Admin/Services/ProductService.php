<?php

namespace Modules\Admin\Services;

use Illuminate\Http\Request;
use Modules\Admin\Repository\ProductRepository;
use Auth;
use Modules\Admin\Entities\Product;

class ProductService {
	
    protected  $productRepo;

    public function __construct()
    {
        $this->productRepo =new ProductRepository;
    }

     /**
     * tO GET all product in service function.
     *
     * @param  
     * @return product collection
     */
    public function allProduct($id)
    {   
        $Id = base64_decode($id);
        return $this->productRepo->allProduct($Id);
    }  

    /**
     * Create a saveProduct service function.
     *
     * @param  $request
     * @return 
     */
    public function saveProduct($request)
    {   
        return $this->productRepo->saveProduct($request);
    }  
      /**
     * update a product .
     *
     * @param  $request
     * @return 
     */
    public function updateProduct($request,$id,$lang)
    {   
        return $this->productRepo->updateProduct($request,$id,$lang);
    } 


     /**
     * Delete a product .
     *
     * @param  $request
     * @return 
     */
    public function deleteProduct($id)
    {   
        return $this->productRepo->deleteProduct($id);
    } 

    /**
     * show a product .
     *
     * @param  $id
     * @return 
     */
    public function productDetail($id)
    {   
        $pid = base64_decode($id);
        return $this->productRepo->productDetail($pid);
    } 
    /**
     * show a product based on category id .
     *
     * @param  $id
     * @return 
     */
    public function getProductByCategory($cat_id)
    {  
       
        $category_id = base64_decode($cat_id);
        return $this->productRepo->getProductByCategory($category_id);
    } 

    public function hotproduct($productId,$statusid)
    {
         return $this->productRepo->hotproduct($productId,$statusid);
    }

    public function getHotProducts()
    {
        return $this->productRepo->getHotProducts();
    }

      /**
     * To remove attachment in database.
     * @param  request
     * @return Response
     */
    public function removeAttachment($request)
    {
        return $this->productRepo->removeAttachment($request->all());
    }
}
