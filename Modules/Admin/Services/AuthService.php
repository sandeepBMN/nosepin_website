<?php

namespace Modules\Admin\Services;

use Illuminate\Http\Request;
use Modules\Admin\Repository\UserRepository;
use Auth;
use Modules\Admin\Entities\AdminUser;

class AuthService {
	
    protected  $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Create a login service function.
     *
     * @param  $request
     * @return 
     */
    public function login($request)
    {
       
        return $this->userRepo->login($request);
    } 
}