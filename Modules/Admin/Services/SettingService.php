<?php

namespace Modules\Admin\Services;

use Illuminate\Http\Request;
use Modules\Admin\Repository\SettingRepository;
use Auth;
use Modules\Admin\Entities\Setting;

class SettingService {
	
    protected  $settingRepo;

    public function __construct(SettingRepository $settingRepo)
    {
        $this->settingRepo = $settingRepo;
    }

   /**
     * Create a goldprice service function.
     *
     * @param  $request
     * @return 
     */
    public function saveGoldPrice($request)
    {   
        return $this->settingRepo->saveGoldPrice($request->all());
    }
     /**
     * Create a goldprice service function.
     *
     * @param  $request
     * @return 
     */
    public function saveGst($request)
    {   
        return $this->settingRepo->saveGst($request->all());
    }

     /**
     * To  get a goldprice service function.
     *
     * @param  $request
     * @return 
     */
    public static function getGoldPrice()
    {   
        $goldprice=  SettingRepository::getGoldPrice();
        return $goldprice;
    }  

}