<?php

namespace Modules\Admin\Services;

use Illuminate\Http\Request;
use Modules\Admin\Repository\CategoryRepository;
use Auth;
use Modules\Admin\Entities\Category;

class CategoryService {
	
    protected  $catRepo;

    public function __construct()
    {
        $this->catRepo =new CategoryRepository;
    }

    /**
     * To get all Category in  service function.
     *
     * @param  $request
     * @return 
     */
    public function allCategory()
    {
        return $this->catRepo->allCategory();
    }
     /**
     * To get parent Category in  service function where parentid = 0.
     *
     * @param  $request
     * @return 
     */
    public function parentCategory()
    {
        return $this->catRepo->parentCategory();
    }

    /**
     * Create a saveCategory service function.
     *
     * @param  $request
     * @return 
     */
    public function saveCategory($request)
    {   
        return $this->catRepo->saveCategory($request);
    }  
    /**
     * update a state .
     *
     * @param  $request
     * @return 
     */
    public function updateCategory($request,$id,$lang)
    {   
        return $this->catRepo->updateCategory($request,$id,$lang);
    } 

     /**
     * Delete a state .
     *
     * @param  $request
     * @return 
     */
    public function deleteCategory($id)
    {   
        return $this->catRepo->deleteCategory($id);
    } 

     /**
     * To get all subCategory based on id.
     *
     * @param  $id category id
     * @return 
     */
    public function getSubCategory($id)
    {
        return $this->catRepo->getSubCategory($id);
    }

}