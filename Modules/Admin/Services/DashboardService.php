<?php

namespace Modules\Admin\Services;

use Illuminate\Http\Request;
use Modules\Admin\Repository\DashboardRepository;
use Auth;
use Modules\Admin\Entities\GoldPrice;

class DashboardService {
	
     protected  $dashboardRepo;

    public function __construct(DashboardRepository $dashboardRepo)
    {
        $this->dashboardRepo = $dashboardRepo;
    }


   /**
     * Create a goldprice service function.
     *
     * @param  $request
     * @return 
     */
    public function saveGoldPrice($request)
    {   
        return $this->dashboardRepo->saveGoldPrice($request->all());
    }  

     
}