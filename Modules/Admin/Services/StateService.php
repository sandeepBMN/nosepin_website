<?php

namespace Modules\Admin\Services;

use Illuminate\Http\Request;
use Modules\Admin\Repository\StateRepository;
use Auth;
use Modules\Admin\Entities\AdminUser;

class StateService {
	
    protected  $stateRepo;

    public function __construct(StateRepository $stateRepo)
    {
        $this->stateRepo = $stateRepo;
    }

     /**
     * to get all state service function.
     *
     * @param  
     * @return state collection
     */
    public function allState()
    {   
        return $this->stateRepo->allState();
    }  

    /**
     * Create a savestate service function.
     *
     * @param  $request
     * @return 
     */
    public function saveState($request)
    {   
        return $this->stateRepo->saveState($request);
    }  
    /**
     * update a state .
     *
     * @param  $request
     * @return 
     */
    public function updateState($request,$id)
    {   
        return $this->stateRepo->updateState($request,$id);
    } 

     /**
     * Delete a state .
     *
     * @param  $request
     * @return 
     */
    public function deleteState($id)
    {   
        return $this->stateRepo->deleteState($id);
    } 
}