<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
	use SoftDeletes;
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'states';
    protected $dates = ['deleted_at'];
    protected $fillable = [
    		'name'
    ];
    public $timestamps = true;

    public function product(){
        return $this->belongsTo('Modules\Admin\Entities\Product','state_id','id');
    }
     public function language()
    {
        return $this->morphMany('Modules\Admin\Entities\Language', 'languageable');
    }
}
