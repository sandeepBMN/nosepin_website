<?php

namespace Modules\Admin\Entities;
use Modules\Admin\Entities\Attachment;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table = 'products';
    protected $fillable = [
        'name','description','labour','inc_dec_labour','weight','purity','discountPrice','attachment_id','state_id','category_id','
        hot_product'
    ];
    public $timestamps = true;

    public function category(){
    	return $this->belongsTo('Modules\Admin\Entities\Category');
    }
     public function state(){
    	return $this->belongsTo('Modules\Admin\Entities\State');
    }

     public function language()
    {
        return $this->morphMany('Modules\Admin\Entities\Language', 'languageable');
    }

     public function order()
    {
        return $this->hasMany('App\Models\Order','product_id','id');
    }
    public function attachments(){
        return $this->morphMany(Attachment::class,'attachable');
    }
}
