<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class GoldPrice extends Model
{
	protected $table = 'gold_price';
    protected $fillable = [
    	'gold_price'
    ];
    public $timestamps = true;

    
}
