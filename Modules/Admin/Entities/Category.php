<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Admin\Entities\Attachment;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name','parent_id','cat_image'
    ];
    public $timestamps = true;

     /**
     * accessor method to get the admin profile image
     *
     */
    public function getImageAttribute()
    {
        return $this->cat_image;
    }

     public function product(){
        return $this->belongsTo('Modules\Admin\Entities\Product','category_id','id');
    }

     public function childCategory()
    {
        return $this->hasMany('Modules\Admin\Entities\Category','parent_id','id');
    }
    public function language()
    {
        return $this->morphMany('Modules\Admin\Entities\Language', 'languageable');
    }
     public function attachments(){
        return $this->morphMany(Attachment::class,'attachable');
    }
}
