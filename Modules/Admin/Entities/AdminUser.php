<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class AdminUser extends Authenticatable
{
	 use Notifiable;

    protected $fillable = [
    	 'firstname','lastname','username', 'email', 'password','remember_token','profile_image'
    ];
     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * accessor method to get the admin profile image
     *
     */
    public function getImageAttribute()
    {
        return $this->profile_image;
    }
}
