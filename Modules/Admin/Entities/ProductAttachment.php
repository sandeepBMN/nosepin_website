<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductAttachment extends Model
{
	protected $table = 'product_attachments';
    protected $fillable = [
    	'name','path','mimeType','product_id'
    ];
    public $timestamps = false;

    public  function product()
    {
        return $this->belongsTo('Modules\Admin\Entities\Product','id','product_id');
    } 
}
