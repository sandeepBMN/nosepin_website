<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
	protected $table = 'languages';
    protected $fillable = [
    	'text','attachable_model','language','attachable_id','type'
    ];
    public $timestamps = true;


      public function languageable()
    {
        return $this->morphTo();
    }
}
