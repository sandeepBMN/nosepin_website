<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
   protected $fillable = ['image_name','storage_path','type','attachable_id','attachable_type'];
}
