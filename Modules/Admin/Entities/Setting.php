<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	protected $table = 'settings';
    protected $fillable = ['key','value'];
    public $timestamps = true;
}
