<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function() {
    Route::POST('login', 	['as' => 'login.create','uses' => 'Auth\LoginController@login']);
	Route::get('logout', 	['as' => 'admin.logout','uses' => 'Auth\LoginController@logout']);
	Route::get('login', 	['as' => 'admin.login',	'uses' => 'Auth\LoginController@adminLogin']);
	Route::get('/getsubcategories/{id}', 	['as' => 'admin.getsubcategories',	'uses' => 'CategoryController@showSubCategory']);
	Route::group(['middleware' => 'AuthAdmin'] , function(){
    	Route::get('/',['as' => 'admin.dashboard' , 'uses' => 'AdminController@index']);
    	Route::resource('state','StateController');
    	Route::resource('category','CategoryController')->only(['index','create','store']);
        Route::get('/category/edit/{id}/{lang}','CategoryController@edit')->name('category.edit');
        Route::put('/category/update/{id}/{lang}','CategoryController@update')->name('category.update');
        Route::get('category/delete/{id}','CategoryController@destroy')->name('category.destroy');
    	Route::resource('product','ProductController')->only(['index','store','update','destroy','show']);
        Route::get('product/create/{lang}/{id?}','ProductController@create')->name('product.create');
        Route::get('/product/edit/{id}/{lang}','ProductController@edit')->name('product.edit');
        Route::put('/product/update/{id}/{lang}','ProductController@update')->name('product.update');
    	Route::get('product/delete/{id}','ProductController@destroy')->name('product.destroy');    
        Route::get('/productattach/delete','ProductController@removeAttachment');
       
            // to create gold price
        Route::get('/create/goldprice','SettingController@goldpriceCreate')->name('goldprice.create');
        Route::post('goldprice','SettingController@saveGoldPrice')->name('goldprice.store');

              // to create gst price
        Route::get('/create/gst','SettingController@gstCreate')->name('gst.create');
        Route::post('gst','SettingController@saveGst')->name('gst.store');

        // hot item or not 
        Route::get('/hotproduct/{productid}/{status}','ProductController@hotproduct')->name('product.hotproduct');

        Route::get('/language/{type}','AdminController@setLanguage')->name('language.save');

        Route::resource('users','UsersController')->only(['index']);
        Route::get('orders' , ['as' => 'client-orders' , 'uses' => 'OrdersController@listOrder']);
        Route::get('orders/details/{id}' , ['as' => 'get-order-details' , 'uses' => 'OrdersController@showDetails']);
        Route::POST('update/delivery/date' , ['as' => 'update-delivery-date' , 'uses' => 'OrdersController@updateDeliveryDate']);
        Route::POST('update/delivery/status' , ['as' => 'update-delivery-status' , 'uses' => 'OrdersController@updateDeliveryStatus']);

	});
});
