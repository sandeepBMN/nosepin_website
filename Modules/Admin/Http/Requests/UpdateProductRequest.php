<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'name' => 'required|string|max:100',
            'labour'    => 'required|numeric|max:15',
            'inc_dec_labour'    => 'required',
            'weight' => 'required|numeric',
            'purity.*'  => 'required|numeric',
            'prod_image' => 'required',
            'prod_image.*' => 'mimes:jpeg,png,jpg,gif'
        ];
    }
}
