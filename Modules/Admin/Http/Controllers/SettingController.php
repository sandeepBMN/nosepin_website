<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Services\SettingService;
use Modules\Admin\Http\Requests\CreateGoldPriceRequest;
use Session;

class SettingController extends Controller
{

     public $settingService = '';
   
    /**
        * Create a new controller instance.
        *
        * @return void
    */

    public function __construct(SettingService $settingService)
    {
        $this->settingService = $settingService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    /**
     * Show the form for creating a new gold price.
     * @return Response
     */
    public function goldpriceCreate()
    {
        return view('admin::settings/creategoldprice');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
      public function saveGoldPrice(CreateGoldPriceRequest $request)
     {
        $validated = $request->validated();
        $saveprice = $this->settingService->saveGoldPrice($request);
        if($saveprice  == true)
        {
            Session::flash('alert-success','Gold Price saved successfully');
            return redirect()->route('goldprice.create');
        }else{
            return back()->withErrors('invalid input found');
        }
     }
      /**
     * Show the form for creating a new gst price.
     * @return Response
     */
    public function gstCreate()
    {
        return view('admin::settings/savegst');
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
      public function saveGst(Request $request)
     {
        $saveprice = $this->settingService->saveGst($request);
        if($saveprice  == true)
        {
            Session::flash('alert-success','GST saved successfully');
            return redirect()->route('gst.create');
        }else{
            return back()->withErrors('invalid input found');
        }
     }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
