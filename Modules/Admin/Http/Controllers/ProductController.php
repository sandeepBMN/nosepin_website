<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Routing\Controller;
use App\Http\Controllers\Controller;
use Modules\Admin\Entities\Category;
use Modules\Admin\Entities\Language;
use Modules\Admin\Entities\State;
use Modules\Admin\Entities\Product;
use Modules\Admin\Services\ProductService;
use Validator;
use Input;
use Modules\Admin\Http\Requests\CreateProductRequest;
use Modules\Admin\Http\Requests\UpdateProductRequest;
use Session;

class ProductController extends Controller
{

    public $prodService = '';
   

    /**
        * Create a new controller instance.
        *
        * @return void
    */
    public function __construct(ProductService $prodService)
    {
        $this->prodService = $prodService;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $product = $this->prodService->getProducts(); 
        return view('admin::product/index',compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($lang,$pid=null)
    {
        $state = State::all();
        $selectcat =  Category::where('parent_id',0)->get();
        if($pid != null)
        {
            $pid = base64_decode($pid);
        }
        return view('admin::product/create',compact('selectcat','state','pid','lang'));
    }

    /**
     * Store a new product.
     * @param Request $request
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $validated = $request->validated();

        $product = $this->prodService->saveProduct($request);

        if($product['status']  == true)
        {
            Session::flash('alert-success','Product saved successfully');
            if($product['btn'] =='save')
            {
                Session::forget('type');
                return redirect()->route('product.index');
            }elseif($product['btn'] =='addHindi')
            {

                Session::put('type','hn');
                 return redirect()->route('product.create',['lang'=>'hn','pid'=>base64_encode($product['data'])]);
            }elseif($product['btn'] =='savehindi')
            {
                Session::forget('type');
                 return redirect()->route('product.index');
            }else{
            return back()->withErrors($product['message']);
            }
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $product = $this->prodService->productDetail($id);
       

        return view('admin::product/show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id,$lang)
    {
        $productId = base64_decode($id);
        if($lang == 'en')
        {
            $product = Product::with(['attachments'=> function($query){ 
            $data = $query->where(['type' => 'original'])->get();
        }])->find($productId);
            if($product->purity != null)
            {
                $purity = json_decode($product->purity,true);

            }else
            {
                $purity =[];
            }
        }else{
            
            $product = Language::where('attachable_id',$productId)->where('attachable_model','Product')->get();
            if(!$product->isEmpty()){
                $getproduct = $product->toArray();
                 $product = array_column($getproduct, 'text', 'type');
                 $product['id'] = $productId;
            }else{
                return back()->withErrors('No hindi product found');
            }
        } 
        $state = State::all();
        $selectcat =  Category::where('parent_id',0)->get();
        return view('admin::product/edit',compact('product','state','selectcat','purity','lang'));
    }

    /**
     * Update the product.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id,$lang)
    {

        $input = Input::except('_token');
        if($lang == 'en')
        {
            $validator =$this->validate($request, [
               'name' => 'required|string|max:100',
                'labour'    => 'required|numeric|max:15',
                'inc_dec_labour'    => 'required',
                'weight' => 'required|numeric',
                'purity.*'  => 'required|numeric',
                'prod_image' => 'required',
                'prod_image.*' => 'mimes:jpeg,png,jpg,gif'
           ]);  
        }else{
             $validator = $this->validate($request,[
                'name'  => 'required|string|max:100',
                'description'    => 'required|max:1000',
            ]);
        }
        
       // if ($validator->fails()) {
       //      return redirect()->back()->withErrors($validator)->withInput();
       // }
        
          $pId = base64_decode($id);
          $updateproduct = $this->prodService->updateProduct($request,$pId,$lang);
        if($updateproduct['status'] == true)
        {
            Session::flash('alert-success','Product Update successfully');
            return redirect()->route('product.index');
        }else{
             return back()->withErrors($updateproduct['message']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $deleteProduct= $this->prodService->deleteProduct($id);
       if($deleteProduct == true)
       {
            // Session::flash('alert-success','Category Delete successfully');
            return redirect()->route('product.index');
       }
    }

     /**
     * To add hot product in database.
     * @param int $productId,$statusid
     * @return Response
     */

    public function hotproduct($productId,$statusid)
    {
         $hotProduct = $this->prodService->hotproduct($productId,$statusid);
         if($hotProduct['status'] == true)
        {
            Session::flash('alert-success',$hotProduct['message']);
            return redirect()->route('product.index');
        }else{
            return back()->withErrors($hotProduct['message']);
        }
    }

      /**
     * To remove attachment in database.
     * @param  request
     * @return Response
     */
    public function removeAttachment(Request $request)
    {
        $status = $this->prodService->removeAttachment($request);
        if($status == true)
        {
            return response()->json(['status' => true]);
        }
        else{
            return response()->json(['status' => false]);
        }
    }

}
