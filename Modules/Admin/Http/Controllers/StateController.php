<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Entities\State;
use Modules\Admin\Http\Requests\CreateStateRequest;
use Modules\Admin\Http\Requests\UpdateStateRequest;
use Modules\Admin\Services\StateService;
use Session;

class StateController extends Controller
{


    public $stateService = '';
   

    /**
        * Create a new controller instance.
        *
        * @return void
    */
    public function __construct(StateService $stateService)
    {
        $this->stateService = $stateService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $state = $this->stateService->allState();
        return view('admin::state/index', compact('state'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::state/create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CreateStateRequest $request)
    {
        $state = $this->stateService->saveState($request->all());
        if($state == true)
        {
            Session::flash('alert-success','State saved successfully');
            return redirect()->route('state.create');
        }

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $stateId = base64_decode($id);
        $state =   State::find($stateId);

        return view('admin::state/edit',compact('state'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateStateRequest $request, $id)
    {
        $sId = base64_decode($id);
        $updatestate = $this->stateService->updateState($request->all(),$sId);
        if($updatestate == true)
        {
            Session::flash('alert-success','State Update successfully');
            return redirect()->route('state.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $deleteState = $this->stateService->deleteState($id);
       if($deleteState == true)
       {
            Session::flash('alert-success','State Delete successfully');
            return redirect()->route('state.index');
       }
    }
}
