<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Entities\Category;
use Modules\Admin\Entities\Language;
use Modules\Admin\Services\CategoryService;
use Modules\Admin\Http\Requests\CreateCategoryRequest;
use Modules\Admin\Http\Requests\UpdateCategoryRequest;
use Session;

class CategoryController extends Controller
{

     public $catService = '';
   
    /**
        * Create a new controller instance.
        *
        * @return void
    */
    public function __construct(CategoryService $catService)
    {
        $this->catService = $catService;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $category = $this->catService->allCategory(); 
      
        return view('admin::category/index',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
       $category =  Category::where('parent_id',0)->get();
        return view('admin::category/create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $validated = $request->validated();
        $category = $this->catService->saveCategory($request);
        if($category['status']  == true)
        {
            Session::flash('alert-success','Category saved successfully');
            if($category['type'] == 'en')
            {
                $type = $category['type'];
                 return redirect()->route('category.create',compact('type'));
            }else{
                $type = $category['type'];
                 return redirect()->route('category.create',compact('type'));
            }
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id,$lang)
    {

        $catId = base64_decode($id);
       if($lang== 'en')
       {
        $category =   Category::find($catId);
       }else{
        $category = Language::where('attachable_id',$catId)->where('attachable_model','Category')->first();

       }
        $selectcat =  Category::where('parent_id',0)->get();
        return view('admin::category/edit',compact('category','selectcat','lang'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateCategoryRequest $request, $id,$lang)
    {
        $catId = base64_decode($id);

        $updatestate = $this->catService->updateCategory($request,$catId,$lang);
        if($updatestate['status'] == true)
        {
            Session::flash('alert-success','Category Update successfully');
            return redirect()->route('category.index');
        }else{
            return back()->withErrors($updatestate['message']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
         $deleteCategory= $this->catService->deleteCategory($id);
       if($deleteCategory == true)
       {
            Session::flash('alert-success','Category Delete successfully');
            return redirect()->route('category.index');
       }
    }

     /**
     * use ajax to show sub category if parent category exist.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function showSubCategory(Request $request,$id){

        $data =Category::where('parent_id',$id)->get();
        if($data->count() > 0)
        {
            return response()->json($data); 
        }

    }



}
