<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\User;
use Modules\Admin\Services\OrderService;

class OrdersController extends Controller
{
    protected $orderSer;
    public function __construct(OrderService $orderService)
    {
        $this->orderSer = $orderService;
    }
    public function listOrder()
    {
        $listOrder = $this->orderSer->listOrder();
        return view('admin::order.index' , compact('listOrder'));
    }
    public function showDetails($id)
    {
        $orderDetails = $this->orderSer->showDetails($id);
        return view('admin::order.show' , compact('orderDetails'));
    }   
    public function updateDeliveryDate(Request $request)
    {
        $orderDetails = $this->orderSer->updateDeliveryDate($request->except('_token'));
        return back();
    }
    public function updateDeliveryStatus (Request $request)
    {
        $orderDetails = $this->orderSer->updateDeliveryStatus ($request->except('_token'));
        return back();
    }
}