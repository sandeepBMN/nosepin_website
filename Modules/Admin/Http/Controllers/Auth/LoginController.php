<?php

namespace Modules\Admin\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Modules\Admin\Services\AuthService;
use Modules\Admin\Http\Requests\LoginRequest;
use Auth;

class LoginController extends Controller
{
    /*
	    |--------------------------------------------------------------------------
	    | Login Controller
	    |--------------------------------------------------------------------------
	    |
	    | This controller handles authenticating users for the application and
	    | redirecting them to your home screen. The controller uses a trait
	    | to conveniently provide its functionality to your applications.
	    |
    */

    use AuthenticatesUsers;

	/**
		* Where to redirect users after login.
		*
		* @var string
	*/
   	protected $service = '';
   	protected $redirectTo = '/';

	/**
		* Create a new controller instance.
		*
		* @return void
	*/
    public function __construct(AuthService $service)
    {
        // $this->middleware('AuthAdminCheck')->except('logout');
        $this->service = $service;
    }
	/**
		* Create a user login function
		*
		* @param  array $request
		* @return json response 
	*/
    public function adminLogin()
    {
    	return view('admin::auth.login');
    }
    public function login(LoginRequest $request)
    {
    	
		$user = $this->service->login($request->all());
		if( $user == true ){
			return redirect()->route('admin.dashboard');
		}else{
            return back()->withErrors(['error'=>'Wrong Admin Credentials.']);
		}
    }
    /**
		* Create a admin logout function
		*
		* @param  array $request
		* @return json response 
	*/
     public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
}
