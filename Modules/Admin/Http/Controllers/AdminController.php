<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Entities\Category;
use Modules\Admin\Services\CategoryService;
use Modules\Admin\Services\ProductService;
use Modules\Admin\Services\DashboardService;
use App\Services\PaymentService;
use Session;

class AdminController extends Controller
{
    
    public $catService = '';
    public $prodService = '';
   

    /**
        * Create a new controller instance.
        *
        * @return void
    */
    public function __construct(CategoryService $catService,ProductService $prodService)
    {
        $this->catService = $catService;
        $this->prodService = $prodService;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = [];
        $category = $this->catService->allCategory();
        $products = $this->prodService->allProduct($id=null);
        $goldprice = DashboardService::getGoldPrice();
        // $orderlist = PaymentService::listOrder($page=null);
        
        $data['category'] = $category;
        $data['products'] = $products;   
        if($goldprice != null)
        {
            $data['goldprice'] = $goldprice->gold_price;
        }else{
            $data['goldprice'] = 0;
        }
        return view('admin::dashboard/index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function setLanguage($type){

        if($type)
        {
           Session::put('language', $type);
            return back();
        }
    }
}
