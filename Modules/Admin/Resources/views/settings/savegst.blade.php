@extends('admin::layouts.master')

  @section('content')

  <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <div class="breadcrumbs-inner">
                    <div class="row m-0">
                        <div class="col-sm-4">
                            <div class="page-header float-left">
                                <div class="page-title">
                                    <h1>Gst</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="page-header float-right">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="#">Home</a></li>
                                        
                                        <li class="active">Gsy</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))

          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
    </div> 
    	  <!-- Animated -->
    <div class="animated fadeIn">
    	<div class="row">
    		<div class="col-md-12">
    			<div class="card">
                    <div class="card-header">
                        <strong>Save GST</strong>
                    </div>
                    <div class="card-body card-block">
                           
                      {!! Form::open(['route' => 'gst.store','class' => 'form-horizontal','method' => 'post']) !!}
                            <div class="row form-group">
                                <div class="col col-md-3">
                                	  {!! Form::label('gst', 'GST',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9 {{ $errors->has('gst') ? 'has-error' : ''}}">
                                           
                                    {!! Form::number('gst', old('gst'), ['class' => 'form-control','placeholder' => 'Enter Gst Price','id' => 'gst','min'=> '0']) !!}
                                    @if($errors->has('gst'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('gst') }}</strong>
                                        </span>
                                    @endif
                                   <input type="hidden" name="error" class="counterror" value="{{ count($errors)}}">
                                </div>
                            </div>
                          	<div class="row form-group">
                          		<div class="offset-3 col-md-9">
                                	<button type="submit" class="btn btn-primary save">Save</button>
                          		</div>
                          	</div>
                   		{!! Form::close() !!}
                    </div>
                           
                </div>
    		</div>
    	</div>
    </div>


  @endsection
  @section('addscript')
  <script type="text/javascript">
  	 @if (count($errors) > 0)
   
	    jQuery('button[type="submit"]').prop('disabled', true);
	     jQuery('#gst').keyup(function() {
	        if(jQuery(this).val() != '') {
	           jQuery('button[type="submit"]').prop('disabled', false);
	        }
	     });
    @endif
  </script>
  @endsection