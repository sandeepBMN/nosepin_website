@extends('admin::layouts.master')
	@section('addstyle')
		<link rel="stylesheet" href="{{asset('css/admin/assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/assets/css/sweetalert/sweetalert.css')}}">
    
	@endsection
@section('content')
  <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
      @endforeach
  </div> 
  <div class="animated fadeIn">
    <div class="row">
	    <div class="col-md-12">
	      <div class="card">
          <div class="card-header">
              <strong class="card-title">Categories List</strong>
          </div>
          <div class="card-body">
					  @if ($category->count())
              <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                  <tr>
                      <th>#</th>
                      <th></th>
                      <th>Name</th>
                      <th>Image</th>
                      <th>Action</th>
                     
                  </tr>
                </thead>
                    <tbody class="">

                  @foreach ($category as $row)
                      <tr class="">
                        <td>
                            <label data-toggle="collapse" data-target="#demo_{{ $row->id}}" class="accordion-toggle"> <i class="fa fa-plus"></i></label>
                        </td>
                        <td>{{$row->id}}</td>
                        <td>{{ $row->name }}</td>
                        @if(isset($row->attachments))
                            @if($row->attachments->count() > 0)
                              @foreach($row->attachments as $k=>$v)
                                @if($v['type'] == 'thumbs')
                                  <td><img src="{{  asset(str_replace("\\","/",$v['storage_path'].'\\'. $v['image_name'])) }}" width="50" alt="no-image"></td>
                                @endif
                              @endforeach
                            @else
                              <td><img src="{{  asset('assets/images/noimage.jpg') }}" width="50"></td>
                            @endif
                        @endif
                        <td style="display: inline-flex;" >
                          <a href="{{ route('category.edit',['id'=>base64_encode($row->id),'lang'=>'en']) }}" class="btn btn-primary btn-sm">Edit </a> ||
                          <a href="{{ route('category.edit',['id'=>base64_encode($row->id),'lang'=>'hn']) }}" class="btn btn-primary btn-sm">संपादित करें </a>  ||
                          <a href="{{route('category.destroy', $row->id) }}" data-toggle="tooltip" data-original-title="Delete" id="confirm_delete" class="confirm_delete btn btn-danger btn-sm">Delete</a>
                        </td>         
                      </tr>  
                      
                      
                     
          
                  @endforeach
                    </tbody>
                   
              </table>    
            @else
              <div class="text-center">
                <h3>No result Found</h3>
              </div>
            @endif
	                
          </div>
        </div>
      </div>
    </div>
  </div>

  @endsection
  @section('addscript')
   	<script src="{{asset('js/admin/assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/init/datatables-init.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/sweetalert/sweetalert.min.js')}}"></script>

    <script type="text/javascript">
        window.success = '{{ Session::get('success') }}';
        window.error = '{{ Session::get('error') }}';
    </script>
    <script src="{{asset('js/admin/assets/js/sweetalert/custom.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
      } );
      
$(document).ready(function() {
    $('.accordian-body').on('show.bs.collapse', function () {
    $(this).closest("table")
        .find(".collapse.in")
        .not(this)
        .collapse('toggle')
    });
});
  </script>


  @endsection