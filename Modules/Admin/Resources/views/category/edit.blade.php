@extends('admin::layouts.master')

  @section('content')
	 <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <div class="breadcrumbs-inner">
                    <div class="row m-0">
                        <div class="col-sm-4">
                            <div class="page-header float-left">
                                <div class="page-title">
                                    <h1>Edit Category</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="page-header float-right">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Category</a></li>
                                        
                                        <li class="active">Edit</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))

          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
    </div> 
    	  <!-- Animated -->
    <div class="animated fadeIn">
    	<div class="row">
    		<div class="col-md-12">
    			<div class="card">
                    <div class="card-header">
                        <strong>Edit Category</strong>
                    </div>
                    <div class="card-body card-block">
                        @if($lang == 'en')

                    	{!! Form::model($category,['route' => ['category.update',base64_encode($category->id),'en'],'class' => 'form-horizontal','method' => 'PUT','files' => true]) !!}
                             <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('category', 'Select category',['class'=> 'form-control-label']) !!}
                                    
                                </div>
                                <div class="col-12 col-md-9">
                                    <select name="main_cat" id="category" class=" form-control">
                                        <option value="">Please select</option>
                                        @foreach($selectcat as $v)
                                            <option value="{{ $v->id }}" {{ $category->parent_id == $v->id ? 'selected="selected"' : '' }} >{{ $v->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                	{!! Form::label('catName', 'Category Name',['class'=> 'form-control-label']) !!}
                                	<!-- <label for="stateName" class=" form-control-label">State Name</label> -->
                                </div>
                                <div class="col-12 col-md-9">
                                	<!-- <input type="text" id="stateName" name="name" placeholder="Enter State Name..." class="form-control"> -->
                                	{!! Form::text('name', old('name',(isset($category->name) ? $category->name :'')), ['class' => 'form-control','placeholder' => 'Enter State Name...','id' => 'catName']) !!}
                                	@if($errors->has('name'))
		                                <span class="text-danger" role="alert">
		                                    <strong>{{ $errors->first('name') }}</strong>
		                                </span>
		                            @endif
                                </div>
                            </div>
                            @if(isset($category->attachments))
                                    @if(!$category->attachments->isEmpty())
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                {!! Form::label('cat_image', 'Image',['class'=> 'form-control-label']) !!}
                                                
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <label class="btn-file btn btn-primary">Browse
                                                    <input type="file" id="edit_cat_image" name="cat_image" class="form-control-file" >
                                                </label>
                                               
                                                @if($errors->has('cat_image'))
                                                    <span class="text-danger" role="alert">
                                                        <strong>{{ $errors->first('cat_image') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                @endif

                            <div class="row append-img-edit mb-15">
                                <div class="offset-3 col-md-9 appended-div">

                                @if(isset($category->attachments))
                                    @if(!$category->attachments->isEmpty())
                                        @foreach($category->attachments as $k=>$v)
                                            @if($v['type'] == 'thumbs')
                                                <div id="preview-img-edit" class="preview-img-edit">
                                                    <img src="{{ asset(str_replace("\\","/",$v['storage_path'].'\\'. $v['image_name'])) }}" width="120">
                                                    <div class="overlay">
                                                        <a href="javascript:void(0)" class="category_delete_image" data-id="{{$category->id }}"><i class="fa fa-trash fa-2x icon "></i></a>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach     
                                    @endif
                                @endif
                                </div>
                            </div>
                          	<div class="row form-group">
                          		<div class="offset-3 col-md-9">
                                  
                          			<input type="submit" class="btn btn-primary" name="update" value="Update">
                                  
                                    <input type="button" class="btn btn-secondary" name="cancel" value="Cancel">
                          		</div>
                          	</div>
                   		{!! Form::close() !!}
                        @else
                            {!! Form::model($category,['route' => ['category.update',base64_encode($category->attachable_id),'hn'],'class' => 'form-horizontal','method' => 'PUT','files' => true]) !!}
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('editcatName', 'Category Name',['class'=> 'form-control-label']) !!}
                                   
                                </div>
                                <div class="col-12 col-md-9">
                                   
                                    {!! Form::text('name', old('name',(isset($category->text) ? $category->text :'')), ['class' => 'form-control','placeholder' => 'Enter State Name...','id' => 'editcatName']) !!}
                                    @if($errors->has('name'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="offset-3 col-md-9">
                                  
                                    <input type="submit" class="btn btn-primary" name="updatehindi" value="Update in Hindi">
                                  
                                    <input type="button" class="btn btn-secondary" name="cancel" value="Cancel">
                                </div>
                            </div>
                             {!! Form::close() !!}   
                        @endif
                    </div>
                           
                </div>
    		</div>
    	</div>
    </div>

  @endsection
  @section('addscript')
  <script type="text/javascript" src="{{ asset('js/admin/assets/js/category.js')}}"></script>
  <script type="text/javascript">
   jQuery(document).on('click','.category_delete_image',function(e){
       jQuery(this).parents('.append-img').remove();
       jQuery('#cat_image').val('');
    });
      
  </script>

  @endsection