@extends('admin::layouts.master')

  @section('content')
	 <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <div class="breadcrumbs-inner">
                    <div class="row m-0">
                        <div class="col-sm-4">
                            <div class="page-header float-left">
                                <div class="page-title">
                                    <h1>Category</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="page-header float-right">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="#">Home</a></li>
                                        
                                        <li class="active">Category</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))

          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
    </div> 
    	  <!-- Animated -->
    <div class="animated fadeIn">
    	<div class="row">
    		<div class="col-md-12">
    			<div class="card">
                    <div class="card-header">
                        <strong>Save Category</strong>
                    </div>
                    <div class="card-body card-block">
                           

                    	{!! Form::open(['route' => 'category.store','class' => 'form-horizontal','method' => 'post','autocomplete'=> 'off','files' => true]) !!}
                           
                                @if(\Session::get('type')!= 'hn')      
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            {!! Form::label('category', 'Select category',['class'=> 'form-control-label']) !!}
                                            
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <select name="main_cat" id="category" class=" form-control">
                                                <option value="">Please select</option>
                                                @foreach($category as $v)
                                                    <option value="{{ $v->id }}">{{ $v->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        {!! Form::label('categoryName', 'Category Name',['class'=> 'form-control-label']) !!}
                                        <!-- <label for="stateName" class=" form-control-label">State Name</label> -->
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <!-- <input type="text" id="stateName" name="name" placeholder="Enter State Name..." class="form-control"> -->
                                        {!! Form::text('name', old('name'), ['class' => 'form-control','placeholder' => 'Enter Category Name...','id' => 'categoryName']) !!}
                                        <input type="hidden" name="categoryid" value="{{ (\Session::get('catid')!= null) ? 
                                        \Session::get('catid') : '' }}">
                                        @if($errors->has('name'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('categoryName', 'Category Name',['class'=> 'form-control-label']) !!}
                                    <!-- <label for="stateName" class=" form-control-label">State Name</label> -->
                                </div>
                                <div class="col-12 col-md-9">
                                    <!-- <input type="text" id="stateName" name="name" placeholder="Enter State Name..." class="form-control"> -->
                                    {!! Form::text('name', old('name'), ['class' => 'form-control','placeholder' => 'Enter Category Name...','id' => 'categoryName']) !!}
                                    @if($errors->has('name'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('cat_image', 'Image',['class'=> 'form-control-label']) !!}
                                    
                                </div>
                                <div class="col-12 col-md-9">
                                    <label class="btn-file btn btn-primary">Browse
                                        <input type="file" id="cat_image" name="cat_image" class="form-control-file">
                                    </label>
                                   
                                    @if($errors->has('cat_image'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('cat_image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row append-img mb-15" style="display: none;">
                                <div class="offset-3 col-md-9">
                                    <div id="preview-img">
                                        <img src="" width="120">
                                    </div>
                                </div>
                            </div>
                          	<div class="row form-group">
                          		<div class="offset-3 col-md-9">
                                    <input type="submit" class="btn btn-primary" name="save" value="Save">
                                    <input type="submit" class="btn btn-primary" name="addhindi" value="Save & Add hindi">
                                    <input type="button" class="btn btn-secondary" name="cancel" value="Cancel">
                          		</div>
                          	</div>
                   		{!! Form::close() !!}
                    </div>
                           
                </div>
    		</div>
    	</div>
    </div>

  @endsection
  @section('addscript')
  <script type="text/javascript" src="{{ asset('js/admin/assets/js/category.js')}}"></script>
 
  @endsection