 <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{ route('admin.dashboard') }}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                    <!-- <li class="menu-title">UI elements</li> --><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-file-text"></i>Category</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-list"></i><a href="{{ route('category.index') }}">Calegory list</a></li>
                            <li><i class="fa fa-plus-square"></i><a href="{{ route('category.create') }}">Add Category</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>State</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-list"></i><a href="{{ route('state.index') }}">State List</a></li>
                            <li><i class="fa fa fa-plus-square"></i><a href="{{ route('state.create') }}">Add State</a></li>
                        </ul>
                    </li>
                     <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Product</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-list"></i><a href="{{ route('product.index') }}">Product List</a></li>
                            <li><i class="fa fa fa-plus-square"></i><a href="{{ route('product.create',['lang'=>'en']) }}">Add Product</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children ">
                        <a href="{{ route('client-orders') }}" > <i class="menu-icon fa fa-table"></i>Client Orders</a>
                        {{-- <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-list"></i><a href="{{ route('product.index') }}">Product List</a></li>
                            <li><i class="fa fa fa-plus-square"></i><a href="{{ route('product.create') }}">Add Product</a></li>
                        </ul> --}}
                    </li>
                    <li class="menu-item-has-children">
                       <a href="{{ route('users.index') }}"><i class="menu-icon fa fa-laptop"></i>Users </a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cog"></i>Settings</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-list"></i><a href="{{route('goldprice.create') }}">Gold Setting</a></li>
                            <li><i class="fa fa fa-plus-square"></i><a href="{{route('gst.create') }}">Gst Setting</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->