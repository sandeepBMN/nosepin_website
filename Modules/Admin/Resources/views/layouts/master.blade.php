    @include('admin::layouts/header')
@include('admin::layouts/sidebar')

 <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Content -->
        <div class="content">
            @yield('content')
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>
        <!-- Footer -->
        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2019
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://colorlib.com">BMN Infotech PVT LTD</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- /.site-footer -->
    </div>
@include('admin::layouts/footer')
    
    <!-- /#right-panel -->