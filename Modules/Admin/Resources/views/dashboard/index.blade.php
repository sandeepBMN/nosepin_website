@extends('admin::layouts.master')
    @section('addstyle')
    <link rel="stylesheet" href="{{asset('css/admin/assets/css/sweetalert/sweetalert.css')}}">
    @endsection
  @section('content')

                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumbs">
                            <div class="breadcrumbs-inner">
                                <div class="row m-0">
                                    <div class="col-sm-4">
                                        <div class="page-header float-left">
                                            <div class="page-title">
                                                <h1>Dashboard</h1>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="page-header float-right">
                                            <div class="page-title">
                                                <ol class="breadcrumb text-right">
                                                    <li><a href="#">Dashboard</a></li>
                                                    <li><a href="#">Forms</a></li>
                                                    <li class="active">Basic</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
              <!-- Animated -->
            <div class="animated fadeIn">

                <!-- Widgets  -->
                <div class="row">
                    
                
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-five">
                                    <div class="stat-icon dib flat-color-1">
                                        <i class="pe-7s-cash"></i>
                                    </div>
                                    <div class="stat-content">
                                        <div class="text-left dib">
                                            <div class="stat-text"><span class="count">{{ (!$data['category']->isEmpty()) ? $data['category']->count() : 0 }}</span></div>
                                            <div class="stat-heading">Categories</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-five">
                                    <div class="stat-icon dib flat-color-3">
                                        <i class="pe-7s-browser"></i>
                                    </div>
                                    <div class="stat-content">
                                        <div class="text-left dib">
                                            <div class="stat-text"><span class="count">{{ (!$data['products']->isEmpty()) ? $data['products']->count() : 0 }}</span></div>
                                            <div class="stat-heading">Products</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-five">
                                    <div class="stat-icon dib flat-color-2">
                                        <i class="pe-7s-cart"></i>
                                    </div>
                                    <div class="stat-content">
                                        <div class="text-left dib">
                                            <div class="stat-text"><span class="count">3435</span></div>
                                            <div class="stat-heading">orders</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-five">
                                    <div class="stat-icon dib flat-color-4">
                                        <i class="pe-7s-users"></i>
                                    </div>
                                    <div class="stat-content">
                                        <div class="text-left dib">
                                            <div class="stat-text"><span class="count">2986</span></div>
                                            <div class="stat-heading">Clients</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <a href="" >
                            <div class="card text-white bg-flat-color-gold">
                                <div class="card-body">
                                    <div class="card-left pt-1 float-left">
                                        <h3 class="mb-0 fw-r">
                                            <span class="count">{{ $data['goldprice'] }}</span>
                                        </h3>
                                        <p class="text-light mt-1 m-0">Gold Price</p>
                                    </div><!-- /.card-left -->

                                    <div class="card-right float-right text-right">
                                        <img src="{{ asset('css/admin/images/avatar/37-512.png')}}">
                                    </div><!-- /.card-right -->
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
               
                <!-- /Widgets -->
                <div class="modal fade goldmodal" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mediumModalLabel">Save Gold Price</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                                {!! Form::open(['route' => 'goldprice.store','class' => 'form-horizontal','method' => 'post']) !!}
                            <div class="modal-body">
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            {!! Form::label('goldprice', 'Gold Price',['class'=> 'form-control-label']) !!}
                                        </div>
                                        <div class="col-12 col-md-9 {{ $errors->has('goldprice') ? 'has-error' : ''}}">
                                           
                                                {!! Form::number('goldprice', old('goldprice'), ['class' => 'form-control','placeholder' => 'Enter Gold Price','id' => 'goldprice','min'=> '0']) !!}
                                                @if($errors->has('goldprice'))
                                                    <span class="text-danger" role="alert">
                                                        <strong>{{ $errors->first('goldprice') }}</strong>
                                                    </span>
                                                @endif
                                           <input type="hidden" name="error" class="counterror" value="{{ count($errors)}}">
                                        </div>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary save">Save</button>
                            </div>
                                {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
              
            </div>
            <!-- .animated -->
            
@endsection
@section('addscript')
<script src="{{asset('js/admin/assets/js/sweetalert/sweetalert.min.js')}}"></script>
<script type="text/javascript">
        window.success = '{{ Session::get('success') }}';
        window.error = '{{ Session::get('error') }}';
    </script>
    <script src="{{asset('js/admin/assets/js/sweetalert/custom.js')}}"></script>
<script type="text/javascript">
    
   
    @if (count($errors) > 0)
   
    jQuery('#mediumModal').modal();
    jQuery('button[type="submit"]').prop('disabled', true);
     jQuery('#goldprice').keyup(function() {
        if(jQuery(this).val() != '') {
           jQuery('button[type="submit"]').prop('disabled', false);
        }
     });
    @endif
    
</script>
@endsection
           
