@extends('admin::layouts.master')
	@section('addstyle')
		<link rel="stylesheet" href="{{asset('css/admin/assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
	@endsection
  @section('content')
  <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))

          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
    </div> 
  	<div class="animated fadeIn">
    	<div class="row">

	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-header">
	                    <strong class="card-title">Data Table</strong>
	                </div>
	                <div class="card-body">

					             @if ($state->count())
	                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach ($state as $row)
                                <tr>
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->name }}</td>

                                    <td style="display: inline-flex;">
                                    	<a href="{{ route('state.edit',base64_encode($row->id)) }}" class="btn btn-primary btn-sm">Edit </a>  |  
                               			<form action="{{ route('state.destroy', $row->id)}}" method="post">
						                  @csrf
						                  @method('DELETE')
						                  <button class="btn btn-danger btn-sm"> Delete</button>
						                </form>
                                    </td>
                                    
                                </tr>
                            	
                            	@endforeach
                            </tbody>
                          </table>
	                    @else
	                    	<div class="text-center">
	                    		<h3>No result Found</h3>
	                    	</div>
	                    @endif
	                </div>
	            </div>
	        </div>

	    </div>
	</div>
  @endsection
  @section('addscript')
   	<script src="{{asset('js/admin/assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/init/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
      } );
  </script>

  @endsection