@extends('admin::layouts.master')

  @section('content')
	 <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <div class="breadcrumbs-inner">
                    <div class="row m-0">
                        <div class="col-sm-4">
                            <div class="page-header float-left">
                                <div class="page-title">
                                    <h1>Edit State</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="page-header float-right">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">State</a></li>
                                        
                                        <li class="active">Edit</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))

          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
    </div> 
    	  <!-- Animated -->
    <div class="animated fadeIn">
    	<div class="row">
    		<div class="col-md-12">
    			<div class="card">
                    <div class="card-header">
                        <strong>Edit State</strong>
                    </div>
                    <div class="card-body card-block">
                           
                    	{!! Form::model($state,['route' => ['state.update',base64_encode($state->id)],'class' => 'form-horizontal','method' => 'PUT']) !!}
                            <div class="row form-group">
                                <div class="col col-md-3">
                                	{!! Form::label('stateName', 'State Name',['class'=> 'form-control-label']) !!}
                                	<!-- <label for="stateName" class=" form-control-label">State Name</label> -->
                                </div>
                                <div class="col-12 col-md-9">
                                	<!-- <input type="text" id="stateName" name="name" placeholder="Enter State Name..." class="form-control"> -->
                                	{!! Form::text('name', old('name',(isset($state->name) ? $state->name :'')), ['class' => 'form-control','placeholder' => 'Enter State Name...','id' => 'stateName']) !!}
                                	@if($errors->has('name'))
		                                <span class="text-danger" role="alert">
		                                    <strong>{{ $errors->first('name') }}</strong>
		                                </span>
		                            @endif
                                </div>
                            </div>
                          	<div class="row form-group">
                          		<div class="offset-3 col-md-9">
                          			 <button type="submit" class="btn btn-primary"> Update</button>
                          		</div>
                          	</div>
                   		{!! Form::close() !!}
                    </div>
                           
                </div>
    		</div>
    	</div>
    </div>

  @endsection