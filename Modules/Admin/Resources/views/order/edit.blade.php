@extends('admin::layouts.master')
@section('addstyle')
    <link rel="stylesheet" href="{{asset('css/admin/assets/css/sweetalert/sweetalert.css')}}">
@endsection
	@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <div class="breadcrumbs-inner">
                    <div class="row m-0">
                        <div class="col-sm-4">
                            <div class="page-header float-left">
                                <div class="page-title">
                                    <h1>Product</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="page-header float-right">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Product</a></li>
                                        
                                        <li class="active">Save</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))

          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
        @if ($errors->any())
             @foreach ($errors->all() as $error)
                 <div class="alert alert-danger">
                    <span>{{$error}}</span>
                </div>
             @endforeach
         @endif
    </div> 
    	  <!-- Animated -->
    <div class="animated fadeIn">
    	<div class="row">
    		<div class="col-md-12">
    			<div class="card">
                    <div class="card-header">
                        <strong>Update Product</strong>
                    </div>
                    <div class="card-body card-block">

                      @if($lang == 'en')
                    	{!! Form::model($product,['route' => ['product.update',base64_encode($product->id),'en'],'class' => 'form-horizontal','method' => 'PUT','files' => true]) !!}
                            

                             <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('state', 'Select State',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    <select name="state" id="state" class=" form-control">
                                        <option value="">Please select</option>
                                        @foreach($state as $v)
                                            <option value="{{ $v->id }}" {{ $product->state_id == $v->id ? 'selected="selected"' : '' }}>{{ $v->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('category', 'Select Category',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    <select name="category" id="category" class=" form-control" onchange="getSubcategory(this)">
                                        <option value="">Please category</option>
                                        @foreach($selectcat as $v)
                                            <option value="{{ $v->id }}" data-id="{{ $v->id}}" >{{ $v->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                             <div class="row form-group appendsubcat" style="display: none;">
                                
                                <div class="offset-3 col-md-9 subcategory">
                                    
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('productName', 'Product Name',['class'=> 'form-control-label']) !!}
                                   
                                </div>
                                <div class="col-12 col-md-9">
                                   
                                    {!! Form::text('name', old('name'), ['class' => 'form-control','placeholder' => 'Enter Product Name...','id' => 'productName']) !!}
                                    @if($errors->has('name'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('description', 'Description',['class'=> 'form-control-label']) !!}                                   
                                </div>
                                <div class="col-12 col-md-9">
                                    {!! Form::textarea('description', old('description'), ['class' => 'form-control','placeholder' => 'Enter Product Name...','id' => 'description','rows' => '5']) !!}
                                    @if($errors->has('description'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('labour', 'Product Labour',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    {!! Form::number('labour', old('price'), ['class' => 'form-control','placeholder' => 'Enter Labour in %','id' => 'labour','min'=>'0']) !!}
                                    @if($errors->has('labour'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('labour') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('inc_dec_labour', 'labour Inc/Dec',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    <select name="inc_dec_labour" id="inc_dec_labour" class="form-control">
                                        <option value="">Please select</option>
                                        <option value="inc" {{ $product->inc_dec_labour == 'inc' ? 'selected' : '' }} > Increment</option>
                                        <option value="dec" {{ $product->inc_dec_labour == 'dec' ? 'selected' : '' }}>Decrement</option>
                                    </select>
                                    @if($errors->has('inc_dec_labour'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('inc_dec_labour') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('weight', 'Weight',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    {!! Form::number('weight', old('weight'), ['class' => 'form-control','placeholder' => 'Enter Weight','id' => 'weight','min' => '0']) !!}
                                    @if($errors->has('weight'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('weight') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('purity', 'Purity',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    <div class="input-group" id="extend">
                                        <div class="input-group form-group">
                                            {!! Form::number('purity[]', old('purity',(count($purity) > 0 ? $purity[0] : '' )), ['class' => 'form-control','placeholder' => 'Enter Purity','id' => 'purity','min' => '0']) !!}
                                            <div class="input-group-btn">
                                                <button class="btn btn-info add_field_button" type="button"><i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        @if(!empty($purity))
                                            @foreach($purity as $key=>$val)
                                                @if($key != 0)
                                                    <div class="input-group form-group">
                                                        {!! Form::number('purity[]', old('purity',$val), ['class' => 'form-control','placeholder' => 'Enter Purity','id' => 'purity','min' => '0']) !!}
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-info remove_field"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                    
                                    @if($errors->has('purity'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('purity') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('discountPrice', 'Discount',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    {!! Form::text('discountPrice', old('discountPrice','0'), ['class' => 'form-control','placeholder' => 'Enter Discount','id' => 'discountPrice']) !!}
                                    
                                </div>
                            </div>
                            @if(isset($product->attachments))
                           
                                 @if($product->attachments->count() < 3)
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                {!! Form::label('prod_image', 'Image',['class'=> 'form-control-label']) !!}
                                                
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <label class="btn-file btn btn-primary">Browse
                                                    <input type="file" id="edit_prod_image" name="prod_image[]" class="form-control-file" multiple="multiple" >
                                                </label>
                                               
                                                @if($errors->has('prod_image'))
                                                    <span class="text-danger" role="alert">
                                                        <strong>{{ $errors->first('prod_image') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        @endif
                                @endif

                                <div class="row append-img-edit mb-15">
                                    <div class="offset-3 col-md-9 appended-div">
                                        @if(isset($product->attachments))
                                            @if($product->attachments->count() < 4)
                                            <input type="hidden" name="filecount" id="fcount" value="{{ @$product->attachments->count()}}">
                                                @foreach($product->attachments as $k=>$v)
                                                <div id="preview-img-edit" class="preview-img-edit attach_{{ $k }}">
                                                    <img src="{{ asset(str_replace("\\","/",$v['storage_path'].'\\'. $v['image_name'])) }}" width="120">
                                                    <div class="overlay">
                                                        <a href="javascript:void(0)" class="edit_product_image" data-id="{{ $v->id }}"><i class="fa fa-trash fa-2x icon "></i></a>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif
                                        @endif
                                    </div>
                                </div>
                          
                          	<div class="row form-group">
                          		<div class="offset-3 col-md-9">
                                    <input type="submit" class="btn btn-primary" name="update" value="Update">
                          		</div>
                          	</div>
                   		{!! Form::close() !!}
                        @else
                        {!! Form::model($product,['route' => ['product.update',base64_encode($product['id']),'hn'],'class' => 'form-horizontal','method' => 'PUT','files' => true]) !!}
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('productName', 'Product Name',['class'=> 'form-control-label']) !!}
                                   
                                </div>
                                <div class="col-12 col-md-9">
                                   
                                    {!! Form::text('name', old('name',(array_key_exists('name',$product) ? $product['name'] : '')), ['class' => 'form-control','placeholder' => 'Enter Product Name...','id' => 'productName']) !!}
                                    @if($errors->has('name'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('description', 'Description',['class'=> 'form-control-label']) !!}                                   
                                </div>
                                <div class="col-12 col-md-9">
                                    {!! Form::textarea('description', old('description',(array_key_exists('description', $product) ? $product['description'] : '')), ['class' => 'form-control','placeholder' => 'Enter Product Name...','id' => 'description','rows' => '5']) !!}
                                    @if($errors->has('description'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="offset-3 col-md-9">
                                    <input type="submit" class="btn btn-primary" name="updatehindi" value="Update in hindi">
                                </div>
                            </div>
                                {!! Form::close() !!}
                        @endif

                    </div>
                           
                </div>
    		</div>
    	</div>
    </div>

	@endsection
@section('addscript')
    <script src="{{asset('js/admin/assets/js/sweetalert/sweetalert.min.js')}}"></script>
    <script type="text/javascript">
        window.success = '{{ Session::get('success') }}';
        window.error = '{{ Session::get('error') }}';
    </script>
    <script src="{{asset('js/admin/assets/js/sweetalert/custom.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/admin/assets/js/product.js')}}"></script>
@endsection
