@extends('admin::layouts.master')
	@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <div class="breadcrumbs-inner">
                    <div class="row m-0">
                        <div class="col-sm-4">
                            <div class="page-header float-left">
                                <div class="page-title">
                                    <h1>Product</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="page-header float-right">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Product</a></li>
                                        
                                        <li class="active">Save</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))

          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
        @if ($errors->any())
             @foreach ($errors->all() as $error)
                 <div class="alert alert-danger">
                    <span>{{$error}}</span>
                </div>
             @endforeach
         @endif
    </div> 
    	  <!-- Animated -->
    <div class="animated fadeIn">
    	<div class="row">
    		<div class="col-md-12">
    			<div class="card">
                    <div class="card-header">
                        <strong>Save Product</strong>
                    </div>
                    <div class="card-body card-block">
                           
                    	{!! Form::open(['route' => 'product.store','class' => 'form-horizontal','method' => 'post','files' => true]) !!}
                            
                        @if($lang != 'hn')
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('state', 'Select State',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    <select name="state" id="state" class=" form-control">
                                        <option value="">Please select</option>
                                        @foreach($state as $v)
                                            <option value="{{ $v->id }}">{{ $v->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('category', 'Select Category',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    <select name="category" id="category" class=" form-control" onchange="getSubcategory(this)">
                                        <option value="">Please category</option>
                                        @foreach($selectcat as $v)
                                            <option value="{{ $v->id }}" data-id="{{ $v->id}}">{{ $v->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group appendsubcat" style="display: none;">
                                
                                <div class="offset-3 col-md-9 subcategory">
                                    <select name="subcategory" id="subcat" class=" form-control">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                        @endif
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('productName', 'Product Name',['class'=> 'form-control-label']) !!}
                                   
                                </div>
                                <div class="col-12 col-md-9">
                                   
                                    {!! Form::text('name', old('name'), ['class' => 'form-control','placeholder' => 'Enter Product Name...','id' => 'productName']) !!}
                                     <input type="hidden" name="productid" value="{{ ($pid != null) ? 
                                        $pid : '' }}">
                                    @if($errors->has('name'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('description', 'Description',['class'=> 'form-control-label']) !!}                                   
                                </div>
                                <div class="col-12 col-md-9">
                                    {!! Form::textarea('description', old('description'), ['class' => 'form-control','placeholder' => 'Enter Product Name...','id' => 'description','rows' => '5']) !!}
                                    @if($errors->has('description'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @if($lang != 'hn')
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('labour', 'Product labour',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    {!! Form::number('labour', old('labour'), ['class' => 'form-control','placeholder' => 'Enter labour in %','id' => 'labour','min'=> '0']) !!}
                                    @if($errors->has('labour'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('labour') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('inc_dec_labour', 'labour Inc/Dec',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    <select name="inc_dec_labour" id="inc_dec_labour" class="form-control">
                                        <option value="">Please select</option>
                                        <option value="inc">Increment</option>
                                        <option value="dec">Decrement</option>
                                    </select>
                                    @if($errors->has('inc_dec_labour'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('inc_dec_labour') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('weight', 'Weight',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    {!! Form::number('weight', old('weight'), ['class' => 'form-control','placeholder' => 'Enter Weight','id' => 'weight','min' => '0']) !!}
                                   
                                    @if($errors->has('weight'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('weight') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-md-3">
                                    {!! Form::label('purity', 'Purity',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    <div class="input-group" id="extend">
                                        <div class="input-group form-group">
                                         
                                            {!! Form::number('purity[]', old('purity'), ['class' => 'form-control','placeholder' => 'Enter Purity','id' => 'purity','min' => '0']) !!}
                                            <div class="input-group-btn">
                                                <button class="btn btn-info add_field_button" type="button"><i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    @if($errors->has('purity'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('purity') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('discountPrice', 'Discount',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    {!! Form::text('discountPrice', old('discountPrice','0'), ['class' => 'form-control','placeholder' => 'Enter Discount','id' => 'discountPrice']) !!}
                                    
                                </div>
                            </div>
                           
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    {!! Form::label('prod_image', 'Image',['class'=> 'form-control-label']) !!}
                                </div>
                                <div class="col-12 col-md-9">
                                    <label class="btn-file btn btn-primary">Browse
                                        <input type="file" id="prod_image" name="prod_image[]" class="form-control-file" multiple="multiple">
                                    </label>
                                   
                                    @if($errors->has('prod_image'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('prod_image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                           
                            <div class="row append-img-edit mb-15" style="display: none;">
                               <div class="offset-3 col-md-9 appended-div">
                               </div>
                            </div>
                         @endif     
                          	<div class="row form-group">
                          		<div class="offset-3 col-md-9">
                                     @if($lang == 'hn')
                                        <input type="submit" class="btn btn-primary" name="savehindi" value="Save in Hindi">
                                    @else
                                    <input type="submit" class="btn btn-primary" name="save" value="Save">
                                    <input type="submit" class="btn btn-primary" name="addHindi" value="Save and Continue in Hindi">
                                    @endif
                                    <input type="button" class="btn btn-secondary" name="cancel" value="Cancel">
                          			 
                          		</div>
                          	</div>
                   		{!! Form::close() !!}
                    </div>
                           
                </div>
    		</div>
    	</div>
    </div>

	@endsection
	@section('addscript')
	<script type="text/javascript" src="{{ asset('js/admin/assets/js/product.js')}}"></script>
   <!--  <script type="text/javascript">

    var count ='';
    jQuery("#prod_image").change(function(e){

        jQuery('.appended-div').html('');
        readimageURL(this);
    });
     function readimageURL(input) {
               
        count = document.getElementById("prod_image").files.length;
            console.log(count);   
        if (count > 0 && count < 4) {
            
            var total_file = document.getElementById("prod_image").files.length;

             for(var i=0;i<total_file;i++)
            {      
                    jQuery('.append-img-edit').css('display','block');
                    jQuery('.append-img-edit').find('.appended-div').append('<div id="preview_img_'+i+'" class="preview-img-edit">'
                        +'<img src="'+URL.createObjectURL(event.target.files[i])+'" width="120">'
                        +'</div>');
            }
        }else{
             jQuery('.append-img-edit').css('display','block');
              jQuery('.append-img-edit').find('.appended-div').html('Three files are allowed').css("color", "red");
        }
    }
    </script> -->
	@endsection
