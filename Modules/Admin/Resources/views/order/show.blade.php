@extends('admin::layouts.master')
@section('addstyle')

<style type="text/css">
	.showimage{
		width: 185px;
		height: 185px;
		margin-right: 10px;
		margin-bottom: 10px;
		float: left;
	}
	table td.attachfile{
		border-top: none;
	}




	/* reset */


	/* content editable */

	*[] { border-radius: 0.25em; min-width: 1em; outline: 0; }

	*[] { cursor: pointer; }

	*[]:hover, *[]:focus, td:hover *[], td:focus *[], img.hover { background: #DEF; box-shadow: 0 0 1em 0.5em #DEF; }

	.invoice-order span[] { display: inline-block; }

	/* heading */

	.invoice-order h1 { font: bold 100% sans-serif; letter-spacing: 0.5em; text-align: center; text-transform: uppercase; }

	/* table */

	.invoice-order table { font-size: 75%; table-layout: fixed; width: 100%; }
	.invoice-order table { border-collapse: separate; border-spacing: 2px; }
	.invoice-order th, .invoice-order td { border-width: 1px; padding: 0.5em; position: relative; text-align: left; }
	.invoice-order th, .invoice-order td { border-radius: 0.25em; border-style: solid; }
	.invoice-order th { background: #EEE; border-color: #BBB; }
	.invoice-order td { border-color: #DDD; }

	/* page */

	.invoice-order html { font: 16px/1 'Open Sans', sans-serif; overflow: auto; padding: 0.5in; }
	.invoice-order html { background: #999; cursor: default; }

	/* body { box-sizing: border-box; height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
	body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); } */

	/* header */

	.invoice-order header { margin: 0 0 3em; }
	.invoice-order header:after { clear: both; content: ""; display: table; }

	.invoice-order header h1 { background: #000; border-radius: 0.25em; color: #FFF; margin: 0 0 1em; padding: 0.5em 0; }
	.invoice-order header address { float: left; font-size: 75%; font-style: normal; line-height: 1.25; margin: 0 1em 1em 0; }
	.invoice-order header address p { margin: 0 0 0.25em; }
	.invoice-order header span, .invoice-order header img { display: block; float: right; }
	.invoice-order header span { margin: 0 0 1em 1em; max-height: 25%; max-width: 60%; position: relative; }
	.invoice-order header img { max-height: 100%; max-width: 100%; }
	.invoice-order header input { cursor: pointer; -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; height: 100%; left: 0; opacity: 0; position: absolute; top: 0; width: 100%; }

	/* article */

	.invoice-order article, .invoice-order article address, .invoice-order table.meta, .invoice-order table.inventory { margin: 0 0 3em; }
	.invoice-order article:after { clear: both; content: ""; display: table; }
	.invoice-order article h1 { clip: rect(0 0 0 0); position: absolute; }

	.invoice-order article address { float: left; font-size: 125%; font-weight: bold; }

	/* table meta & balance */

	.invoice-order table.meta, .invoice-order table.balance { float: right; width: 36%; }
	.invoice-order table.meta:after, .invoice-order table.balance:after { clear: both; content: ""; display: table; }

	/* table meta */

	.invoice-order table.meta th { width: 40%; }
	.invoice-order table.meta td { width: 60%; }

	/* .invoice-order table items */

	.invoice-order table.inventory { clear: both; width: 100%; }
	.invoice-order table.inventory th { font-weight: bold; text-align: center; }

	.invoice-order table.inventory td:nth-child(1) { width: 26%; }
	.invoice-order table.inventory td:nth-child(2) { width: 38%; }
	.invoice-order table.inventory td:nth-child(3) { text-align: right; width: 12%; }
	.invoice-order table.inventory td:nth-child(4) { text-align: right; width: 12%; }
	.invoice-order table.inventory td:nth-child(5) { text-align: right; width: 12%; }

	/* .invoice-order table balance */

	.invoice-order table.balance th, .invoice-order table.balance td { width: 50%; }
	.invoice-order table.balance td { text-align: right; }

	/* aside */

	.invoice-order aside h1 { border: none; border-width: 0 0 1px; margin: 0 0 1em; }
	.invoice-order aside h1 { border-color: #999; border-bottom-style: solid; }

	/* javascript */
	@media print {
		* { -webkit-print-color-adjust: exact; }
		.invoice-order html { background: none; padding: 0; }
		.invoice-order body { box-shadow: none; margin: 0; }
		.invoice-order span:empty { display: none; }
	}

@page { margin: 0; }

</style>
@endsection
@section('content')
	<div class="animated fadeIn">
    	<div class="row">
	       <div class="col-lg-12">
				<div class="row">
					<div class="col-md-6">
						<form action="{{ route('update-delivery-date') }}" method="POST">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="email">Add Delivery Date:</label>
							<input type="date" class="form-control" name="deliveryDate" value="{{ $orderDetails['delivery_date'] }}">
							<input type="hidden" class="form-control" name="order_id" value="{{ $orderDetails['id'] }}">
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
					<div class="col-md-1">
						
					</div>
					<div class="col-md-5">
						<form action="{{ route('update-delivery-status') }}" method="POST" id="changeDeliveryStatus">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="email">Delivery Status:</label>
								@php
									$deliveryStatus = ['order inprocess' => 1,'order shipped' => 2,'order deliever' => 3,'payment pending' => 4];	
								@endphp
								<select class="form-control" id="deliveryStatus" name="deliveryStatus">
									@foreach( $deliveryStatus as $k => $v )
										<option value="{{ $v }}" {{ ($orderDetails['delievery_status'] == $v)? 'selected' : '' }}>{{ ucfirst($k) }}</option>
									@endforeach
								</select>
								<input type="hidden" class="form-control" name="order_id" value="{{ $orderDetails['id'] }}">
							</div>
						</form>
					</div>
				</div>
                <div class="card">

                    <div class="card-header ">
                        <strong class="card-title">Product Detail</strong>
                    </div>
                    <div class="card-body invoice-order">
						<header>
							<h1>Invoice</h1>
							@if( $orderDetails['deliverAddress'] != null )
								<address>
								<p>{{ $orderDetails['User']['name'] }}</p>
									<p>{{ $orderDetails['deliverAddress'][0]['address'] }}</p>
									<p>{{ $orderDetails['deliverAddress'][0]['city'] }} {{ $orderDetails['deliverAddress'][0]['country'] }} {{ $orderDetails['deliverAddress'][0]['state'] }}</p>
								<p>{{ $orderDetails['User']['mobile'] }}</p>
								</address>
							@endif
							@if( $orderDetails['deliverAddress'] == null )
								<div>Address Deleted By customer</div>
							@endif

							
							{{-- <span><img alt="" src="http://www.jonathantneal.com/examples/invoice/logo.png"><input type="file" accept="image/*"></span> --}}
						</header>
						<article>
							<h1>Recipient</h1>
							<address >
								<p>{{ $orderDetails['User']['email'] }}</p>
							</address>
							<table class="meta">
								<tr>
									<th><span >Invoice #</span></th>
									<td><span >{{ $orderDetails['invoice_no'] }}</span></td>
								</tr>
								<tr>
									<th><span >Date</span></th>
								<td><span >{{ $orderDetails['created_at'] }}</span></td>
								</tr>
								{{-- <tr>
									<th><span >Amount Due</span></th>
									<td><span id="prefix" >$</span><span>600.00</span></td>
								</tr> --}}
							</table>
							<table class="inventory">
								<thead>
									<tr>
										<th><span >Item</span></th>
										<th><span >Weight (in gram)</span></th>
										<th><span >labour (in %)</span></th>
										<th><span >Quantity</span></th>
										<th><span >Total Weight (in gram)</span></th>
										<th><span >Purity (in %)</span></th>
										<th><span >Price</span></th>
									</tr>
								</thead>
								<tbody>
									@php
										$totalAmount = 0;
									@endphp
									@foreach( $orderDetails['orderProducts'] as $k => $v)
										@php
											
											$totalAmount = ($totalAmount + $v['price']);
										@endphp
										<tr>
											<td><span>{{ $v['name'] }}</span></td>
											<td>{{ $v['weight'] }}</td>
											<td>{{ $v['labour'] }}</td>
											<td><span>{{ $v['quantity'] }}</span></td>
											<td><span>{{ $v['total_weight'] }}</span></td>
											<td>{{ $v['purity'] }}</td>
											<td><span data-prefix>₹</span><span>{{ $v['price'] }}</span></td>
										</tr>
									@endforeach
								</tbody>
							</table>
							<table class="balance">
								<tr>
									<th><span >Total</span></th>
									<td><span data-prefix>₹</span><span>{{ ($totalAmount - (($totalAmount * 18)/100)) }}</span></td>
								</tr>
								<tr>
									<th><span >Total Tax (GST 18%)</span></th>
								<td><span data-prefix>₹</span><span>{{ (($totalAmount * 18)/100) }}</span></td>
								</tr>
								<tr>
									<th><span >Balance Due</span></th>
									<td><span data-prefix>₹</span><span>{{ $totalAmount }}</span></td>
								</tr>
							</table>
						</article>
						<aside>
							<h1><span >Additional Notes</span></h1>
							<div >
								<p>All Item will be sold on GST ( GST No here ).</p>
							</div>
						</aside>
               	 	</div>
            	</div>
	    	</div>
		</div> 
	</div>

@endsection
@section('addscript')
<script src="{{asset('js/admin/assets/js/sweetalert/custom.js')}}"></script>

	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#deliveryStatus').change(function(){
				jQuery('#changeDeliveryStatus').submit();
			});
		});
	</script>

@endsection
