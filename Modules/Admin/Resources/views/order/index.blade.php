@extends('admin::layouts.master')
	@section('addstyle')
    <link rel="stylesheet" href="{{asset('css/admin/assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/admin/assets/css/sweetalert/sweetalert.css')}}">
  @endsection

  @section('content')
  <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))

          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
    </div> 
  	<div class="animated fadeIn">
    	<div class="row">

	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-header">
	                    <strong class="card-title">Product List</strong>
	                </div>
	                <div class="card-body">

					    @if ($listOrder->count())
                          	<table id="bootstrap-data-table" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Invoice no</th>
										<th>GST Percent</th>
										<th>GST Amount</th>
										<th>on Gold Price</th>
										<th>Total Price</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($listOrder as $row)
										<tr>
											<td>{{ $row->invoice_no }}</td>
											<td>{{ $row->gst_percent }}</td>
											<td>{{ $row->gst_amount }}</td>
											<td>{{ $row->gold_price }}</td>
											<td>{{ $row->totalprice }}</td>
										<td><a href="{{ route('get-order-details' , $row->id) }}" class="btn btn-primary">GET Details</a></td>
										</tr>
									@endforeach
								</tbody>
                          	</table>
                     	@endif
	                </div>
	            </div>
	        </div>

	    </div>
	</div>
  @endsection
  @section('addscript')
   	<script src="{{asset('js/admin/assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/init/datatables-init.js')}}"></script>
    <script src="{{asset('js/admin/assets/js/sweetalert/sweetalert.min.js')}}"></script>

    <script type="text/javascript">
        window.success = '{{ Session::get('success') }}';
        window.error = '{{ Session::get('error') }}';
    </script>
    <script src="{{asset('js/admin/assets/js/sweetalert/custom.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
      } );
  </script>
 
  @endsection