<?php 
namespace Modules\Admin\Repository;

use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\State;
use Modules\Admin\Services\StateService;
use Auth;
use Validator;
use Input;
use Hash;

class StateRepository 
{
	
    /**
     * To get  all state 
     * @param 
     * @return  state collection 
     */
    public function allState(){
        $state = State::all();
        return $state;
    }


     /**
     * To save  a new state 
     * @param  array $data 
     * @return  array 
     */
    public function saveState($data)
    {
       $state = State::create([
            'name'   => $data['name'],
        ]);
        return true;
    } 
    /**
     * To save  a new state 
     * @param  array $data , state id $id
     * @return  array 
     */
    public function updateState($data,$id)
    {

        $state = State::find($id);
        if($state != null)
        {
            $state->name = $data['name'];
            $state->save();  
            return true;
        }
        else{
            return false;
        }
    }
    /**
     * To Delete a state 
     * @param  state id $id 
     * @return  array 
     */
    public function deleteState($id){

        $state = State::find($id);
        $state->delete();
        return true;
    }
}