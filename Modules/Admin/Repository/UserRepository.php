<?php 
namespace Modules\Admin\Repository;

use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\AdminUser;
use Modules\Admin\Services\AuthService;
use Auth;
use Validator;
use Input;
use Hash;

class UserRepository extends AuthService
{
	public $user;


	function __construct(AdminUser $user) {
      $this->user = $user;
      
    }
     /**
     * To login a new user 
     * @param  array $data 
     * @return  array 
     */
    public function login($data)
    {
       
        $credentials = [
            'email' => $data['email'],
            'password' => $data['password'],
        ];
       
        if(Auth::guard('admin')->attempt($credentials)){
            Auth::login(Auth::guard('admin')->user());
            return true;
        }else{
            return false;
        }
    }
}