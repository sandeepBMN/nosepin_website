<?php 
namespace Modules\Admin\Repository;

use Modules\Admin\Entities\Product;
use Modules\Admin\Entities\Category;
use Modules\Admin\Entities\Language;
use Modules\Admin\Entities\Attachment;
use Modules\Admin\Entities\ProductAttachment;
use Modules\Admin\Services\ProductService;
use Auth;
use Validator;
use Input;
use Image;
use Hash;
use File;
use Session;

class ProductRepository 
{

    /**
     * To get  all product
     * @param   
     * @return  all product collection 
     */
    public function allProduct($id)
    {
        if($id != null)
        {
            $product =  Product::with('attachments','category')->where('category_id',$id)->get();

        }else{
            $product =  Product::with('attachments','category')->get();
        }
       return $product;
    }
    /**
     * To get  all product
     * @param   
     * @return  all product collection 
     */
    public function getProducts()
    {
    
        $product =  Product::with('attachments','category')->get();
       return $product;
    }


     /**
     * To save  a new product
     * @param  array $data 
     * @return  array 
     */
    public function saveProduct($request)
    {

         $lang =  Session::get('type');
       $newdata = $request->all();
     
       $product=[];
    
         
          if(array_key_exists('savehindi',$newdata))
        {
            $saveproductlang = [];
            $arr = ["name","description"];
            foreach ($arr as $key => $value) {

                if(array_key_exists($value, $newdata))
                {

                    $saveproductlang[] =[
                        'text' => $newdata[$value],
                        'attachable_model' => 'Product',
                        'language' => $lang,
                        'type'     => $value,
                        'attachable_id' => $newdata['productid']
                    ];
                }
            }

        }else{
           $product['name']          = $newdata['name'];
            $product['description']   = $newdata['description'];
            $product['labour']  = $newdata['labour'];
            $product['inc_dec_labour']  = $newdata['inc_dec_labour'];
            $product['weight']            =  $newdata['weight'];
            $product['purity']            =  json_encode($newdata['purity'],true);
            $product['discount_price']    =  $newdata['discountPrice'];
            $product['state_id']          = $newdata['state'];
            if (array_key_exists('subcategory',$newdata) && $newdata['subcategory'] != null){
            $product['category_id']  = $newdata['subcategory'];
            }else
            {
            $product['category_id']  = $newdata['category'];
            }
        }
    
        if (array_key_exists('prod_image',$newdata) && $newdata['prod_image'] != null){

            $files = $request->file('prod_image');
           
            if($files) 
            {
                $docsize=0;
               foreach ($files as  $image) {
                $docsize +=  $image->getSize();
               }
               
                $countfile = count($files);
                if(is_numeric($docsize) && $docsize <= 5000000 )
                { 
                    if ($countfile < 4)
                    {
                        if (array_key_exists('save',$newdata) ||  array_key_exists('addHindi',$newdata)){
                            $products =  Product::create($product);
                            $productId = $products->id;
                           
                                // Get image file
                                $destinationPath = "uploads/product/";
                                $filepath =   $this->saveAttach($files,$destinationPath,$productId);
                            
                            if(array_key_exists('save', $newdata))
                           {
                            return ['status'=>true,'type'=> 'en','btn'=>'save'];
                           }
                            if(array_key_exists('addHindi', $newdata))
                           {
                            return ['status'=>true,'type'=> 'en','btn' => 'addHindi','data'=> $productId];
                           }
                       }
                                                 
                    }else{
                        return ['status' =>false,'message'=> 'Max Three files are Allowed','type'=>400];
                    }
                }
                else{
                      return ['status' =>false,'message'=> 'Total Files size should not greater than 5MB','type'=>400];
                }
            }
        }
         if(array_key_exists('savehindi', $newdata))
        {
            foreach ($saveproductlang as $key => $value) {
                
                $lang = new Language();
                $lang->text = $value['text'];
                $lang->attachable_model = $value['attachable_model'];
                $lang->language = $value['language'];
                $lang->type = $value['type'];
                $lang->attachable_id = $value['attachable_id'];
                $lang->save();
            }

            return ['status'=>true,'type' => 'hn','btn'=> 'savehindi'];
        }
       
    } 

    public function saveAttach($files,$path,$id){
        $saveImageTypesArray = ['original','medium','thumbs'];
          $model = Product::find($id);
                if($model != null){
                    if($files)
                    {
                        foreach ($files as $key => $file) {

                                $ext = $file->getClientOriginalExtension();
                                $readImage = Image::make($file);
                                $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;


                            foreach($saveImageTypesArray as $index => $type){
                                $filePath = $path.$id.'/'.$type.'/';
                                if (!file_exists($filePath)) {
                                    mkdir($filePath, 0755, true);
                                }
                                if($type == 'original')
                                {
                                    $readImage->save($filePath.$fileName);
                                   
                                }elseif($type == 'medium'){
                                    $readImage->resize(350,350)->save($filePath.$fileName);
                                }elseif($type == 'thumbs')
                                {
                                    $readImage->resize(150,150)->save($filePath.$fileName);
                                }
                                $attachment = new Attachment;
                                $attachment->image_name = $fileName;
                                $attachment->storage_path ='uploads\\product\\'.$id.'\\'.$type;
                                $attachment->type = $type;
                                $model->attachments()->save($attachment);
                            }
                        }
                        return true;
                    }
                }
                else{
                    return false;
                }
            

    }


     /**
     * To save  a new update 
     * @param  array $request , product id $id
     * @return  array 
     */
    public function updateProduct($request,$id,$lang)
    {
        $getproduct = Product::with(['attachments'=> function($query){ 
            $data = $query->where(['type' => 'original'])->get();
        }])->find($id);

        $allcount = $getproduct->attachments->count();
        $langcat = Language::where('attachable_id',$id)->where('attachable_model','Product')->get();
            $arraylang= $langcat->toArray();
            $newdata = $request->all();
            if(array_key_exists('updatehindi',$newdata))
            {
                $saveproductlang = [];
                $arr = ["name","description"];
                foreach ($arr as $key => $value) {

                    if(array_key_exists($value, $newdata))
                    {

                        $saveproductlang[] =[
                            'text' => $newdata[$value],
                            'type'     => $value,
                            'language' => $lang,
                            'attachable_id' => $id
                        ];
                    }
                }

            }else{
                $product['name']          = $newdata['name'];
                $product['description']   = $newdata['description'];
                $product['labour']  = $newdata['labour'];
                $product['inc_dec_labour']  = $newdata['inc_dec_labour'];
                $product['weight']            =  $newdata['weight'];
                $product['purity']            =  json_encode($newdata['purity'],true);
                $product['discount_price']    =  $newdata['discountPrice'];
                $product['state_id']          = $newdata['state'];
                if (array_key_exists('subcategory',$newdata) && $newdata['subcategory'] != null){
                $product['category_id']  = $newdata['subcategory'];
                }else{
                $product['category_id']  = $newdata['category'];
                }
            }
        
         if($allcount == 0 || $allcount < 4)
        {    
            if(array_key_exists('prod_image',$newdata) && $newdata['prod_image'] != null){
                $files = $request->file('prod_image');
                if($files) {
                    $docsize=0;
                       foreach ($files as  $image) {
                            $docsize +=  $image->getSize();
                       }
                        $countfile = count($files);
                     if(is_numeric($docsize) && $docsize <= 5000000 )
                    { 
                        $totalfiles =(int) $allcount+ (int) $countfile;
                         
                        if ($totalfiles < 4) {
                            if (array_key_exists('update',$newdata)){
                                $saveProduct =  Product::where('id',$id)->update($product);
                                // Get image file
                                $destinationPath = "uploads/product/".$id;
                                $filepath =   $this->saveAttach($files,$destinationPath,$id);
                                 return ['status'=>true,'type'=> 'en','btn'=>'update'];
                            }
                        }else{
                            return ['status' =>false,'message'=> 'Max Three files are Allowed','type'=>400];
                        }
                    }else{
                        return ['status' =>false,'message'=> 'Total Files size should not greater than 5MB','type'=>400];
                    }
                }
            }

        }
        // if($allcount == 3)
        // {
        //       if(!array_key_exists('prod_image',$newdata))
        //     {
        //          if (array_key_exists('update',$newdata)){
        //             $saveProduct =  Product::where('id',$id)->update($product);
        //                  return ['status'=>true,'type'=> 'en','btn'=>'update'];  
        //             }       
        //     }    
        // }
            if(array_key_exists('updatehindi', $newdata))
            {
                // $arrId = array_column($arraylang, 'attachable_id');
                    $update =     Language::where(['attachable_id'=>$id])->where(['attachable_model'=>'Product'])->get();
                    
                // foreach ($arrId as $key => $value) {
                    foreach ($saveproductlang as $k => $v) {

                         Language::where(['attachable_id'=>$id])->where('attachable_model','Product')
                         ->update(['text' =>$v['text'],'type' => $v['type']]);
                        // $update->text = $v['text'];
                        // $update->type = $v['type'];
                        // $update->update();
                    }
                    // $language = new Language();
                    // $language->text = $value['text'];
                    // $language->type = $value['type'];
                    // $language->language = $value['language'];
                    // $language->attachable_model = 'Product';
                    // $language->attachable_id   = $id;
                    
                // }

                return ['status'=>true,'type' => 'hn','btn'=> 'updatehindi'];
            }
    }

     /**
     * To Delete a product
     * @param  product id $id 
     * @return  array 
     */
    public function deleteProduct($id){

        $getProduct = Product::find($id);
        $getProduct->delete();
        return true;
    }
    /**
     * To show a product
     * @param  product id $id 
     * @return  array 
     */
    public function productDetail($id){

        $getproduct = Product::with('attachments','category','state')->where('id',$id)->first();
        if($getproduct !=null)
        {
            $getproduct = $getproduct->toArray();
        }
        $arr = ['attachment_id','updated_at','category_id','state_id'];
        foreach ($arr as $key => $value) {
           
            if(array_key_exists($value, $getproduct))
            {
                unset($getproduct[$value]);
            }
        }
        return $getproduct;
    }

    /**
     * To get  products based on category
     * @param   
     * @return  all product collection 
     */
    public function getProductByCategory($cat_id)
    {
       $product =  Product::with('attachments')->where('category_id',$cat_id)->get()->toArray();
       return $product;
    }

    public function hotproduct($productId,$statusid)
    {
        $product = Product::find($productId);
        if($product != null)
        {
            $product->hot_product = $statusid;
            $product->save();
            if($statusid == 0)
            {
                 return ['status' =>true,'message'=> 'Product Added in hot product list successfully'];
            }elseif($statusid == 1){
                 return ['status' =>true,'message'=> 'Product Remove in hot product list successfully'];
            }
        }else{
            return ['status' =>false,'message'=> 'Product not found','type'=>404];
        }
    }


     /**
     * To get hotproducts  with attachments.
     * @param 
     * @return Response
     */
    public function getHotProducts()
    {
        $hotproduct = Product::with('attachments')->where('hot_product',1)->get();
        if($hotproduct != null)
        {
            return $hotproduct;
            
        }else{
            return false;
        }
    }

     /**
     * To remove attachment in database.
     * @param  request
     * @return Response
     */
    public function removeAttachment($data)
    {
        
       $attach =  ProductAttachment::where('id',$data['id'])->first();
        if($attach != null)
        {
            $attach->delete();
            return true;
        }
        else{
            return false;
        }
    }
}
    