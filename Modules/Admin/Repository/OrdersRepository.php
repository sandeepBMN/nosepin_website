<?php 
namespace Modules\Admin\Repository;

use Modules\Admin\Entities\Product;
use Modules\Admin\Entities\Category;
use Modules\Admin\Entities\Language;
use Modules\Admin\Entities\Attachment;
use Modules\Admin\Entities\ProductAttachment;
use Modules\Admin\Services\ProductService;
use Auth;
use Validator;
use Input;
use Image;
use Hash;
use File;
use Session;
use App\Models\Order;

class OrdersRepository 
{
    /**
     * To get  all product
     * @param   
     * @return  all product collection 
     */
    public function listOrder()
    {
        $listOrders = Order::with('orderProducts')->get();
        return $listOrders; 
    }
    public function showDetails($id)
    {
        $listOrders = Order::where('id' , $id)->with(['orderProducts' , 'deliverAddress', 'User'])->first();
        return $listOrders; 
    }
    public function updateDeliveryDate($formData)
    {
        $updateOrder = Order::where(['id' => $formData['order_id']])->update([ 'delivery_date' => $formData['deliveryDate'] ]);
    }
    public function updateDeliveryStatus ($formData)
    {
        $updateOrder = Order::where(['id' => $formData['order_id']])->update([ 'delievery_status' => $formData['deliveryStatus'] ]);
    }
}