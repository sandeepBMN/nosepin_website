<?php 
namespace Modules\Admin\Repository;

use Modules\Admin\Entities\Category;
use Modules\Admin\Entities\Attachment;
use Modules\Admin\Entities\Language;
use Modules\Admin\Services\CategoryService;
use Auth;
use Validator;
use Input;
use Hash;
use File;
use Session;
use App\Helpers\ImageHelper;
use Image;

class CategoryRepository 
{

     /**
     * To get  all category
     * @param   
     * @return  all category collection 
     */
    public function allCategory()
    {
       $category =  Category::with('attachments')->get();
       
       return $category;
    }
      /**
     * To get  parent category where parentid = 0
     * @param   
     * @return  all category collection 
     */
    public function parentCategory()
    {
       $parentcategory =  Category::with('childCategory','attachments')->where('parent_id',0)->get();

       return $parentcategory;
    }
     /**
     * To save  a new category
     * @param  array $data 
     * @return  array 
     */
    public function saveCategory($data)
    {
        $lang =  Session::get('type');
       $newdata = $data->all();
       // dd($newdata);
       $category  =[];
        if(array_key_exists('main_cat',$newdata) && $newdata['main_cat'] != '')
        {
            $isExistCategory = Category::find($data['main_cat']);
            if($isExistCategory != null)
            {
                $category['parent_id'] = $data['main_cat'];
            }
        }
        else{
            $category['parent_id'] = 0;
        }
         if(array_key_exists('savehindi',$newdata))
        {
            $saveCategorylang =[
                'text' => $newdata['name'],
                'attachable_model' => 'Category',
                'language' => $lang,
                'type'     => 'name',
                'attachable_id' => $newdata['categoryid']
            ];
        }else{
           $category['name'] =  $newdata['name'];
        }

       if (array_key_exists('save',$newdata) ||  array_key_exists('addhindi',$newdata)){
            $category =  Category::create($category);
            $categoryid = $category->id;
            if (array_key_exists('cat_image',$newdata) && $newdata['cat_image'] != null){ 
                // Get image file
                $destinationPath = 'uploads/category/';
                $image = $data->file('cat_image');
                $filepath =   $this->saveAttach($image,$destinationPath,$categoryid);
    
            }  
            if(array_key_exists('save', $newdata))
           {
            return ['status'=>true,'type'=> 'en','btn'=>'save'];
           }
            if(array_key_exists('addhindi', $newdata))
           {
            return ['status'=>true,'type'=> 'en','btn' => 'addhindi','data'=> $categoryid];
           }
       }
            
        if(array_key_exists('savehindi', $newdata))
        {
            // $saveCategorylang['attachable_id'] = $newdata['categoryid'];
            Language::create($saveCategorylang);
            return ['status'=>true,'type' => 'hn','btn'=> 'savehindi'];
        }
    } 

    public function saveAttach($image,$path,$id){
        $saveImageTypesArray = ['original','medium','thumbs'];
     
        $model = Category::find($id);
        if($model != null){
            foreach($saveImageTypesArray as $index => $type){

                $fileName = time().rand(1111,9999) . '.' . $image->getClientOriginalExtension();
                $readImage = Image::make($image);

                $fname = $image->getClientOriginalName();
                $ext = $image->getClientOriginalExtension();
                $name = time();
        
                $filePath = $path.$id.'/'.$type.'/';
                    if (!file_exists($filePath)) {
                        mkdir($filePath, 0755, true);
                    }

                if($type == 'original')
                {
                    $readImage->save($filePath.$fname);
                   
                }elseif($type == 'medium'){
                    $readImage->resize(350,350)->save($filePath.$fname);
                }elseif($type == 'thumbs')
                {
                    $readImage->resize(150,150)->save($filePath.$fname);
                }
                    
                $attachment = new Attachment;
                $attachment->image_name = $fname;
                $attachment->storage_path ='uploads\\category\\'.$id.'\\'.$type;
                $attachment->type = $type;
                $model->attachments()->save($attachment);
           }
           return true;
        }
        else{
            return false;
        }
    }
    /**
     * To save  a new state 
     * @param  array $data , state id $id
     * @return  array 
     */
    public function updateCategory($data,$id,$lang)
    {
        $newdata = $data->all();

        $cat = Category::with(['attachments'=> function($query){ 
            $data = $query->where(['type' => 'original'])->get();
        }])->find($id);
        
        $langcat = Language::where('attachable_id',$id)->where('attachable_model','Category')->first();

        if(array_key_exists('updatehindi',$newdata))
        {
                $saveCategorylang =[
                    'text' => $newdata['name'],
                    // 'attachable_model' => $langcat['attachable_model'],
                    // 'language' => $lang,
                    // 'type'     => $langcat['type'],
                ];
        }

            $category  =[];
        if(array_key_exists('main_cat',$newdata) && $newdata['main_cat'] != '')
        {
            $isExistCategory = Category::find($newdata['main_cat']);
            if($isExistCategory != null)
            {
                $category['parent_id'] = $newdata['main_cat'];
            }
        }
        else{
            $category['parent_id'] = 0;
        }
        if($cat->attachments->count() ==0 )
        {
            if(array_key_exists('cat_image',$newdata) && $newdata['cat_image'] != null)
            {
                 if (array_key_exists('update',$newdata)){
                     $category['name'] =  $newdata['name'];
                        $category =  Category::where('id',$id)->update($category);  
                         $destinationPath = 'uploads/category/';
                        $image = $data->file('cat_image');
                        $filepath =   $this->saveAttach($image,$destinationPath,$id);
                         return ['status'=>true,'type'=> 'en','btn'=>'update'];
                    }

            }
       }
       if($cat->attachments->count() > 0)
       {
            if(!array_key_exists('cat_image',$newdata))
            {
                 if (array_key_exists('update',$newdata)){
                     $category['name'] =  $newdata['name'];
                        $category =  Category::where('id',$id)->update($category);
                         return ['status'=>true,'type'=> 'en','btn'=>'update'];  
                    }       
            }else{
                return ['status' => false,'message'=> 'Only one file is Allowed'];
            }    
       }

            
           //  if (array_key_exists('update',$newdata)){
           //      $category['name'] =  $newdata['name'];
           //      $category =  Category::where('id',$id)->update($category);
                
           //      if (array_key_exists('cat_image',$newdata) && $newdata['cat_image'] != null){ 
           //          // Get image file
           //          $destinationPath = 'uploads/category/';
           //          $image = $data->file('cat_image');
           //          $filepath =   $this->saveAttach($image,$destinationPath,$id);
           //      }  
           //      return ['status'=>true,'type'=> 'en','btn'=>'update'];
           // }
                
            if(array_key_exists('updatehindi', $newdata))
            {
                Language::where('attachable_id',$id)->where('attachable_model','Category')->update($saveCategorylang);
                return ['status'=>true,'type' => 'hn','btn'=> 'updatehindi'];
            }
        
    }

    /**
     * To Delete a state 
     * @param  state id $id 
     * @return  array 
     */
    public function deleteCategory($id){

        $getCategory = Category::find($id);
        $getCategory->delete();
        return true;
    }

     /**
     * To get subcategory
     * @param   $id
     * @return   
     */
    public function getSubCategory($id)
    {
       $category =  Category::with('attachments')->where('parent_id',$id)->get();
       return $category;
    }

    public function deleteCategoryAttachment($data)
    {
        $category =  Category::where('id',$data['id'])->first();
        if($category != null)
        {
            if(isset($category->attachments))
            {
                $attachments = $category->attachments;
                foreach ($attachments as $key => $value) {

                    unlink($value->storage_path.'\\'.$value->image_name);
                    Attachment::where('id',$value->id)->delete();
                }
                 return ['status'=>true,'message'=>'Attachment deleted successfully!'];
            }else{
                 return ['status'=>false,'message'=>'No attachment found'];
            }
        }
        else{
           return ['status'=>false,'message'=>'No Category found'];
        }
    }
}