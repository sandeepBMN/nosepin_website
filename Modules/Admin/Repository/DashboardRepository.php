<?php 
namespace Modules\Admin\Repository;

use Modules\Admin\Entities\GoldPrice;
use Modules\Admin\Services\DashboardService;
use Auth;
use Validator;
use Input;
use Hash;

class DashboardRepository 
{

	/**
     * To save  a new gold price 
     * @param  array $data 
     * @return  array 
     */
    public function saveGoldPrice($data)
    {
       $state = GoldPrice::create([
            'gold_price'   => $data['goldprice'],
        ]);
        return true;
    } 

}