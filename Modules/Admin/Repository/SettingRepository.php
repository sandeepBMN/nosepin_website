<?php 
namespace Modules\Admin\Repository;

use Modules\Admin\Entities\Setting;

use Modules\Admin\Services\SettingService;
use Auth;
use Session;

class SettingRepository 
{

	/**
     * To save  a new gold price 
     * @param  array $data 
     * @return  array 
     */
    public function saveGoldPrice($data)
    {
    	if($data)
    	{
    		
	    	if(array_key_exists('goldprice', $data))
	    	{
	    		$key = 'goldprice';
	    	}else{
	    		$key='';
	    	}
	    	$value = $data['goldprice'];
	       	$setting = Setting::create([
	            'key'   => $key,
	            'value'   => $value,
	        ]);
	        return true;
	    		
    	}else{
    		return false;
    	}
    } 

    /**
     * To save  a new gold price 
     * @param  array $data 
     * @return  array 
     */
    public function saveGst($data)
    {
    	if($data)
    	{
	    	if(array_key_exists('gst', $data))
	    	{
	    		$key = 'gst';
	    	}else{
	    		$key='';
	    	}
	    	$value = $data['gst'];
	       	$setting = Setting::create([
	            'key'   => $key,
	            'value'   => $value,
	        ]);
	        return true;
	    		
    	}else{
    		return false;
    	}
    } 


     /**
     * To  get a goldprice function.
     *
     * @param  $request
     * @return 
     */
    public static function getGoldPrice()
    {   
       $goldprice = Setting::orderBy('id', 'DESC')->where('key','goldprice')->first();

       return $goldprice;
    }  

}