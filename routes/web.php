<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
	Route::get('/login', 'website\Auth\LoginController@login')->name('login');
	Route::POST('/login', 'website\Auth\LoginController@Createlogin')->name('create.login');
	Route::get('/logout', 'website\Auth\LoginController@Logout')->name('page.logout');
	Route::POST('/otp', 'website\Auth\LoginController@VerifyOtp')->name('verify.otp');
	
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/', 'website\HomeController@index')->name('page.index');
	Route::get('/products', 'website\HomeController@products')->name('page.products');
	Route::get('/product/detail/{id}', 'website\HomeController@productdetail')->name('page.products.detail');
	Route::get('/products/{id}', 'website\HomeController@products')->name('page.category.products');
	Route::get('/categories', 'website\HomeController@categories')->name('page.categories');
	Route::get('/categories/{id}', 'website\HomeController@categories')->name('page.subCategories');
	Route::get('/states', 'website\HomeController@states')->name('page.states');

	Route::group(['middleware' => 'AuthCheck'],function(){
		Route::get('/cart', 'website\HomeController@cart')->name('page.cart');
		Route::POST('/cart', 'website\HomeController@saveCart')->name('create.cart');
		Route::get('/cart/item/delete/{id}', 'website\HomeController@cartItemDelete')->name('cart.item.delete');
		Route::POST('/cart/item/update', 'website\HomeController@updateCart')->name('update.cart');
		Route::get('/my-account', 'website\HomeController@myAccount')->name('page.myAccount');
		Route::POST('/save/profile', 'website\HomeController@saveProfile')->name('save.profile');
		Route::get('/checkout', 'website\HomeController@checkout')->name('page.checkout');
		Route::POST('/create/address', 'website\HomeController@saveAddress')->name('create.address');
		Route::POST('/save/orders', 'website\HomeController@saveOrder')->name('save.order');
	});