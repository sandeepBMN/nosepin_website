<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['prefix'=>'v1'], function(){

	Route::get('/payment/showform','PaymentController@showPaymentForm');
	Route::post('/payment/success','PaymentController@paymentSuccess')->name('payment.success');
	Route::post('/payment/fail','PaymentController@paymentFail')->name('payment.fail');
	Route::post('login','Auth\LoginController@authenticate');
	Route::get('user/activation/{otp}/{id}','Auth\LoginController@activate')->name('user.activate');
	Route::get('/parentcategory','CategoryController@parentCategory');
	Route::get('/state','StateController@allState');
	Route::group(['middleware' => 'auth:api'],function(){
		Route::get('/category/{id?}','CategoryController@allCategory');
		Route::group(['prefix'=>'profile'], function(){
				Route::put('update','UserProfileController@updateProfile');
				Route::get('/','UserProfileController@getUser');
		});
		Route::group(['prefix' => 'category'], function(){
		});
		Route::group(['prefix' => 'product'], function(){
			Route::get('/{id?}','ProductController@allProduct');
			Route::get('/detail/{id}','ProductController@productDetail');
			Route::get('/{category_id}','ProductController@getProductByCategory');
		});
		Route::post('/save/address','AddressController@saveAddress');
		Route::get('/address','AddressController@getAddress');
		Route::delete('/delete/address/{id}','AddressController@deleteAddress');

		// ---------orders-----------
		Route::post('/save/order','PaymentController@saveOrder');
		Route::get('orders/{page}','PaymentController@listOrder');

		Route::get('/gethotproducts','ProductController@getHotProducts');
		Route::get('/getgoldprice','PaymentController@getGoldPrice');

		Route::get('/getorderproducts/{invoice}','PaymentController@getOrderProducts');
		
		//for payment gateway
		// Route::group(['prefix' => 'payment'], function(){
			// 	Route::get('/success','PaymentController@paymentSuccess')->name('payment.success');
			// 	Route::get('/fail','PaymentController@paymentFail')->name('payment.fail');
			// });
			Route::POST('/getPrice','PriceController@processPrice');
			Route::POST('/process/usercart','PriceController@processUserCart');
	});
});