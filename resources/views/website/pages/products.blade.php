@extends('website.layouts.master')
@section('content')
        <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Shop</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Products</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Hiraola's Breadcrumb Area End Here -->

        <!-- Begin Hiraola's Content Wrapper Area -->
        <div class="hiraola-content_wrapper">
            <div class="container">
                <div class="row">
                 
                    <div class="col-lg-12 order-1 order-lg-2">
                        <div class="shop-toolbar">
                            <div class="product-view-mode ">
                                <a class="active grid-3" data-target="gridview-3" data-toggle="tooltip" data-placement="top" title="Grid View"><i class="fa fa-th"></i></a>
                                <a class="list" data-target="listview" data-toggle="tooltip" data-placement="top" title="List View"><i class="fa fa-th-list"></i></a>
                            </div>
                        </div>
                        <div class="shop-product-wrap grid gridview-3 row">
                        @if($products->count() > 0)
                            @foreach($products as $key => $product)
                            <div class="col-lg-4">
                                <div class="slide-item">
                                    <div class="single_product">
                                        <div class="product-img">
                                            <a href="{{route('page.products.detail',base64_encode($product['id'])) }}">
                                                @if($product->attachments->count() > 0)
                                                    @if($product->attachments->count() == 1)
                                                        <img class="primary-img" src="{{ asset($product->attachments[0]['path']) }}" alt="Hiraola's Product Image">
                                                        
                                                    @endif
                                                     @if($product->attachments->count() >= 2)
                                                        <img class="primary-img" src="{{ asset($product->attachments[0]['path']) }}" alt="Hiraola's Product Image">
                                                        <img class="secondary-img" src="{{ asset($product->attachments[1]['path']) }}" alt="Hiraola's Product Image">
                                                    @endif     
                                                @else
                                                    <img class="primary-img" src="assets/images/product/medium-size/1-9.jpg" alt="Hiraola's Product Image">
                                                    <img class="secondary-img" src="assets/images/product/medium-size/1-8.jpg" alt="Hiraola's Product Image">
                                                @endif    
                                            </a>
                                            <div class="add-actions">
                                                <ul>
                                                    <li><a class="hiraola-add_cart" href="{{ route('page.cart') }}" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag"></i></a>
                                                    </li>
                                                  
                                                    <li class="quick-view-btn"><a href="{{route('page.products.detail',base64_encode($product['id'])) }}"  title="View Detail"><i
                                                            class="ion-eye"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="hiraola-product_content">
                                            <div class="product-desc_info">
                                                <h6><a class="product-name" href="{{route('page.products.detail',base64_encode($product['id'])) }}">{{ $product['name'] }}</a></h6>
                                                <div class="price-box">
                                                    <span class="new-price">Weight:<small>{{ $product['weight'] }} </small><span style="text-transform: lowercase;"> g</span></span>
                                                </div>
                                                <div class="additional-add_action">
                                                    <ul>
                                                        <li><a class="hiraola-add_compare" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Add To Wishlist">Labour:<small>{{ $product['labour'] }}</small></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-slide_item">
                                    <div class="single_product">
                                        <div class="product-img">
                                            <a href="{{route('page.products.detail',base64_encode($product['id'])) }}">
                                                 @if($product->attachments->count() > 0)
                                                    @if($product->attachments->count() == 1)
                                                        <img class="primary-img" src="{{ asset($product->attachments[0]['path']) }}" alt="Hiraola's Product Image">
                                                    @endif
                                                     @if($product->attachments->count() >= 2)
                                                        <img class="primary-img" src="{{ asset($product->attachments[0]['path']) }}" alt="Hiraola's Product Image">
                                                        <img class="secondary-img" src="{{ asset($product->attachments[1]['path']) }}" alt="Hiraola's Product Image">
                                                    @endif     
                                                @else
                                                    <img class="primary-img" src="assets/images/product/medium-size/1-9.jpg" alt="Hiraola's Product Image">
                                                    <img class="secondary-img" src="assets/images/product/medium-size/1-8.jpg" alt="Hiraola's Product Image">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="hiraola-product_content">
                                            <div class="product-desc_info">
                                                <h6><a class="product-name" href="{{route('page.products.detail',base64_encode($product['id'])) }}">{{ $product['name'] }}</a></h6>
                                               
                                                <div class="price-box">
                                                    <span class="new-price">Weight:<small>{{ $product['weight'] }}</small></span>
                                                </div>
                                                <div class="product-short_desc">
                                                    <p>{{ $product['description']}}</p>
                                                </div>
                                            </div>
                                            <div class="add-actions">
                                                <ul>
                                                    <li><a class="hiraola-add_cart" href="{{ route('page.cart') }}" data-toggle="tooltip" data-placement="top" title="Add To Cart">Add To Cart</a></li>
                                                   
                                                    <li class="quick-view-btn"><a href="{{route('page.products.detail',base64_encode($product['id'])) }}"  title="View Detail"><i
                                                            class="ion-eye"></i></a></li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                         @else
                            <div class="col-lg-12">
                                <h3>No product available</h3>
                            </div>
                        @endif
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="hiraola-paginatoin-area">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <ul class="hiraola-pagination-box" id="pagination">
                                                <li class="active"><a href="javascript:void(0)">1</a></li>
                                                <li><a href="javascript:void(0)">2</a></li>
                                                <li><a href="javascript:void(0)">3</a></li>
                                                <li><a class="Next" href="javascript:void(0)"><i
                                                        class="ion-ios-arrow-right"></i></a></li>
                                                <li><a class="Next" href="javascript:void(0)">>|</a></li>
                                            </ul>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('addjavascript')
<script type="text/javascript">
   
    $(document).ready(function(){
        jQuery(function($) {
            var items = $(".shop-product-wrap .col-lg-4");
            var numItems = items.length;
            var perPage = 12;
            var pagination_placeholder_selector = "#pagination"; // put in a variable to ensure proper changes in the future
            var myPageName = "#page-"; // a number will follow for each page
            items.slice(perPage).hide();
            $(pagination_placeholder_selector).pagination({
                items: numItems,
                itemsOnPage: perPage,
                cssStyle: "light-theme",
                hrefTextPrefix: myPageName,
                onPageClick: function(pageNumber) { // this is where the magic happens
                    var showFrom = perPage * (pageNumber - 1);
                    var showTo = showFrom + perPage;
                    items.hide() // first hide everything, then show for the new page
                         .slice(showFrom, showTo).show();
                }
            });
            var checkFragment = function() {
            var hash = window.location.hash || (myPageName+"1");
            var re = new RegExp("^"+myPageName+"(\\d+)$");
            hash = hash.match(re);
            if(hash)
                $(pagination_placeholder_selector).pagination("selectPage", parseInt(hash[1]));
              };
            $(window).bind("popstate", checkFragment);
            checkFragment();
        }); 
    });
 
</script>
@endsection