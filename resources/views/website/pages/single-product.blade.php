 @extends('website.layouts.master')
@section('content')
        <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Product Detail</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Product Detail</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="sp-area">
            <div class="container">
                <div class="sp-nav">
                    <div class="row">
                        <div class="col-lg-5 col-md-5">
                            @if(sizeof($detailProduct['attachments']) > 0)
                            <div class="sp-img_area">
                                <div class="zoompro-border">
                                    <img class="zoompro" src="{{ asset($detailProduct['attachments'][0]['path']) }}" data-zoom-image="{{ asset($detailProduct['attachments'][0]['path']) }}" alt="Hiraola's Product Image" />
                                </div>
                                <div id="gallery" class="sp-img_slider">
                                    @foreach($detailProduct['attachments'] as $key => $value )
                                    <a class="active" data-image="{{ asset($value['path']) }}" data-zoom-image="{{ asset($value['path']) }}">
                                        <img src="{{ asset($value['path']) }}" alt="Hiraola's Product Image">
                                    </a>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-lg-7 col-md-7">
                            <form action="{{ route('create.cart') }}" method="POST">
                                @csrf
                                <div class="sp-content">
                                    <div class="sp-heading">
                                        <h5><a href="#">{{ $detailProduct['name'] }}</a></h5>
                                        <p>{{ $detailProduct['description'] }}</p>
                                    </div>
                                    <div class="sp-essential_stuff">
                                        <ul>
                                            <li>Weight: <a href="javascript:void(0)"><span>{{ $detailProduct['weight'] }}<span style="text-transform: lowercase;"> g </span></span></a></li>
                                        </ul>
                                    </div>
                                    <div class="product-size_box">
                                        <span>Purity</span>
                                            <?php
                                                $purities = json_decode( $detailProduct['purity'] , true);
                                            ?>
                                        <select name="purity" class="myniceselect nice-select">
                                           @foreach($purities as $key =>$purity)
                                              <option value="{{ $purity }}">{{ $purity }}</option>
                                            @endforeach
                                        </select> %
                                    </div>
                                   
                                     <div class="quantity">
                                        <label>Quantity</label>
                                        <div class="cart-plus-minus">
                                            <input class="cart-plus-minus-box" value="1" type="number" name="quantity" readonly>
                                            <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                                            <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
                                        </div>
                                    </div>
                                    <input value="{{ $detailProduct['id'] }}" type="hidden" name="product_id">
                                    <div class="m-t-10">
                                        <button class="hiraola-btn cartReqSubmit" class="qty-cart_btn cartReqSubmit" >Add To Cart</button></li>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="hiraola-product-tab_area-2 sp-product-tab_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="sp-product-tab_nav ">
                            <div class="product-tab">
                                <ul class="nav product-menu">
                                    <li><a class="active" data-toggle="tab" href="#description"><span>Description</span></a>
                                    </li>
                                    <li><a data-toggle="tab" href="#specification"><span>Specification</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content hiraola-tab_content">
                                <div id="description" class="tab-pane active show" role="tabpanel">
                                    <div class="product-description">
                                        <ul>
                                            <li>
                                                <strong>{{ $detailProduct['name'] }}</strong>
                                                <span>{{ $detailProduct['description'] }}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="specification" class="tab-pane" role="tabpanel">
                                    <table class="table table-bordered specification-inner_stuff">
                                        <tbody>
                                            <tr>
                                                <td colspan="2"><strong>Memory</strong></td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td>test 1</td>
                                                <td>8gb</td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td colspan="2"><strong>Processor</strong></td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td>No. of Cores</td>
                                                <td>1</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hiraola-product_area hiraola-product_area-2 section-space_add">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hiraola-section_title">
                            <h4>Related Products</h4>
                        </div>

                    </div>
                    <div class="col-lg-12">
                        <div class="hiraola-product_slider-3">
                            {{-- {{ dd($products) }} --}}
                            @foreach($products as $key => $product)
                            <div class="slide-item">
                                <div class="single_product">
                                    <div class="product-img">
                                         <a href="{{route('page.products.detail',base64_encode($product['id'])) }}">
                                            @if($product->attachments->count() > 0)
                                                @if($product->attachments->count() == 1)
                                                    <img class="primary-img" src="{{ asset($product->attachments[0]['path']) }}" alt="Hiraola's Product Image">
                                                    
                                                @endif
                                                 @if($product->attachments->count() >= 2)
                                                    <img class="primary-img" src="{{ asset($product->attachments[0]['path']) }}" alt="Hiraola's Product Image">
                                                    <img class="secondary-img" src="{{ asset($product->attachments[1]['path']) }}" alt="Hiraola's Product Image">
                                                @endif     
                                            @else
                                                <img class="primary-img" src="assets/images/product/medium-size/1-9.jpg" alt="Hiraola's Product Image">
                                                <img class="secondary-img" src="assets/images/product/medium-size/1-8.jpg" alt="Hiraola's Product Image">
                                            @endif    
                                        </a>
                                        {{-- <span class="sticker-2">Sale</span> --}}
                                        <div class="add-actions">
                                            <ul>
                                                <li><a class="hiraola-add_cart" href="#" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag"></i></a></li>
                                                <li class="quick-view-btn" ><a href="{{route('page.products.detail',base64_encode($product['id'])) }}" data-placement="top" title="Quick View"><i class="ion-eye"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="hiraola-product_content">
                                        <div class="product-desc_info">
                                            <h6><a class="product-name" href="{{route('page.products.detail',base64_encode($product['id'])) }}">{{ $product['name'] }}</a></h6>
                                            <div class="price-box">
                                                <span class="new-price">Weight: {{ $product['weight'] }}</span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('addjavascript')
<script type="text/javascript">
    $(document).on('click','.qtybutton',function(){
            $(this).parents('.quantity').find('p').hide();
        var qty = $(this).parents('.cart-plus-minus ').find('.cart-plus-minus-box').val();
        console.log(qty);
        if (qty < 4) {
            $(this).parents('.cart-plus-minus').css('box-shadow','0.3px 0px 5px red');
            $(this).parents('.quantity').append('<p class="text-danger">Please add more then 4 quantity</p>');
        }
    }); 
    $(document).on('click','.cartReqSubmit',function(){
            $(this).parents('.sp-content ').find('.text-danger').hide();
        var qty = $(this).parents('.sp-content ').find('.cart-plus-minus-box').val();
        if (qty < 4) {
            $(this).parents('.sp-content ').append('<p class="text-danger">Please add more then 4 quantity</p>');
            return false;
        }
    });  
 
</script>

@endsection