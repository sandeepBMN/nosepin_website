@extends('website.layouts.master')
@section('content')
        <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Other</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Cart</li>
                    </ul>
                </div>
            </div>
        </div>
        {{-- {{ dd($cartItems) }} --}}
        <div class="hiraola-cart-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">

                        @if(sizeof($cartItems) > 0)
                        <form action="javascript:void(0)">
                            <div class="table-content table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="hiraola-product-remove">remove</th>
                                            <th class="hiraola-product-thumbnail">images</th>
                                            <th class="cart-product-name">Product</th>
                                            <th class="cart-product-name">Purity</th>
                                            <th class="hiraola-product-price">Weight</th>
                                            <th class="hiraola-product-quantity">Quantity</th>
                                            <th class="hiraola-product-subtotal">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody class="mainClass">
                                        <?php
                                            $subTotal = 0;
                                            $total = '';
                                        ?>
                                        @foreach($cartItems as $key =>$items)

                                            <?php
                                                $total = $subTotal + $items['totalAmount'];
                                                $subTotal = $total;
                                            ?>
                                        <tr class="amnt{{$key+1}}">
                                            <td class="hiraola-product-remove"><a href="{{route('cart.item.delete',$items['cartId']) }}"><i class="fa fa-trash"
                                                title="Remove"></i></a></td>
                                            <td class="hiraola-product-thumbnail">
                                                <a href="javascript:void(0)">
                                                    @if($items['attachment'] != null)
                                                        <img src="{{ asset($items['attachment']) }}" width="100px">
                                                    @else
                                                        <img src="{{ asset('assets/images/noimage.jpg') }}" width="100px">
                                                    @endif
                                                </a>
                                            </td>
                                            <td class="hiraola-product-name"><a href="javascript:void(0)">{{ $items['name'] }}</a></td>
                                            <td class="hiraola-product-name"><a href="javascript:void(0)">{{ $items['purity'] }} %</a></td>
                                            <td class="hiraola-product-price"><span class="amount">{{ $items['totalWeight'] }} g </span></td>
                                            <td class="quantity">
                                                <label>Quantity</label>
                                                <div class="cart-plus-minus">
                                                    <input class="cart-plus-minus-box" value="{{ $items['qty'] }}" min="4" type="number" name="quantity" readonly>
                                                    <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                                                    <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
                                                </div>
                                            </td>
                                            <td class="product-subtotal">₹ <span class="amount amount{{$key+1 }}">{{ $items['totalAmount'] }}</span>
                                                <input type="hidden" class="totalAmount" value="{{ $items['totalAmount'] }}">
                                                <input type="hidden" class="itemId" value="{{$items['cartId']}}">
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                         {{--    <div class="row">
                                <div class="col-12">
                                    <div class="coupon-all">
                                        <div class="coupon">
                                            <input id="coupon_code" class="input-text" name="coupon_code" value="" placeholder="Coupon code" type="text">
                                            <input class="button" name="apply_coupon" value="Apply coupon" type="submit">
                                        </div>
                                        <div class="coupon2">
                                            <input class="button" name="update_cart" value="Update cart" type="submit">
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="row">
                                <div class="col-md-5 ml-auto">
                                    <div class="cart-page-total">
                                        <?php

                                        ?>
                                        <h2>Cart totals</h2>
                                        <ul>
                                            {{-- <li>Subtotal <span>$118.60</span></li> --}}
                                            <li>Total <span>₹ &nbsp;<span class="totalPrice">{{ $total }}</span></span></li>
                                        </ul>
                                        <a class="btncheckout" href="{{ route('page.checkout') }}">Proceed to checkout</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        @else
                            <h3>Cart is empty</h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('addjavascript')
<script type="text/javascript">
    $(document).on('click','.qtybutton',function(){
        var qty = $(this).parents('.cart-plus-minus ').find('.cart-plus-minus-box').val();
        var price = $(this).parents('.quantity').siblings('.product-subtotal').find('.totalAmount').val();
        var itemId = $(this).parents('.quantity').siblings('.product-subtotal').find('.itemId').val();
        var itemTotal =  qty * price;
        $(this).parents('.quantity').siblings('.product-subtotal').find('.amount').html();
        // $(this).parents('.quantity').siblings('.product-subtotal').find('.amount').text(itemTotal);
        var mainlength = $(this).parents('.mainClass').find('tr').length;
            var subTotal = 0;
        for (var i = 1; mainlength >= i ; i++) {
            var subPrice = $('.mainClass').children('.amnt'+i+'').find('.amount'+i+'').text();
            var subTotal = (parseInt(subTotal)+parseInt(subPrice));
                var total =  subTotal;
            $('.cart-page-total').find('.totalPrice').html();
            $('.cart-page-total').find('.totalPrice').text(total);
        }
        if (qty > 4) {
            $.ajax('/cart/item/update', {
                type: 'POST',
                data: { quantity:qty,price:itemTotal,itemId:itemId,"_token": "{{ csrf_token() }}" },
                success: function (success) {
                    location.reload();
                }
            });
        }else{
            $(this).parents('.cart-plus-minus').css('box-shadow','0.3px 0px 5px red');
            $(this).parents('.quantity').css('border','2px solid #e84b4bb0');
        }
    });  
 
</script>

@endsection