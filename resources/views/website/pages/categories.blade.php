@extends('website.layouts.master')
@section('content')
        <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Shop</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Categories</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="hiraola-content_wrapper">
            <div class="container">
                <div class="row">
                    @if($category->count() > 0)
                    <div class="col-lg-12 order-1 order-lg-2">
                        <div class="shop-product-wrap grid gridview-3 row">
                            @foreach($category as $key => $value)
                            <div class="col-lg-3">
                                <div class="slide-item">
                                    @if($value->childCategory->count() > 0)
                                    <div class="single_product">
                                        <div class="product-img">
                                            <a href="{{route('page.subCategories',base64_encode($value['id']))}}">
                                                <img class="primary-img" src="{{$value['cat_image']}}" >
                                            </a>
                                        </div>
                                        <div class="hiraola-product_content">
                                            <div class="product-desc_info">
                                                <h6><a class="product-name" href="{{route('page.subCategories',base64_encode($value['id']))}}">{{$value['name'] }}</a></h6>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="single_product">
                                        <div class="product-img">
                                            <a href="{{route('page.category.products',base64_encode($value['id']))}}">
                                                <img class="primary-img" src="{{$value['cat_image']}}" >
                                            </a>
                                        </div>
                                        <div class="hiraola-product_content">
                                            <div class="product-desc_info">
                                                <h6><a class="product-name" href="{{route('page.category.products',base64_encode($value['id']))}}">{{$value['name'] }}</a></h6>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                             
                            </div>
                            @endforeach
                           
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="hiraola-paginatoin-area">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <ul class="hiraola-pagination-box" id="pagination">
                                                <li class="active"><a href="javascript:void(0)">1</a></li>
                                                <li><a href="javascript:void(0)">2</a></li>
                                                <li><a href="javascript:void(0)">3</a></li>
                                                <li><a class="Next" href="javascript:void(0)"><i
                                                        class="ion-ios-arrow-right"></i></a></li>
                                                <li><a class="Next" href="javascript:void(0)">>|</a></li>
                                            </ul>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                         <div class="col-lg-12 order-1 order-lg-2">
                            <div class="shop-product-wrap grid gridview-3 row">
                                <h6>No Category Available</h6>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('addjavascript')
<script type="text/javascript">
   
    $(document).ready(function(){
        jQuery(function($) {
            var items = $(".shop-product-wrap .col-lg-3");
            var numItems = items.length;
            var perPage = 12;
            var pagination_placeholder_selector = "#pagination"; // put in a variable to ensure proper changes in the future
            var myPageName = "#page-"; // a number will follow for each page
            items.slice(perPage).hide();
            $(pagination_placeholder_selector).pagination({
                items: numItems,
                itemsOnPage: perPage,
                cssStyle: "light-theme",
                hrefTextPrefix: myPageName,
                onPageClick: function(pageNumber) { // this is where the magic happens
                    var showFrom = perPage * (pageNumber - 1);
                    var showTo = showFrom + perPage;
                    items.hide() // first hide everything, then show for the new page
                         .slice(showFrom, showTo).show();
                }
            });
            var checkFragment = function() {
            var hash = window.location.hash || (myPageName+"1");
            var re = new RegExp("^"+myPageName+"(\\d+)$");
            hash = hash.match(re);
            if(hash)
                $(pagination_placeholder_selector).pagination("selectPage", parseInt(hash[1]));
              };
            $(window).bind("popstate", checkFragment);
            checkFragment();
        }); 
    });
 
</script>
@endsection