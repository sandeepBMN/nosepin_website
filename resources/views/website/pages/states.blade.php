@extends('website.layouts.master')
@section('content')

        <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Other</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li class="active">States</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Hiraola's Breadcrumb Area End Here -->
        <!--Begin Hiraola's Wishlist Area -->
        <div class="hiraola-wishlist_area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <form action="javascript:void(0)">
                            <div class="table-content table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="hiraola-product_remove">#</th>
                                            <th class="hiraola-product-thumbnail">States</th>
                                            <th class="cart-product-name">Location</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($states->count() > 0)
                                        @foreach($states as $key => $state)
                                        <tr>
                                            <td class="hiraola-product_remove"><a href="javascript:void(0)">{{ $key+1 }}</a></td>
                                            <td class="hiraola-product-thumbnail"><a href="javascript:void(0)">{{ $state['name'] }}</a>
                                            </td>
                                            <td class="hiraola-product-name"><a href="javascript:void(0)"><i class="fa fa-location-arrow"></i></a></td>
                                        </tr>
                                        @endforeach
                                         @else
                                         <div class="col-lg-12 order-1 order-lg-2">
                                            <div class="shop-product-wrap grid gridview-3 row">
                                                <h6>No State Available</h6>
                                            </div>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection