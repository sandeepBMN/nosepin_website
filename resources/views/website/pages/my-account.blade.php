 @extends('website.layouts.master')
@section('content')
        <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Other</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li class="active">My Account</li>
                    </ul>
                </div>
            </div>
        </div>
        <main class="page-content">
            <div class="account-page-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <ul class="nav myaccount-tab-trigger" id="account-page-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="account-dashboard-tab" data-toggle="tab" href="#account-dashboard" role="tab" aria-controls="account-dashboard" aria-selected="true">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="account-orders-tab" data-toggle="tab" href="#account-orders" role="tab" aria-controls="account-orders" aria-selected="false">Orders</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="account-address-tab" data-toggle="tab" href="#account-address" role="tab" aria-controls="account-address" aria-selected="false">Addresses</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="account-logout-tab" href="{{route('page.logout') }}" role="tab" aria-selected="false">Logout</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-9">
                            <div class="tab-content myaccount-tab-content" id="account-page-tab-content">
                                <div class="tab-pane fade show active" id="account-dashboard" role="tabpanel" aria-labelledby="account-dashboard-tab">
                                    <div class="myaccount-details">
                                        <form action="{{ route('save.profile') }}" method="POST" class="hiraola-form">
                                            @csrf
                                            <div class="hiraola-form-inner">
                                                <div class="single-input single-input-half">
                                                    <label for="account-details-firstname">Name*</label>
                                                    <input type="text" id="account-details-firstname" name="name" value="{{ $user['name'] }}" required>
                                                     @if($errors->has('name'))
                                                         <span class="text-danger">{{ $errors->first('name') }}</span>   
                                                    @endif
                                                </div>
                                                <div class="single-input single-input-half">
                                                    <label for="account-details-lastname">Mobile*</label>
                                                    <input type="number" id="account-details-lastname" value="{{ $user['mobile'] }}" readonly>

                                                </div>
                                                <div class="single-input">
                                                    <label for="account-details-email">Email*</label>
                                                    <input type="email" id="account-details-email" name="email" value="{{ $user['email'] }}" required>
                                                     @if($errors->has('email'))
                                                         <span class="text-danger">{{ $errors->first('email') }}</span>   
                                                    @endif
                                                </div>
                                                @if($user['password'] == null)
                                                <div class="single-input">
                                                    <label for="account-details-oldpass">Password</label>
                                                    <input type="password" id="account-details-oldpass" name="password">
                                                     @if($errors->has('password'))
                                                         <span class="text-danger">{{ $errors->first('password') }}</span>   
                                                    @endif
                                                </div>
                                                @else 
                                                <div class="single-input">
                                                    <label for="account-details-oldpass">Current Password(leave blank to leave
                                                        unchanged)</label>
                                                    <input type="password" id="account-details-oldpass" name="currentPass">
                                                     @if($errors->has('currentPass'))
                                                         <span class="text-danger">{{ $errors->first('currentPass') }}</span>   
                                                    @endif
                                                </div>
                                                <div class="single-input">
                                                    <label for="account-details-newpass">New Password (leave blank to leave
                                                        unchanged)</label>
                                                    <input type="password" id="account-details-newpass" name="newPass">
                                                     @if($errors->has('newPass'))
                                                         <span class="text-danger">{{ $errors->first('newPass') }}</span>   
                                                    @endif
                                                </div>
                                                <div class="single-input">
                                                    <label for="account-details-confpass" >Confirm New Password</label>
                                                    <input type="password" id="account-details-confpass" name="confirmPass">
                                                     @if($errors->has('confirmPass'))
                                                         <span class="text-danger">{{ $errors->first('confirmPass') }}</span>   
                                                    @endif
                                                </div>
                                                @endif
                                                <div class="single-input">
                                                    <button class="hiraola-btn hiraola-btn_dark" type="submit"><span>SAVE
                                                    CHANGES</span></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="account-orders" role="tabpanel" aria-labelledby="account-orders-tab">
                                    <div class="myaccount-orders">
                                        <h4 class="small-title">MY ORDERS</h4>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <tbody>
                                                    <tr>
                                                        <th>ORDER</th>
                                                        <th>DATE</th>
                                                        <th>STATUS</th>
                                                        <th>TOTAL</th>
                                                        <th></th>
                                                    </tr>
                                                    @foreach($listOrder['order'] as $key=>$order)
                                                    <tr>
                                                        <td><a class="account-order-id" href="javascript:void(0)">{{ $order['invoice_no'] }}</a></td>
                                                        <td>{{ $order['created_at'] }}</td>
                                                        {{-- <td>{{$order['delievery_status'] }}</td> --}}
                                                        <td>Processing</td>
                                                        <td>{{ $order['totalprice'] }} for {{ sizeof($order->orderProducts) }} items</td>
                                                        <td><a href="javascript:void(0)" class="hiraola-btn hiraola-btn_dark hiraola-btn_sm"><span>View</span></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="account-address" role="tabpanel" aria-labelledby="account-address-tab">
                                    <div class="myaccount-address">
                                        <p>The following addresses will be used on the checkout page by default.</p>
                                        <div class="row">
                                               
                                            @if(sizeof($address['userAddress']) > 0)
                                                @foreach($address['userAddress'] as $key => $address)
                                               <div class="col-md-4">
                                                    <div class="bg-color-gray min-h-170 btnClick p-10">
                                                        <input type="hidden" value="{{ $address['id'] }}" class="addressId">
                                                        <h4 class="small-title">{{ $address['address_type'] }}</h4>
                                                        <address>
                                                            {{ $address['address'] }}
                                                        </address>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @else
                                                <a href="javascript:;" class="col-md-12 mainClick">
                                                    <div class="bg-color-gray min-h-170 btnClick p-10">
                                                        <h4 class="small-title">You have no address Please create address</h4>
                                                        
                                                    </div>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="account-details" role="tabpanel" aria-labelledby="account-details-tab">
                                    <div class="myaccount-details">
                                        <form action="#" class="hiraola-form">
                                            <div class="hiraola-form-inner">
                                                <div class="single-input single-input-half">
                                                    <label for="account-details-firstname">First Name*</label>
                                                    <input type="text" id="account-details-firstname">
                                                </div>
                                                <div class="single-input single-input-half">
                                                    <label for="account-details-lastname">Last Name*</label>
                                                    <input type="text" id="account-details-lastname">
                                                </div>
                                                <div class="single-input">
                                                    <label for="account-details-email">Email*</label>
                                                    <input type="email" id="account-details-email">
                                                </div>
                                                <div class="single-input">
                                                    <label for="account-details-oldpass">Current Password(leave blank to leave
                                                        unchanged)</label>
                                                    <input type="password" id="account-details-oldpass">
                                                </div>
                                                <div class="single-input">
                                                    <label for="account-details-newpass">New Password (leave blank to leave
                                                        unchanged)</label>
                                                    <input type="password" id="account-details-newpass">
                                                </div>
                                                <div class="single-input">
                                                    <label for="account-details-confpass">Confirm New Password</label>
                                                    <input type="password" id="account-details-confpass">
                                                </div>
                                                <div class="single-input">
                                                    <button class="hiraola-btn hiraola-btn_dark" type="submit"><span>SAVE
                                                    CHANGES</span></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
@endsection