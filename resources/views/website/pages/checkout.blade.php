@extends('website.layouts.master')
@section('content')
        <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Other</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Checkout</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="checkout-area">
            <div class="container">
              {{--   <div class="row">
                    <div class="col-12">
                        <div class="coupon-accordion">
                            <h3>Returning customer? <span id="showlogin">Click here to login</span></h3>
                            <div id="checkout-login" class="coupon-content">
                                <div class="coupon-info">
                                    <p class="coupon-text">Quisque gravida turpis sit amet nulla posuere lacinia. Cras sed est
                                        sit amet ipsum luctus.</p>
                                    <form action="javascript:void(0)">
                                        <p class="form-row-first">
                                            <label>Username or email <span class="required">*</span></label>
                                            <input type="text">
                                        </p>
                                        <p class="form-row-last">
                                            <label>Password <span class="required">*</span></label>
                                            <input type="text">
                                        </p>
                                        <p class="form-row">
                                            <input value="Login" type="submit">
                                            <label>
                                                <input type="checkbox">
                                                Remember me
                                            </label>
                                        </p>
                                        <p class="lost-password"><a href="javascript:void(0)">Lost your password?</a></p>
                                    </form>
                                </div>
                            </div>
                            <h3>Have a coupon? <span id="showcoupon">Click here to enter your code</span></h3>
                            <div id="checkout_coupon" class="coupon-checkout-content">
                                <div class="coupon-info">
                                    <form action="javascript:void(0)">
                                        <p class="checkout-coupon">
                                            <input placeholder="Coupon code" type="text">
                                            <input class="coupon-inner_btn" value="Apply Coupon" type="submit">
                                        </p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="col-lg-6 col-12">
                    <div class="tab-pane " id="account-address" role="tabpanel" aria-labelledby="account-address-tab">
                            <div class="myaccount-address">
                                <p>The following addresses will be used on the checkout page by default.</p>
                                <div class="row">
                                    @if(sizeof($address['userAddress']) > 0)
                                        @foreach($address['userAddress'] as $key => $address)
                                        <a href="javascript:;" class="col-md-6 mainClick">
                                            <div class="bg-color-gray min-h-170 btnClick p-10">
                                                <input type="hidden" value="{{ $address['id'] }}" class="addressId">
                                                <h4 class="small-title">{{ $address['address_type'] }}</h4>
                                                <address>
                                                    {{ $address['address'] }}
                                                </address>
                                            </div>
                                        </a>
                                        @endforeach
                                    @else
                                        <a href="javascript:;" class="col-md-12 mainClick">
                                            <div class="bg-color-gray min-h-170 btnClick p-10">
                                                <h4 class="small-title">You have no address Please create address</h4>
                                                
                                            </div>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div><br>
                        <form action="{{ route('create.address') }}" method="POST">
                            @csrf
                            <div class="checkbox-form">
                                <div class="different-address">
                                    <div class="ship-different-title">
                                        <h3>
                                            <label>Ship to a different address?</label>
                                            <input id="ship-box" type="checkbox">
                                        </h3>
                                    </div>
                                    <div id="ship-box-info" class="row">
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Title <span class="required">*</span></label>
                                                <input placeholder="" type="text" name="address_type" required>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Country <span class="required">*</span></label>
                                                <input placeholder="" type="text" name="country" required>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>State<span class="required">*</span></label>
                                                <input placeholder="" type="text" name="state" required>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>City <span class="required">*</span></label>
                                                <input type="text" name="city" required>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Zip Code <span class="required">*</span></label>
                                                <input placeholder="" type="number" name="zipcode" required>
                                            </div>
                                        </div>
                                        <div class="order-notes">
                                            <div class="col-md-12">
                                                <div class="checkout-form-list checkout-form-list-2">
                                                    <label>Address</label>
                                                    <textarea name="address" id="checkout-mess" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                 <div class="order-button-payment">
                                                <input value="Save Address" type="submit">
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="your-order">
                            <h3>Bank Transfer</h3>
                            <p>Transfer your money into given bank detail</p>
                            <div class="your-order-table table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr class="cart_item ">
                                            <td class="cart-product-name"> Bank Name:<strong class="product-quantity">
                                        </strong></td>
                                            <td class="cart-product-total"><span class="amount">ICICI Bank</span></td>
                                        </tr> 
                                        <tr class="cart_item ">
                                            <td class="cart-product-name"> Account Number:<strong class="product-quantity">
                                        </strong></td>
                                            <td class="cart-product-total"><span class="amount">1456325125</span></td>
                                        </tr> 
                                        <tr class="cart_item ">
                                            <td class="cart-product-name"> IFSC Code:<strong class="product-quantity">
                                        </strong></td>
                                            <td class="cart-product-total"><span class="amount">ICICI2545</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="your-order">
                            <h3>Your order</h3>
                            <div class="your-order-table table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="cart-product-name">Product</th>
                                            <th class="cart-product-total">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                            $subTotal = 0;
                                            $total = '';
                                            $goldPrice = '';
                                        ?>
                                        @foreach($cartItems as $key =>$Item)
                                         <?php
                                                $total = $subTotal + $Item['totalAmount'];
                                                $subTotal = $total;
                                                $goldPrice = $Item['goldPrice'];
                                            ?>
                                        <tr class="cart_item ">
                                            <td class="cart-product-name"> {{ $Item['name'] }}<strong class="product-quantity">
                                            × {{ $Item['qty'] }}</strong></td>
                                            <td class="cart-product-total"><span class="amount">₹ {{ $Item['totalAmount'] }}</span></td>
                                        </tr>
                                        @endforeach
                                       
                                    </tbody>
                                    <tfoot>
                                        <tr class="cart-subtotal ">
                                            <th>Cart Subtotal</th>
                                            <td><span class="amount">₹ {{ $total }}</span></td>
                                        </tr>
                                        <tr class="order-total ">
                                            <th>Order Total</th>
                                            <td><strong><span class="amount">₹ {{ $total }}</span></strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="payment-method">
                                <div class="payment-accordion">
                                  <form method="POST" action="{{ route('save.order') }}">
                                    @csrf
                                        <div class="order-button-payment">
                                            <input value="{{$goldPrice}}" type="hidden" name="goldPrice">
                                            <input value="111" type="hidden" class="addressInputId" name="addressId">
                                            <input value="Place order" type="submit" class="submitFormBtn">
                                        </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('addjavascript')
<script type="text/javascript">
    $(document).on('click','.btnClick',function(){
       var addressId =  $(this).find('input').val();
        $('.order-button-payment').find('.addressInputId').val(addressId);
        $(this).css('color','#cda457','box-shadow','0.3px 0px 5px #cda457');
        $(this).parents('.mainClick').siblings().find('.btnClick').css('color','#595959','box-shadow','0.3px 0px 5px 0px');
    }); 
    $(document).on('click','.submitFormBtn',function(){
            $('.order-button-payment').find('p').hide();
        var addressId =  $('.order-button-payment').find('.addressInputId').val();
        console.log(addressId );
        if (addressId == '111') {
            $('.order-button-payment').append('<p class="text-danger" >Please Add Address First</p>');
            return false;
        }
    });  

    
</script>

@endsection