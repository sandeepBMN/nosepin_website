@extends('website.layouts.master')
@section('content')
        <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Other</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li class="active">OTP </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="hiraola-login-register_area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
                        <div class="container">
                            <form method="POST" action="{{route('verify.otp') }}">
                                @csrf
                                <div class="login-form">
                                    <h4 class="login-title">Confirm OTP</h4>
                                    <div class="row">
                                        <div class="col-md-12 col-12">
                                            <label>OTP*</label>
                                            <input type="number" placeholder="OTP" name="otp" min="1" value="{{$user['user']['otp'] }}" required>
                                            <input type="hidden" name="id" value="{{base64_encode($user['user']['id'])}}">
                                        </div>
                                        <div class="col-md-12">
                                            <button class="hiraola-login_btn">Login</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
 @endsection