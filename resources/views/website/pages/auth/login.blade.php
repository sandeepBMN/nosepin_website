@extends('website.layouts.master')
@section('content')
        <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Other</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Login </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="hiraola-login-register_area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
                        <div class="loginFormPan">
                            <div class="container">
                                <form method="POST" action="{{route('create.login') }}">
                                    @csrf
                                    <div class="login-form">
                                        <h4 class="login-title">Login</h4>
                                        <div class="row">
                                            <div class="col-md-12 col-12">
                                                <label>Mobile*</label>
                                                <input type="number" placeholder="Mobile" name="mobile" min="10" required>
                                                @if($errors->has('mobile'))
                                                     <span class="text-danger">{{ $errors->first('mobile') }}</span>   
                                                @endif
                                            </div>
                                            <div class="col-md-12 col-12">
                                                <label>Password (optional)</label>
                                                <input type="password" placeholder="Password (If you have password)" name="password">
                                                 @if($errors->has('password'))
                                                     <span class="text-danger">{{ $errors->first('password') }}</span>   
                                                @endif
                                            </div>
                                            <div class="col-md-12"> 
                                                <button class="hiraola-login_btn pull-left m-r-10 loginbtn" name="withOtp">Login With OTP</button>
                                                <button class="hiraola-login_btn pull-left m-r-10 loginbtn" name="withPass">Login With Password</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 @endsection