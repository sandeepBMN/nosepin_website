@extends('website.layouts.master')
@section('content')

        <div class="slider-with-category_menu">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 order-md-1 order-lg-2">
                        <div class="hiraola-slider_area">
                            <div class="main-slider">
                                <div class="single-slide animation-style-01 bg-1">
                                    <div class="container">
                                        <div class="slider-content">
                                            <h5><span>Black Friday</span> This Week</h5>
                                            <h2>Work Desk</h2>
                                            <h3>Surface Studio 2019</h3>
                                            <h4>Starting at <span>£1599.00</span></h4>
                                            <div class="hiraola-btn-ps_left slide-btn">
                                                <a class="hiraola-btn" href="{{route('page.products')}}">Shopping Now</a>
                                            </div>
                                        </div>
                                        <div class="slider-progress"></div>
                                    </div>
                                </div>
                                <div class="single-slide animation-style-02 bg-2">
                                    <div class="container">
                                        <div class="slider-content">
                                            <h5><span>-10% Off</span> This Week</h5>
                                            <h2>Phantom4</h2>
                                            <h3>Pro+ Obsidian</h3>
                                            <h4>Starting at <span>£809.00</span></h4>
                                            <div class="hiraola-btn-ps_left slide-btn">
                                                <a class="hiraola-btn" href="{{route('page.products')}}">Shopping Now</a>
                                            </div>
                                        </div>
                                        <div class="slider-progress"></div>
                                    </div>
                                </div>
                                <div class="single-slide animation-style-02 bg-3">
                                    <div class="container">
                                        <div class="slider-content">
                                            <h5><span>Black Friday</span> This Week</h5>
                                            <h2>Work Desk</h2>
                                            <h3>Surface Studio 2019</h3>
                                            <h4>Starting at <span>£1599.00</span></h4>
                                            <div class="hiraola-btn-ps_left slide-btn">
                                                <a class="hiraola-btn" href="{{route('page.products')}}">Shopping Now</a>
                                            </div>
                                        </div>
                                        <div class="slider-progress"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
        <div class="hiraola-product_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hiraola-section_title">
                            <h4>New Categories</h4>
                        </div>
                    </div>
                    @if($categories->count() > 0)
                    <div class="col-lg-12 order-1 order-lg-2">
                        <div class="shop-product-wrap grid gridview-3 row">
                            @foreach($categories as $key => $category)
                            <div class="col-lg-3">
                                <div class="slide-item">
                                    <div class="single_product">
                                        <div class="product-img">
                                            <a href="javascript:void;">
                                                <img class="primary-img" src="{{ $category['cat_image'] }}" alt="Hiraola's Product Image">
                                               {{--  <img class="secondary-img" src="assets/images/product/medium-size/1-8.jpg" alt="Hiraola's Product Image"> --}}
                                            </a>
                                            
                                        </div>
                                        <div class="hiraola-product_content">
                                            <div class="product-desc_info">
                                                <h6><a class="product-name" href="javascript:void;">{{ $category['name'] }}</a></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           @endforeach
                        </div>
                    </div>
                    @else
                     <div class="col-lg-12 order-1 order-lg-2">
                        <div class="shop-product-wrap grid gridview-3 row">
                            <h6>No Category Available</h6>
                        </div>
                    </div>
                    @endif
            </div>
        </div>
      
        <div class="hiraola-product-tab_area-2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product-tab">
                            <div class="hiraola-tab_title">
                                <h4>New Products</h4>
                            </div>
                           
                        </div>
                        <div class="col-lg-12 order-1 order-lg-2">
                       
                        <div class="shop-product-wrap grid gridview-3 row">
                            @if($products->count() > 0)
                             @foreach($products as $key => $product)
                            <div class="col-lg-3">
                                <div class="slide-item">
                                    <div class="single_product">
                                        <div class="product-img">
                                            <a href="javascript:void;">
                                                 @if($product->attachments->count() > 0)
                                                    @if($product->attachments->count() == 1)
                                                        <img class="primary-img" src="{{ asset($product->attachments[0]['path']) }}" alt="Hiraola's Product Image">
                                                    @endif
                                                     @if($product->attachments->count() >= 2)
                                                        <img class="primary-img" src="{{ asset($product->attachments[0]['path']) }}" alt="Hiraola's Product Image">
                                                        <img class="secondary-img" src="{{ asset($product->attachments[1]['path']) }}" alt="Hiraola's Product Image">
                                                    @endif     
                                                @else
                                                    <img class="primary-img" src="assets/images/product/medium-size/1-9.jpg" alt="Hiraola's Product Image">
                                                    <img class="secondary-img" src="assets/images/product/medium-size/1-8.jpg" alt="Hiraola's Product Image">
                                                @endif    
                                            </a>
                                        </div>
                                        <div class="hiraola-product_content">
                                            <div class="product-desc_info">
                                                <h6><a class="product-name" href="javascript:void;">{{ $product['name'] }}</a></h6>
                                                <div class="price-box">
                                                    <span class="new-price">Weight: {{ $product['weight'] }}</span>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                             <div class="col-lg-12 order-1 order-lg-2">
                                <div class="shop-product-wrap grid gridview-3 row">
                                    <h6>No Product Available</h6>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div><br><br/>
        <!--Product Tab Area End Here -->

@endsection