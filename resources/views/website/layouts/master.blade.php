
@include('website.layouts.header')
	@if(Session::has('success'))
		<div class="hasClass" >
			<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
		</div>
	@elseif(Session::has('error'))
		<div class="hasClassDanger" >
			<p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
		</div>	
	@endif
@yield('content')


@include('website.layouts.footer')