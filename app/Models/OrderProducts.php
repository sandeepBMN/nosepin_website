<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProducts extends Model
{

       /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name','weight','quantity','purity','labour','inc_dec_labour','order_id','price','total_weight','product_attachment'
    ];
    

}
