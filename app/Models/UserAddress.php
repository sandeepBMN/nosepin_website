<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users_address';
    protected $fillable = [
       'user_id','address','address_type','city','country','state','zipcode'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
