<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const order_inprocess = 1;
    const order_shipped = 2;
    const order_deliever = 3;
    const payment_pending = 4;

    
    /**
        * The attributes that are mass assignable.
        *
        * @var array
    */
    protected $fillable = [
        'user_id','delievery_status','delivery_date','invoice_no','gst_percent','gst_amount','gold_price','totalprice','payment_status','address_id'
    ];
    
    public function orderProducts ()
    {
        return $this->hasMany('App\Models\OrderProducts' , 'order_id' , 'id');
    }
    public function deliverAddress ()
    {
        return $this->hasMany('App\Models\UserAddress' , 'id', 'address_id' );
    }
    public function User ()
    {
        return $this->belongsTo('App\Models\User' );
    }
}
