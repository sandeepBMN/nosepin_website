<?php 
namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use App\Services\AuthService;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Support\Facades\Validator;
use Hash;
use Exception;
use Auth;
use Str;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class UserRepository 
{
    public $user;

    function __construct(User $user) {

      $this->user = $user;
    }

    // Get all instances of model
    public function all()
    {
        return $this->user->all();
    }
     /**
     * To login a new user 
     * @param  array $data 
     * @return  array 
     */
     public function generateOTP(){
        $otp = mt_rand(1000,9999);
        return $otp;
    }


    public function loginUser($data)
    {
        $newdata = collect($data);
        $mobile =$newdata['mobile'];
        $user = User::where('mobile', $mobile)->first();     
       
        if($user != null)
        {
            $otp = $this->generateOTP();

           $userexist = User::where('mobile', $mobile);
            $userexist->update(['otp'=>$otp]);
            $newuser = $userexist->first();

        }
        else
        {
            $newuser =  $this->user->create([
                            'mobile'     =>     $mobile,
                            'api_token' => Str::random(60),
                            'otp'       => $this->generateOTP(),
                        ]);
        }
        // $userId = $user->id;
        try {
              
            if($newuser)
            {
                return ['status'=>true,'user'=>$newuser,'type'=>200];
            }
            else{
                return ['status'=>false ,'type'=>400,'message'=>'Invalid mobile number'];
            }

        } catch (Exception $e) {
              // something went wrong whilst attempting to encode the token
            return ['status'=>false,'type'=>400,'message' => 'Could not create OTP'];
        }
    }

    public function WebuserLogin($data)
    {
         if (array_key_exists('withPass', $data)) {
            $userData['mobile'] = $data['mobile'];
            $userData['password'] = $data['password']; 
            $user = Auth::attempt($userData);
            if ($user == true) {
                $user = Auth::user();
                return ['status'=>true,'user'=>$user,'type'=>200];
            }else{
                return ['status'=>false ,'type'=>400,'message'=>'Invalid mobile number'];
            }
        }elseif (array_key_exists('withOtp', $data)) {
            $newdata = collect($data);
            $mobile =$newdata['mobile'];
            $user = User::where('mobile', $mobile)->first();     
            if($user != null)
            {
                $otp = $this->generateOTP();
               $userexist = User::where('mobile',$mobile);
                $userexist->update(['otp'=>$otp]);
                $newuser = $userexist->first();
            }
            else
            {
                $newuser =  $this->user->create([
                                'mobile'     => $mobile,
                                'api_token' => Str::random(60),
                                'otp'       => $this->generateOTP(),
                            ]);
            }
            try {
                if($newuser)
                {
                    return ['status'=>true,'user'=>$newuser,'type'=>200];
                }
                else{
                    return ['status'=>false ,'type'=>400,'message'=>'Invalid mobile number'];
                }
            } catch (Exception $e) {
                  // something went wrong whilst attempting to encode the token
                return ['status'=>false,'type'=>400,'message' => 'Could not create OTP'];
            }
        }
    }

      /**
     * update record in the database
     * @param  array $data 
     * @return boolean       
     */
    public function updateProfile($data)
    {

        $user = Auth::user();
        if($user)
        {
            $updateArray =[
                'name'      => $data['name'],
                'email'     => $data['email'],
                'gender'    => $data['gender'],
                // 'password'  => Hash::make($data['password']),
            ];
            User::where('id',$user->id)->update($updateArray);
            return true;
        }
        else{
            return false;
        }
    }

      /**
     * save address in the database
     * @param  array $data 
     * @return   
     */
    public function saveAddress($data)
    {
        $user = Auth::user();
        if($user && $user->isconfirmed == 1)
        {
            $userAddress =UserAddress::create([
                'user_id'      => $user->id,
                'address'     => $data['address'],
                'address_type'     => $data['address_type'],   
                'city'           => $data['city'],
                'country'       => $data['country'],
                'state'         => $data['state'],
                'zipcode'       => $data['zipcode']
            ]);
            return true;
        }
        else{
            return false;
        }
    }
      /**
     * To get user address into the database
     * @param 
     * @return   
     */
      public function getAddress()
    {
        $user = Auth::user();
        
        if($user && $user->isconfirmed == 1)
        {
          $getUserAddress = UserAddress::where('user_id',$user->id)->get()->toArray();
          return $getUserAddress;
        }
        else{
            return false;
        }
    }

    public function getUser(){

         $user = Auth::user();
        if($user != null && $user->isconfirmed == 1){   

              return $user;
        }
        return false;
    }

     /**
     * check if user exists and is confirmed
     * @return  user object if user is valid 
     */

    public function getValidUser($id){
          $userObject = User::find($id);
        if($userObject != null && $userObject->isconfirmed == 1){     
              return $userObject;
        }
        return false;
    }

       /**
       * To delete user address
       * @return 
       */
       public function deleteAddress($addressid,$userId)
       {
            $isExistAddress=  UserAddress::where('user_id',$userId)->where('id',$addressid)->first();
           if($isExistAddress != null)
           {
              $isExistAddress->delete();
              return true;
           }else{
              return false;
           }
       }
         /**
       * To delete user address
       * @return primary address
       */
       public function getPrimaryAddress($addressid,$userId)
       {
            $getPrimaryAddress=  UserAddress::where('user_id',$userId)->where('id',$addressid)->first();
           if($getPrimaryAddress != null)
           {
              return $getPrimaryAddress;
           }else{
              return false;
           }
       }

}