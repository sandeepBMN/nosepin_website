<?php 
namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\OrderProducts;
use Modules\Admin\Entities\Product;
use Modules\Admin\Services\DashboardService;
use App\Models\Order;
use Hash;
use Auth;

class PriceRepository 
{
	 
    public function processPrice($request){
        $data = [];
        $data['purity']         = (int) $request['purity'];
        $data['inc_dec_labour'] = $request['inc_dec_labour'];
        $data['labour']         = $request['labour'];
        $data['goldPrice']      = $request['goldPrice'];
        $data['qty']            = $request['qty'];
        $data['weight']         = (int) $request['weight'];

        if( $data['purity'] != '' || $data['inc_dec_labour'] != '' || $data['labour'] != '' || $data['goldPrice'] != '' || $data['qty'] != '' || $data['weight'] != '' ) {
            return $this->processAmount($data);
        }else{
            return 0;
        }
    }
    public function processAmount($data)
    {
        if( $data['inc_dec_labour'] == 'inc' ){
            $totalpurity = ($data['purity'] + $data['labour']);
        }
        if( $data['inc_dec_labour'] == 'dec' ){
            $totalpurity = ($data['purity'] - $data['labour']);
        }
        
        $oneGramPrice = (($totalpurity * $data['goldPrice'])/100);
        $totalWeight = ( $data['weight'] * $data['qty'] );
        $totalPrice = ( $oneGramPrice * $totalWeight );

        return $totalPrice;
    }
    public function processUserCart($request){
        $cartData = $request;
        $arrayData = [];
        if( $cartData != null ){
            foreach($cartData as $k=> $v){
                $purity     = $request[$k]['purity'];
                $qty        = $request[$k]['qty'];
                $product_id = $request[$k]['product_id'];
                $product = Product::where(['id' => $product_id])->with(['attachments'])->first();
                $productAttachment = '';
                if( $product->attachments != null ){
                    if( $product->attachments->count() > 0 ){
                        $productAttachment = $product->attachments[0]->path;
                    }
                }

                if( $product != null ){
                    $weight         = $product['weight'];
                    $labour         = $product['labour'];
                    $inc_dec_labour = $product['inc_dec_labour'];
                    $currentGoldPrice = DashboardService::getGoldPrice();

                    // $myRequest = new \Illuminate\Http\Request();
                    // $myRequest->setMethod('POST');
                    // $myRequest->request->add(['purity'          => $purity]);
                    // $myRequest->request->add(['inc_dec_labour'  => $inc_dec_labour]);
                    // $myRequest->request->add(['labour'          => $labour]);
                    // $myRequest->request->add(['goldPrice'       => $currentGoldPrice]);
                    // $myRequest->request->add(['qty'             => $qty]);
                    // $myRequest->request->add(['weight'          => $weight]);

                    $data = [];
                    $data['purity']         = (int) $purity;
                    $data['inc_dec_labour'] = $inc_dec_labour;
                    $data['labour']         = $labour;
                    $data['goldPrice']      = (int) $currentGoldPrice->gold_price;
                    $data['qty']            = $qty;
                    $data['weight']         = (int) $weight;
                    $amount = $this->processAmount($data);

                    $processArray = [];
                    $processArray['name']              = $product['name'];
                    $processArray['qty']               = $qty;
                    $processArray['weight']            = (int) $product['weight'];
                    $processArray['totalWeight']       = ( (int)$product['weight'] * $qty );
                    $processArray['purity']            = (int) $purity;
                    $processArray['labour']            = $labour;
                    $processArray['inc_dec_labour']    = $inc_dec_labour;
                    $processArray['totalAmount']       = $amount;
                    $processArray['attachment']        = $productAttachment;
                    $processArray['goldPrice']         = (int) $currentGoldPrice->gold_price;

                    $arrayData[] = $processArray;
                }
            }
        }
        return json_encode($arrayData);
    }

    public function webProcessUserCart($request){
        $cartData = $request;
        $arrayData = [];
        if( $cartData != null ){
            foreach($cartData as $k=> $v){
                $cartId     = $request[$k]['id'];
                $purity     = $request[$k]['purity'];
                $qty        = $request[$k]['qty'];
                $product_id = $request[$k]['product_id'];
                $product = Product::where(['id' => $product_id])->with(['attachments'])->first();
                $productAttachment = '';
                if( $product->attachments != null ){
                    if( $product->attachments->count() > 0 ){
                        $productAttachment = $product->attachments[0]->path;
                    }
                }

                if( $product != null ){
                    $weight         = $product['weight'];
                    $labour         = $product['labour'];
                    $inc_dec_labour = $product['inc_dec_labour'];
                    $currentGoldPrice = DashboardService::getGoldPrice();

                    // $myRequest = new \Illuminate\Http\Request();
                    // $myRequest->setMethod('POST');
                    // $myRequest->request->add(['purity'          => $purity]);
                    // $myRequest->request->add(['inc_dec_labour'  => $inc_dec_labour]);
                    // $myRequest->request->add(['labour'          => $labour]);
                    // $myRequest->request->add(['goldPrice'       => $currentGoldPrice]);
                    // $myRequest->request->add(['qty'             => $qty]);
                    // $myRequest->request->add(['weight'          => $weight]);

                    $data = [];
                    $data['purity']         = (int) $purity;
                    $data['inc_dec_labour'] = $inc_dec_labour;
                    $data['labour']         = $labour;
                    $data['goldPrice']      = (int) $currentGoldPrice->gold_price;
                    $data['qty']            = $qty;
                    $data['weight']         = (int) $weight;
                    $amount = $this->processAmount($data);

                    $processArray = [];
                    $processArray['cartId']              = $cartId;
                    $processArray['name']              = $product['name'];
                    $processArray['qty']               = $qty;
                    $processArray['weight']            = (int) $product['weight'];
                    $processArray['totalWeight']       = ( (int)$product['weight'] * $qty );
                    $processArray['purity']            = (int) $purity;
                    $processArray['labour']            = $labour;
                    $processArray['inc_dec_labour']    = $inc_dec_labour;
                    $processArray['totalAmount']       = $amount;
                    $processArray['attachment']        = $productAttachment;
                    $processArray['goldPrice']         = (int) $currentGoldPrice->gold_price;

                    $arrayData[] = $processArray;
                }
            }
        }
        return json_encode($arrayData);
    }
}