<?php 
namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\OrderProducts;
use Modules\Admin\Entities\Product;
use App\Models\Order;
use Hash;
use Auth;

class PaymentRepository 
{
	  /**
     * save order in the database
     * @param  array $data 
     * @return   
     */
    public function saveOrder($data,$newdata)
    {
        $user = Auth::user();
        $Amount = [];
        $address_id = $data['addressId'];
        $goldPrice = $data['goldPrice'];
        $orders = $newdata;

        foreach ($orders as $key => $value) {
            $cartProductId = $value['product_id'];
            $cartQty = $value['qty'];
            $cartPurity = $value['purity'];

            $product = Product::where(['id' => $cartProductId])->first();
            if( $product != null ){
                $labour = $product->labour; 
                $inc_dec_labour = $product->inc_dec_labour; 
                $weight = $product->weight; 

                if( $inc_dec_labour == 'inc' ){
                    $priceFormula = (((($cartPurity + $labour) * $goldPrice)/100) *($cartQty*$weight));
                }else{
                    $priceFormula = (((($cartPurity - $labour) * $goldPrice)/100) *($cartQty*$weight));
                }

                $Amount[] = $priceFormula;
            }
        }
        $totalAmount = array_sum($Amount);
        $GSTAmount = (($totalAmount * 18)/100);
        $amountBeforeGST = $totalAmount - $GSTAmount;

        $order =    Order::create([
                        'user_id'  => $user->id,
                        'gold_price'    => $goldPrice,
                        'delievery_status' => Order::order_inprocess,
                        'invoice_no'       => 0,
                        'gst_percent'      => 18,
                        'gst_amount'      => $GSTAmount,
                        'totalprice'      => $totalAmount,
                        'address_id'      => $address_id
                    ]);
        $orderId  = $order->id;
        $updateInvoice = Order::where(['id' => $order->id])->update(['invoice_no' => "1100".$order->id]);
        // end save order

        //start save  OrderProducts
        foreach ($orders as $key => $value) {
            $cartProductId = $value['product_id'];
            $cartQty = $value['qty'];
            $cartPurity = $value['purity'];

             if( $product != null ){
                $labour = $product->labour; 
                $inc_dec_labour = $product->inc_dec_labour; 
                $weight = $product->weight; 

                if( $inc_dec_labour == 'inc' ){
                    $price = (((($cartPurity + $labour) * $goldPrice)/100) *($cartQty*$weight));
                }else{
                    $price = (((($cartPurity - $labour) * $goldPrice)/100) *($cartQty*$weight));
                }

            }
            $product = Product::where([ 'id' => $cartProductId ])->with(['attachments'])->first();
            if( $product->has('attachments') ){
                if( $product->attachments->count() > 0 ){
                    $attach = $product->attachments;
                    $thumbsimg='';
                    foreach ($attach as $key => $value) {
                       if($value['type'] == 'thumbs')
                       {
                        $thumbsimg = $value['storage_path'].'/'.$value['image_name'];

                       } 
                    }
                 
                    $attachment = $thumbsimg;
                }
            }

            $saveProductOrder = OrderProducts::create([
                                    'name' => $product['name'],
                                    'weight' => $product['weight'] ,
                                    'quantity' => $cartQty,
                                    'purity' => $cartPurity ,
                                    'labour' => $product['labour'] ,
                                    'inc_dec_labour' => $product['inc_dec_labour'] ,
                                    'order_id' => $orderId ,
                                    'price' => $price ,
                                    'total_weight' => ( $product['weight'] * $cartQty ),
                                    'product_attachment' => $attachment
                                 ]);
        }
        if($saveProductOrder)
        {     
            return true;
        }else{
            return  false;
        }
    }
            
    /**
     * To get all orders list
     * @param  
     * @return   
     */
    public static function listOrder($id,$page){
        // $perPage = 10;
        $user = Auth::user();

        // $UsersOrder = Order::where(['user_id' => $user->id])->with(['orderProducts'])->paginate($perPage, ['*'], 'page', $page);
        $UsersOrder = Order::where(['user_id' => $user->id])->with(['orderProducts'])->orderBy('id','DESC')->get();
        return $UsersOrder;
    }

    /**
     * To getordersproducts by id
     * @param  
     * @return   
     */
    public static function getOrderProducts($orderId){

        $user = Auth::user();
        $order = Order::where('invoice_no',$orderId)->with(['deliverAddress','User'])->first();
        $getorderid = $order->id;
        $userAddress = $order->deliverAddress;
        $getOrderProducts= [];
        if($getorderid != null)
        {
            // $getOrderProducts = OrderProducts::where(['order_id' => $getorderid])->get()->toArray();
            $getOrderProducts['product'] = OrderProducts::where(['order_id' => $getorderid])->get()->toArray();
            $getOrderProducts['address'] = $userAddress;
            $getOrderProducts['order'] = $order;
            return $getOrderProducts;
        }else{
            return false;
        }
    }

    public static function updateOrderPaymentStatus($data){
        Order::where('invoice_no',$data['txnid'])->update(['payment_status' => $data['status']]);
        return true;
    } 

    public static function saveTransaction($data){
              
    } 

}

