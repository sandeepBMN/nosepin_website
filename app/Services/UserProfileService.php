<?php

namespace App\Services;

use App\Repository\UserRepository;
use Storage;
use File;

class UserProfileService {
	
    protected $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo     = $userRepo;
    }
    /**
     *  create a user update service function
     * 
     * @param array $request
     * @return  array
     */

    public function updateProfile($request)
    {        
        $hasUpdate = $this->userRepo->updateProfile($request->all());
        if($hasUpdate != false)
        {
            return ['status' =>true,'message'=> 'Profile updated successfully'];
        }
        else{
             return ['status'=>false,'message' => 'User Not found','type'=>'404'];
        }
    } 
    public function getUser($request)
    {
         return $this->userRepo->getUser();
    }
   
}