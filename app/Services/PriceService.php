<?php

namespace App\Services;

use App\Repository\PriceRepository;
use Storage;
use Auth;
use File;

class PriceService {
	
    protected $priceRepo;

    public function __construct(PriceRepository $priceRepo)
    {
        $this->priceRepo     = $priceRepo;
    }
   /**
     *  To save the order in processPrice service function
     * 
     * @param array $request
     * @return  array
     */
    public function processPrice($request)
    {
         return $this->priceRepo->processPrice($request);
         
    }
      /**
     *  To save the order in processAmount service function
     * 
     * @param array $request
     * @return  array
     */
     public function processAmount($data)
    {
       return $this->priceRepo->processAmount($data);

    }

     /**
     *  To save the order in processUserCart service function
     * 
     * @param array $request
     * @return  array
     */

      public function processUserCart($request){
         return $this->priceRepo->processUserCart($request);
      }
     /**
     *  To save the order in webProcessUserCart service function
     * 
     * @param array $request
     * @return  array
     */

    public function webProcessUserCart($request){
       
       return $this->priceRepo->webProcessUserCart($request);
    }

    
}