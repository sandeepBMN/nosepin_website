<?php

namespace App\Services;

use App\Repository\PaymentRepository;
use Storage;
use Auth;
use File;

class PaymentService {
	
    protected $paymentRepo;

    public function __construct(PaymentRepository $paymentRepo)
    {
        $this->paymentRepo     = $paymentRepo;
    }
   /**
     *  To save the order in saveOrder service function
     * 
     * @param array $request
     * @return  array
     */
    public function saveOrder($request)
    {
        $user = Auth::user();
        if( $user != null ){
            if( $user->isconfirmed == 1 ){
                $orderRecord = $request->all();
                if($request->has('orders'))
                {
                    $newdata =  json_decode($orderRecord['orders'],true);
            	    $order = $this->paymentRepo->saveOrder($orderRecord,$newdata);
                }else{
                    return ['status'=>false,'message' => 'Invalid Input','type'=>'400'];
                }
            	
                if($order != false)
                {
                    return ['status' =>true,'message'=> 'Order save successfully'];
                }else{
                    return ['status' =>false,'message'=> 'Some error Occur'];
                }
                   
            }else{
                 return ['status'=>false,'message' => 'User not Verified ','type'=>'400'];
            }
        }else{
            return ['status'=>false,'message' => 'User Not found','type'=>'404'];
        }
    }

    /**
     *  To get all orders
     * 
     * @param 
     * @return 
     */
    public static function listOrder($page){
        $user = Auth::user();
        if($user && $user->isconfirmed == 1)
        {
            $orders = PaymentRepository::listOrder($user->id,$page);

            if($orders!= false)
            {
                 return ['status' =>true,'order'=> $orders,'message' => 'Order get successfully'];
            }elseif($orders == null){
                 return ['status'=>true,'message' => 'No result found ','order'=> $orders,'type'=>'200'];
            }

        }else{
             return ['status'=>false,'message' => 'User Not found','type'=>'404'];
        }

    }

     /**
     *  To get orderproducts by invoice no
     * 
     * @param 
     * @return 
     */
    public static function getOrderProducts($orderid){
      
        $user = Auth::user();
        if( $user != null ){
            if( $user->isconfirmed == 1 ){
                $orders = PaymentRepository::getOrderProducts($orderid);

                if($orders!= false )
                {
                     return ['status' =>true,'order'=> $orders,'message' => 'Order Products get successfully'];
                }else{
                     return ['status'=>false,'message' => 'Order Not found based on Invoice no. ','type'=>'400'];
                }
            }else{
                return ['status'=>false,'message' => 'User not Verified ','type'=>'400'];
            }

        }else{
             return ['status'=>false,'message' => 'User Not found','type'=>'404'];
        }

    }
    public static function updateOrderPaymentStatus($data)
    {
         $paymentstatus = PaymentRepository::updateOrderPaymentStatus($data);
         return $paymentstatus;
    } 

    public static function saveTransaction($data){
        $saveTransaction = PaymentRepository::saveTransaction($data); 
    }

}