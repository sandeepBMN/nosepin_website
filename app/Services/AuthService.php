<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repository\UserRepository;
use Auth;
use App\Models\User;
use Hash;

class AuthService {
	
    protected static $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        self::$userRepo = $userRepo;
    }
    /**
     *  create a new register service function
     * 
     * @param array $request
     * @return  array
     */
    public function userRegister($request)
    {
         // return self::$userRepo->createUser($request);
    } 
     /**
     * Create a login service function.
     *
     * @param  array $request
     * @return 
     */
    public function userLogin($request)
    {
         $login=  self::$userRepo->loginUser($request);
         // if($login['status'] == true)
         // {
         //    $data= [];
         //    $data['mobile'] = $login['user']['mobile'];
         //    $data['message'] = 'OTP at the time of login is '.$login['user']['otp'];
         //   $sendSms =  $this->sendSms($data);
         // }

         return $login;
    }
     /**
     * Create a login service function.
     *
     * @param  array $request
     * @return 
     */
    public function WebuserLogin($request)
    {
         return self::$userRepo->WebuserLogin($request);
    }  

      /**
     * to get a valid user service function.
     *
     * @param  array $request
     * @return 
     */
    public function getValidUser($id)
    {
         return self::$userRepo->getValidUser($id);
    } 

    // public function sendSms($smsdata)
    // {
    //     $mobile = $smsdata['mobile'];

    //     // Account details
    //    $apiKey = urlencode('2JOi/i9q3Ug-ZL1vy9FFOXTz4SCIZfvP8KVY7hg6of');

    //    // Message details
    //    $numbers = array($mobile);
    //    $sender = urlencode('TXTLCL');
    //    $message = rawurlencode($smsdata['message']);

    //    $numbers = implode(',', $numbers);

    //    // Prepare data for POST request
    //    $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

    //    // Send the POST request with cURL
    //    $ch = curl_init('https://api.textlocal.in/send/');
    //    curl_setopt($ch, CURLOPT_POST, true);
    //    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    //    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //    $response = curl_exec($ch);
    //    curl_close($ch);

    //    // Process your response here
    //    echo $response;
    // }

}