<?php

namespace App\Services;

use App\Repository\UserRepository;
use Storage;
use File;
use Auth;

class UserAddressService {
	
    protected $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo     = $userRepo;
    }
    /**
     *  create a user address service function
     * 
     * @param array $request
     * @return  array
     */
    
    public function saveAddress($request)
    {        
        $saveAddress = $this->userRepo->saveAddress($request->all());
        if($saveAddress != false)
        {
            return ['status' =>true,'message'=> 'Address save successfully'];
        }
        else{
             return ['status'=>false,'message' => 'User Not found','type'=>'404'];
        }
    } 
    /**
     *  To get a user address service function
     * 
     * @param 
     * @return  
     */

    public function getAddress()
    {        

        $getAddress = $this->userRepo->getAddress();

        if($getAddress != false)
        {
            return ['status' =>true,'message'=> 'Address get successfully','userAddress' => $getAddress];
        }
        elseif($getAddress == null){
              return ['status'=>true,'message' => 'Result Not found','userAddress' => $getAddress,'type'=>'200'];
        }else{
             return ['status'=>false,'message' => 'User Not found','type'=>'404'];
        }
    } 

    public function deleteAddress($address_id)
    {
        $id = base64_decode($address_id);

        $user = Auth::user();
        if($user != null && $user->isconfirmed == 1){  
            $deleteAddress = $this->userRepo->deleteAddress($id,$user->id);
            if($deleteAddress != false)
            {
                 return ['status' =>true,'message'=> 'Address delete successfully'];
            }else{
                return ['status'=>false,'message' => 'Address Not found','type'=>'404'];
            }
        }else{
            return ['status'=>false,'message' => 'User Not found','type'=>'404'];
        }

    }
}