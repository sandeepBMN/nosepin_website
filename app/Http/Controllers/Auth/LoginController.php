<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Services\AuthService;
use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AuthService $service)
    {

        $this->middleware('guest')->except('logout');
        $this->service = $service;
    }

    /**
     * Create a user login function
     *
     * @param  array $request
     * @return json response with user_id and token
     */
    public function authenticate(loginRequest $request){
      
        $user =  $this->service->userLogin($request->all());

        if($user['status'] == true)
        {
            
            $user_id = base64_encode($user['user']['id']);
            $token = $user['user']['api_token'];
            $otp = $user['user']['otp'];
            $this->setstatus(200);
            $this->setResponseData(['user_id'=> $user_id, 'otp' => $otp]);
            return $this->toResponse();
        }
         if($user['status'] == false)
        {
            $this->setMessage($user['message']);
            $this->setErrors(['error'=>[$user['message']]]);
            $this->setStatus($user['type']);
            return $this->toResponse();
            
        }
    }

      /**
    * Activate a user account after login 
    * 
    * @param  object  $request
    * @return   json response
    */
    public function activate(Request $request)
    {
       
        $user= User::where('id',base64_decode($request->id))->first();
        if (!isset($user)) {
            $this->setErrors(['error'=>'Invalid user']);
            return $this->toResponse();
        }
        else{
            if($user->otp == trim($request->otp)){
                $user->isconfirmed = 1 ;
                $user->save();
                $this->setMessage('Account activated successfully');
                $this->setResponseData(['api_token' => $user->api_token]);
                $this->setstatus(200);
                return $this->toResponse(); 
            }else{
                $this->setMessage('Invalid otp');
                $this->setErrors(['error'=>'Invalid otp']);
                return $this->toResponse();
                
            }
        }
    }
    

}
