<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Entities\State;
use Modules\Admin\Services\StateService;

class StateController extends Controller
{
    /**
        * Create a new controller instance.
        *
        * @return void
    */
    public function __construct(StateService $stateService)
    {
        $this->stateService = $stateService;
    }
     /**
     * Display a listing of all state in api.
     * @return Response
     */
    public function allState(){
    	$state = $this->stateService->allState();
    	if($state !=null)
    	 {
            $this->setMessage('State get Successfully');
            $this->setResponseData([$state]);
            return $this->toResponse();
        }else{
            $this->setMessage('No result Found');
            $this->setErrors(['error'=>['No Result Found']]);
             $this->setStatus(404);
            return $this->toResponse();
        }
    }
}
