<?php

namespace App\Http\Controllers\website\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Services\AuthService;
use App\Models\User;
use Session;
use Auth;

class LoginController extends Controller
{
   

    use AuthenticatesUsers;



     /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AuthService $service)
    {
        $this->middleware('guest')->except('logout');
        $this->service = $service;
    }

     /**
     *redirect To login page using login Controller login function .
     *
     * @param array request
     * @return json response
     */

    public function login(Request $request)
    {
        return view('website.pages.auth.login');
    }

     /**
     *redirect To Create login after login using login Controller Createlogin function .
     *
     * @param array request
     * @return json response
     */
    public function Logout(Request $request)
    {   
          Auth::logout();
        return redirect('/login');
    }   
    /**
     *redirect To Create login after login using login Controller Createlogin function .
     *
     * @param array request
     * @return json response
     */
    public function Createlogin(Request $request)
    {   
        if (array_key_exists('withPass', $request->all())) {
            $validatedData = $request->validate([
                'mobile' => 'required|max:10',
                'password' => 'required|min:6',
            ]);
            $user = $this->service->WebuserLogin($request->all());
            if ($user['status'] == true) {
                Session::flash('success','Welcome! user Login Successfully');
                return redirect()->route('page.index');
            }else{
                Session::flash('error',$user['message']);
                return back();
            }
        }elseif (array_key_exists('withOtp', $request->all())) {
            $validatedData = $request->validate([
                'mobile' => 'required|min:10|max:12',
            ]);
            $user = $this->service->WebuserLogin($request->all());
            if ($user['status'] == true) {
                return view('website/pages/auth/otp',['user'=> $user]);
            }else{
                Session::flash('error',$user['message']);
                return back();
            }
        }
        return back();
        
    }
      /**
     *redirect To Verify Otp after login using login Controller VerifyOtp function .
     *
     * @param array request
     * @return json response
     */
    public function VerifyOtp(Request $request)
    {
	   $user= User::where('id',base64_decode($request->id))->first();
        if (!isset($user)) {
            $this->setErrors(['error'=>'Invalid user']);
            Session::flash('error','User Not Exist');
            return view('website.pages.auth.otp',['user'=>$user]);
        }
        else{
            if($user->otp == trim($request->otp)){
                $user->isconfirmed = 1 ;
                $user->save();
                $userData['id'] =  $user['id'];
                $userData['otp'] =  $user['otp'];
                $Usercheck = Auth::loginUsingId($user['id']);
                Session::put('api_token',$user->api_token);
                $api_token = Session::get('api_token');
                Session::flash('success','Welcome! user Login Successfully');
        		return redirect()->route('page.index');
            }else{
                Session::flash('error','check! Wrong Otp');
                return back();
                
            }
        }
    }
}
