<?php

namespace App\Http\Controllers\website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Admin\Services\CategoryService;
use Modules\Admin\Services\StateService;
use Modules\Admin\Entities\Product;
use Modules\Admin\Services\ProductService;
use Modules\Admin\Services\DashboardService;
use App\Services\UserAddressService;
use App\Services\PaymentService;
use App\Services\PriceService;
use App\Models\User;
use App\Cart;
use Hash;
use Session;
use Route;
use Auth;

class HomeController extends Controller
{
     public $catService;
     public $stateService;
      public $prodService;
      public $priceService;
      public $paymentService;
      public $addressService;

    function __construct(CategoryService $catService,StateService $stateService,ProductService $prodService,PriceService $priceService,PaymentService $paymentService,UserAddressService $addressService) {

      $this->catService = $catService;
      $this->stateService = $stateService;
      $this->prodService = $prodService;
      $this->priceService = $priceService;
      $this->paymentService = $paymentService;
      $this->addressService = $addressService;
    }
    /**
     *redirect To index page using home Controller index function .
     *
     * @param array request
     * @return json response
     */
    public function index(Request $request)
    {   
        $products =   $this->prodService->allProduct($id =null);
        $categories = $this->catService->parentCategory(); 
        return view('website.pages.index',['categories' => $categories->take(12),'products' =>$products->take(12)]);
    }
    /**
     *redirect To cart page using home Controller cart function .
     *
     * @param array request
     * @return json response
     */
    public function cart()
    {   
        $user = Auth::user();
        $currentGoldPrice = DashboardService::getGoldPrice();
        $cart = Cart::select('id','purity','quantity as qty','product_id')->where('user_id',$user['id'])->get()->toArray();
            $itemPrice = $this->priceService->webProcessUserCart($cart);
        $cartItems = json_decode($itemPrice,true);
        // $cartItems = [];
        // $request = [];
        // foreach ($cart as $key => $value) {
        //     $request['purity']         = $value['purity'];
        //     $request['qty']            = $value['quantity'];
        //     $request['inc_dec_labour'] = $value->product['inc_dec_labour'];
        //     $request['labour']         = $value->product['labour'];
        //     $request['weight']         = $value->product['weight'];
        //     $request['goldPrice']      = $currentGoldPrice['gold_price'];
            
        //     $itemPrice = $this->priceService->processPrice($request);
        //     $cartItems[] = $value;
        //     $cartItems[$key]['price'] = $itemPrice;
        // }
        return view('website.pages.cart',['cartItems'=>$cartItems]);
    } /**
     *delete Item From cart page using home Controller cartItemDelete function .
     *
     * @param array request
     * @return json response
     */
    public function cartItemDelete($id)
    {   
        $delete = Cart::where('id',$id)->delete();
        Session::flash('success','Item deleted from cart');
        return back();
    }
    /**
     *update Item From cart page using home Controller updateCart function .
     *
     * @param array request
     * @return json response
     */
    public function updateCart(Request $request)
    {   
            $cartUpdate = Cart::where('id',$request->itemId)->update([
                                                                    'price' => $request->price,
                                                                    'quantity' => $request->quantity,
                                                                ]);
            return 'true';
    }
    /**
     *Save To cart table using home Controller cart function .
     *
     * @param array request
     * @return json response
     */
    public function saveCart(Request $request)
    {
        Session::put(['purity'=>$request->purity,'quantity' => $request->quantity,'product_id' => $request->product_id]);
        $user = Auth::user();
        $request['user_id'] = $user['id']; 
        $data = Cart::create($request->all());
        $data->save();
        Session::flash('success','Item added in cart');
        return redirect()->route('page.cart');
    }
    /**
     *redirect To contact us page using home Controller contactUs function .
     *
     * @param array request
     * @return json response
     */
    public function contactUs()
    {
        return view('website.pages.contact');
    }
    /**
     *redirect To contact us page using home Controller contactUs function .
     *
     * @param array request
     * @return json response
     */
    public function products($id =null)
    {
        $products =   $this->prodService->allProduct($id);
        return view('website.pages.products',['products' => $products]);
    }
     

    /**
     *redirect To product detail using home Controller productDetail function .
     *
     * @param array request
     * @return json response
     */
    public function productdetail($product_id)
    {
        $detailProduct =   $this->prodService->productDetail($product_id);
        $products =   $this->prodService->allProduct($id = null);
        return view('website.pages.single-product',['detailProduct' =>$detailProduct,'products' =>$products->take(6)]);
        
    }

    /**
     *redirect To Categories page using home Controller categories function .
     *
     * @param array request
     * @return json response
     */
    public function categories($id = null)
    {
        if($id == null)
        {
             $category = $this->catService->parentCategory(); 
        }
        else{
         
            $Id = base64_decode($id);
            $category = $this->catService->getSubCategory($Id);
        }
        if($category != null)
        { 
             return view('website.pages.categories',['category'=>$category]);
        }else{
             return view('website.pages.categories')->withErrors(['No result found']);
        }
    }
     /**
     *redirect To My Account using home Controller myAccount function .
     *
     * @param array request
     * @return json response
     */
    public function myAccount()
    {
        $user = Auth::user();
        $address =  $this->addressService->getAddress();
        $listOrder = $this->paymentService->listOrder($page=1);
        return view('website.pages.my-account',['user' => $user,'listOrder' =>$listOrder,'address' => $address]);
    }

    /**
     *save profile using home Controller saveProfile function .
     *
     * @param array request
     * @return json response
     */
    public function saveProfile(Request $request)
    {
        $user = Auth::user();
        $validatedData = $request->validate([
                'name' => 'required|min:2|max:100',
                'email' => 'required|email',
        ]);
        if ($user['password'] != null) {
            if ($request->currentPass != null) {
                $validatedData = $request->validate([
                    'currentPass' => 'required|min:6',
                    'newPass' => 'required|required_with:confirmPass|same:confirmPass|min:6',
                ]);
                if (Hash::check($request->currentPass,$user['password'])) {
                    $password = Hash::make($request->newPass);
                }else{
                    return back()->withErrors(['currentPass'=>'Current password is not match']);
                } 
            }else{
                $password = $user['password'];
            }
        }else{
            $validatedData = $request->validate([
                'password' => 'required|min:6',
            ]);
            $password = Hash::make($request->password);
        }    
        $save = User::where('id',$user['id'])->update([
                                                        'name'=>$request->name,
                                                        'email'=>$request->email,
                                                        'password'=>$password
                                                        ]);
        Session::flash('success','Profile Save Successfully');
        return back();
    }
      /**
     *redirect To states using home Controller states function .
     *
     * @param array request
     * @return json response
     */
    public function states()
    {
        $states = $this->stateService->allState();
        return view('website.pages.states',compact('states'));
    }

      /**
     *redirect To checkout using home Controller checkout function .
     *
     * @param array request
     * @return json response
     */
    public function checkout()
    {
        $user = Auth::user();
        $address =  $this->addressService->getAddress();
        $cart = Cart::select('id','purity','quantity as qty','product_id')->where('user_id',$user['id'])->get()->toArray();
        $itemPrice = $this->priceService->webProcessUserCart($cart);
        $cartItems = json_decode($itemPrice,true);
        return view('website.pages.checkout',['cartItems' =>$cartItems,'address' => $address]);
    }

    /**
     *To save a User address.
     *
     * @param array request
     * @return json response
     */
    public function saveAddress(Request $request)
    {
        $address =  $this->addressService->saveAddress($request);
        return back();
    }
     /**
     *To save a order.
     *
     * @param array request
     * @return json response
     */
    public function saveOrder(Request $request)
    {
        $user = Auth::user();
        $cart = Cart::select('id','purity','quantity as qty','product_id')->where('user_id',$user['id'])->get()->toArray();
        $request['orders'] = json_encode($cart);
        $order =  $this->paymentService->saveOrder($request);
        $cart = Cart::where('user_id',$user['id'])->delete();
        return redirect()->route('page.myAccount');
    }

   
}
