<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Entities\Product;
use App\Services\PriceService;
use Modules\Admin\Services\DashboardService;

use Auth;

class PriceController extends Controller
{
         public $priceService = '';

    /**
        * Create a new controller instance.
        *
        * @return void
    */
    public function __construct(PriceService $priceService)
    {
        return $this->priceService = $priceService;
    }



    public function processPrice(Request $request){
       return $this->priceService->processPrice($request->all());
    }
    public function processAmount($data)
    {
       
       return $this->priceService->processAmount($data);

    }
    public function processUserCart(Request $request){
       
       return $this->priceService->processUserCart($request->all());
    }
}