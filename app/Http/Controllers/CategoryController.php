<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Entities\Category;
use Modules\Admin\Services\CategoryService;


class CategoryController extends Controller
{
    /**
        * Create a new controller instance.
        *
        * @return void
    */
    public function __construct(CategoryService $catService)
    {
        $this->catService = $catService;
    }
    /**
        * To get all category .
        *
        * @return void
    */
     public function allCategory($id = null)
    {
        if($id == null)
        {
             $category = $this->catService->parentCategory(); 
        }
        else{
         
            $Id = base64_decode($id);
            $category = $this->catService->getSubCategory($Id);
        }
        if($category != null)
        {
            $this->setMessage('Category get Successfully');
            $this->setResponseData([$category]);
            return $this->toResponse();
        }else{
            $this->setMessage('No result Found');
            $this->setErrors(['error'=>['No Result Found']]);
             $this->setStatus(404);
            return $this->toResponse();
        }
    }

    /**
        * To get parent category .
        *
        * @return void
    */
     public function parentCategory()
    {
        $parentCategory = $this->catService->parentCategory(); 
        if($parentCategory != null)
        {
            $this->setMessage('Category get Successfully');
            $this->setResponseData([$parentCategory]);
            return $this->toResponse();
        }else{
            $this->setMessage('No result Found');
            $this->setErrors(['error'=>['No Result Found']]);
             $this->setStatus(404);
            return $this->toResponse();
        }
    }
}
