<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserAddressRequest;
use App\Services\UserAddressService;

class AddressController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct(UserAddressService $addressService)
    {
        $this->addressService = $addressService;
    }

     /**
     *To save a User address.
     *
     * @param array request
     * @return json response
     */
    public function saveAddress(UserAddressRequest $request)
    {
        $address =  $this->addressService->saveAddress($request);

        if($address['status'] == true){
            $this->setMessage($address['message']);
            $this->setResponseData([$address['message']]);
            return $this->toResponse();
        }else{
            $this->setMessage($address['message']);
            $this->setErrors(['error'=>[$address['message']]]);
            return $this->toResponse();
        }
    }

    /**
     *To get a User address.
     *
     * @param 
     * @return 
     */
    public function getAddress(Request $request)
    {
        $address =  $this->addressService->getAddress();

        if($address['status'] == true){
            $this->setMessage($address['message']);
            $this->setResponseData([$address['userAddress']]);
            return $this->toResponse();
        }else{
            $this->setMessage($address['message']);
            $this->setErrors(['error'=>[$address['message']]]);
            return $this->toResponse();
        }
    }

    /**
     *To delete a User address.
     *
     * @param 
     * @return 
     */
    public function deleteAddress($address_id)
    {
        $address =  $this->addressService->deleteAddress($address_id);
        if($address['status'] == true){
            $this->setMessage($address['message']);
            $this->setResponseData([$address['message']]);
            return $this->toResponse();
        }else{
            $this->setMessage($address['message']);
            $this->setErrors(['error'=>[$address['message']]]);
            return $this->toResponse();
        }
    }

}
