<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Entities\Product;
use Modules\Admin\Services\ProductService;

use Auth;

class ProductController extends Controller
{
    public $prodService = '';
    

    /**
        * Create a new controller instance.
        *
        * @return void
    */
    public function __construct(ProductService $prodService)
    {
        $this->prodService = $prodService;
       
    }

    /**
     * To get  all category
     * @param   
     * @return  all category collection 
     */
    public function allProduct($id =null)
    {
        if(Auth::user() && Auth::user()->isconfirmed == 1)
        {
           $product =   $this->prodService->allProduct($id);

            if(!$product->isEmpty())
            {
                $this->setMessage('Product get Successfully');
                $this->setResponseData([$product]);
                return $this->toResponse();
            }else{
                $this->setMessage('No result Found');
                $this->setResponseData([$product]);
                $this->setStatus(200);
                return $this->toResponse();
            }
        }else{
                $this->setMessage('User not found');
                $this->setErrors(['error'=>['User not found']]);
                 $this->setStatus(404);
                return $this->toResponse();
            }
    }

     /**
     * To get product detail 
     * @param   
     * @return  
     */
    public function productDetail($product_id)
    {
        if(Auth::user() && Auth::user()->isconfirmed == 1)
        {
            $detailProduct =   $this->prodService->productDetail($product_id);
           if($detailProduct != null)
            {
                $this->setMessage('Product get Successfully');
                $this->setResponseData([$detailProduct]);
                return $this->toResponse();
            }else{
                $this->setMessage('No result Found');
                $this->setResponseData(['No Result Found']);
                 $this->setStatus(200);
                return $this->toResponse();
            }  
        }else{
                $this->setMessage('User not found');
                $this->setErrors(['error'=>['User not found']]);
                 $this->setStatus(404);
                return $this->toResponse();
        }

    }

     /**
     * To get products based on category 
     * @param   
     * @return  
     */
    public function getProductByCategory($category_id)
    {
        if(Auth::user() && Auth::user()->isconfirmed == 1)
        {
           $product =   $this->prodService->getProductByCategory($category_id);
            if($product != null)
            {
                $this->setMessage('Product get Successfully');
                $this->setResponseData([$product]);
                return $this->toResponse();
            }else{
                $this->setMessage('No result Found');
                $this->setErrors(['error'=>['No Result Found']]);
                 $this->setStatus(404);
                return $this->toResponse();
            }
        }else{
                $this->setMessage('User not found');
                $this->setErrors(['error'=>['User not found']]);
                 $this->setStatus(404);
                return $this->toResponse();
         }
    }

     /**
     * To get hotproducts  
     * @param   
     * @return  
     */
    public function getHotProducts()
    {
        if(Auth::user() && Auth::user()->isconfirmed == 1)
        {
           $hotproduct =   $this->prodService->getHotProducts();
            if($hotproduct !=false)
            {
                $this->setMessage('hotProduct get Successfully');
                $this->setResponseData([$hotproduct]);
                return $this->toResponse();
            }else{
                $this->setMessage('No result Found');
                $this->setErrors(['error'=>['No Result Found']]);
                 $this->setStatus(404);
                return $this->toResponse();
            }
        }else{
                $this->setMessage('User not found');
                $this->setErrors(['error'=>['User not found']]);
                 $this->setStatus(404);
                return $this->toResponse();
         }
    }

    public function showPaymentForm(){
        return view('payment/showform');
    }

}
