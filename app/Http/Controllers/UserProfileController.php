<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserProfileService;
use App\Http\Requests\ProfileRequest;

class UserProfileController extends Controller
{
	
     /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct(UserProfileService $profileService)
    {
        $this->profileService = $profileService;
    }
    /**
     *To Update a User profile.
     *
     * @param array request
     * @return json response
     */
    public function updateProfile(ProfileRequest $request)
    {
      
        $updateUser =  $this->profileService->updateProfile($request);
        if($updateUser['status'] == true){
            $this->setMessage($updateUser['message']);
            $this->setResponseData([$updateUser['message']]);
            return $this->toResponse();
        }else{
            $this->setMessage($updateUser['message']);
            $this->setErrors(['error'=>[$updateUser['message']]]);

            return $this->toResponse();
        }
    }

     /**
     *To get a User profile.
     *
     * @param array request
     * @return json response
     */
      public function getUser(Request $request)
    {
        $getUser =  $this->profileService->getUser($request);
        if($getUser != false && $getUser != null){
            $this->setMessage('User get successfully');
            $this->setResponseData([$getUser]);
            return $this->toResponse();
        }else{
            $this->setMessage('User not found');
            $this->setErrors(['error'=>['User not found']]);
            $this->setStatus(404);
            return $this->toResponse();
        }
    }


}
