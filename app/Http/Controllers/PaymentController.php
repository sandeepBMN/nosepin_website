<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrderProducts;
use Auth;
use App\Services\PaymentService;
use Modules\Admin\Services\SettingService;
use App\Http\Requests\OrderRequest;


class PaymentController extends Controller
{
	 public $paymentService = '';

	/**
        * Create a new controller instance.
        *
        * @return void
    */
    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }
    
    /**
     *To save a order.
     *
     * @param array request
     * @return json response
     */
    public function saveOrder(Request $request)
    {
        $order =  $this->paymentService->saveOrder($request);

        if($order['status'] == true){
            $this->setMessage($order['message']);
            $this->setResponseData([$order['message']]);
            return $this->toResponse();
        }else{
            $this->setMessage($order['message']);
            $this->setErrors(['error'=>[$order['message']]]);
            return $this->toResponse();
        }
    }

     /**
        * To get all order.
        *
        * @return void
    */
     public function listOrder(int $page)
    {
        $listOrder = $this->paymentService->listOrder($page); 
        if($listOrder['status'] ==true)
        {
            $this->setMessage($listOrder['message']);
            $this->setResponseData([$listOrder['order']]);
            return $this->toResponse();
        }else{
            $this->setMessage($listOrder['message']);
            $this->setErrors(['error'=>[$listOrder['message']]]);
             $this->setStatus(404);
            return $this->toResponse();
        }
    }
    /**
        * To get Gold Price.
        *
        * @return void
    */
    public  function getGoldPrice()
    {
       $goldprice = SettingService::getGoldPrice();
       if(Auth::user())
       {
           if($goldprice != '')
            {
                $this->setMessage('Gold Price get Successfuly');
                $this->setResponseData([$goldprice]);
                return $this->toResponse();
            }else{
                $this->setMessage('Result not found');
                $this->setResponseData([$goldprice]);
                return $this->toResponse();
            }
        }else{
                $this->setMessage('User Not Found');
                $this->setErrors(['error'=>['User Not Found']]);
                 $this->setStatus(404);
                return $this->toResponse();
        }
    }

    public function getOrderProducts($id){

        $orderproducts = $this->paymentService->getOrderProducts($id); 
        if($orderproducts['status'] == true)
        {
            $this->setMessage($orderproducts['message']);
            $this->setResponseData([$orderproducts['order']]);
            return $this->toResponse();
        }else{
            $this->setMessage($orderproducts['message']);
            $this->setErrors(['error'=>[$orderproducts['message']]]);
             $this->setStatus($orderproducts['type']);
            return $this->toResponse();
        }
   }
   public function showPaymentForm(){
        return view('payment/showform');
   }

   public function paymentSuccess(Request $request)
   {
      $data = $request->all();
      if($data['status'] == 'success')
      {
       $changeStatus =  $this->paymentService->updateOrderPaymentStatus($data);
        $savetans = $this->paymentService->saveTransaction($data);
      }
    return view('payment/success');
   }
   public function paymentFail(Request $request)
   {
    if($data['status'] != 'success')
      {
       $changeStatus =  $this->paymentService->updateOrderPaymentStatus($data);
      }
         return view('payment/fail');
   }
}
