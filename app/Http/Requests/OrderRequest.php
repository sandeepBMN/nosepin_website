<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Traits\ResponseTrait;
use App\Models\User;
use Auth;

class OrderRequest extends FormRequest
{
     use ResponseTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name'  => 'required|string|max:100',
            'quantity'      => 'required| integer| max:100',
            'weight'        => 'required|numeric',
            'carat'         => 'required|numeric',
            'labour'         => 'required|numeric',
            'gst'            => 'required|string',
            'amount_before_tax' => 'required|numeric',
            'amount_after_tax' => 'required|numeric',
        ];
    }

     /*
    * To show validation error that apply to the request.
    *
    **/
    public function failedValidation(Validator $validator){  
        $errors = (new ValidationException($validator))->errors();
        $this->setErrors($errors);
        $this->setMessage('Some error occur');
        throw new HttpResponseException($this->toResponse());
    }
}
