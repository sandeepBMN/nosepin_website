<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Traits\ResponseTrait;
use App\Models\User;
use Auth;

class ProfileRequest extends FormRequest
{
    use ResponseTrait;


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|regex : /^[a-zA-Z ]*$/ |max:100',
            // 'mobile'    => 'required|regex:/^[0-9]{10,12}$/',
            'email'     => 'required|email|max:255|unique:users,email,'. auth()->user()->id.',id',
            // 'password'  => 'required|string|min:6|confirmed',
            'gender'   => 'required'
        ];
    }

      /*
    * To show validation error that apply to the request.
    *
    **/
    public function failedValidation(Validator $validator){  
        $errors = (new ValidationException($validator))->errors();
        $this->setErrors($errors);
        $this->setMessage('Some error occur');
        throw new HttpResponseException($this->toResponse());
    }
}
