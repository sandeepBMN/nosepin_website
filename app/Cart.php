<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
	protected $table = 'carts';
    protected $fillable = ['purity', 'quantity', 'product_id','user_id','price'];
    public $timestamps = true;

    public function product(){
        return $this->belongsTo('Modules\Admin\Entities\Product','product_id','id');
    }
    public function attachments()
    {
        return $this->hasMany('Modules\Admin\Entities\ProductAttachment', 'product_id','id');
    }
}
