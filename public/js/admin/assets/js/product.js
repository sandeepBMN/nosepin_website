
	 function getSubcategory(ele)
	{
	    var catId = jQuery(ele).children(':selected').data('id');
	    console.log(catId);
	    // var sub_id= $('#sub_id').val();
	    jQuery.ajax({
	        url: "/admin/getsubcategories/"+catId,
	        type: "GET",
	        dataType: "json",
	        success: function(data) 
	        {
	        	// jQuery('.subcategory').empty();
	        	 if(data){
		            jQuery.each(data,function(index,subcatObj){
		            	console.log(subcatObj.name);
		            	jQuery('.appendsubcat').css('display','block');
			            jQuery('.subcategory').append('<select name="subcategory" id="subcat" class=" form-control"><option value="">Select</option><option value="'+subcatObj.id+'" data-id="'+subcatObj.id+'">'+subcatObj.name+'</option>');
			        });
	        	 	
	        	 }
                       
	        }
	    });
	}
jQuery(document).ready(function() {
    var max_fields      = 5; //maximum input boxes allowed
    var field         = jQuery("#extend"); //Fields wrapper
    var add_button      = jQuery(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    jQuery(add_button).click(function(e){

     //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            jQuery(field).append('<div class="input-group form-group"><input type="number" name="purity[]" class="form-control"/>'
                +'  <div class="input-group-btn">'
                +'<button class="btn btn-info remove_field"><i class="fa fa-minus"></i></button>'
                +'</div></div>'); //add input box
        }
    });

    jQuery(field).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
         jQuery(this).parent('div').parent('div').remove(); x--;
    })
});


    var count ='';
    jQuery("#prod_image").change(function(e){

        // jQuery.each(e.target.files, function(index, value){
        //     //Add your condition for allowing only specific file
        //     var fileReader = new FileReader(); 

        //     fileReader.readAsDataURL(value);
           
        //     fileArray.push(fileReader);
        // });
        jQuery('.appended-div').html('');
        readimageURL(this);
    });
     function readimageURL(input) {
               
        count = document.getElementById("prod_image").files.length;
            console.log(count);   
        if (count > 0 && count < 4) {
            
            var total_file = document.getElementById("prod_image").files.length;

             for(var i=0;i<total_file;i++)
            {      
                    jQuery('.append-img-edit').css('display','block');
                    jQuery('.append-img-edit').find('.appended-div').append('<div id="preview_img_'+i+'" class="preview-img-edit">'
                        +'<img src="'+URL.createObjectURL(event.target.files[i])+'" width="120">'
                        +'</div>');
            }
        }else{
             jQuery('.append-img-edit').css('display','block');
              jQuery('.append-img-edit').find('.appended-div').html('Three files are allowed').css("color", "red");
        }
    }
    // for edit product
    	var fcount = '';
    	var fileArray =[];
    	var fileindex = '';
      jQuery("#edit_prod_image").change(function(e){
      	 jQuery.each(e.target.files, function(index, value){
            //Add your condition for allowing only specific file
            var fileReader = new FileReader(); 

            fileReader.readAsDataURL(value);
           
            fileArray.push(fileReader);
        });
        readEditimageURL(fileArray);

    });
      function readEditimageURL(input) {
      	console.log(input);
        fcount =    jQuery('#fcount').val(); 
        console.log(fcount);
        // fileindex = jQuery('.edit_product_image').attr('data-id');

        // nextindex= parseInt(fileindex) + 1 ;
        var filecount = document.getElementById("edit_prod_image").files.length;
            fcount = parseInt(fcount)+parseInt(filecount);
          
        if (fcount > 0 && fcount < 4) {
            
            var total_file = document.getElementById("edit_prod_image").files.length;

             for(var i=0;i<total_file;i++)
            {      
                  
                    jQuery('.append-img-edit').find('.appended-div').append('<div id="preview_img_'+i+'" class="preview-img-edit">'
                        +'<img src="'+URL.createObjectURL(event.target.files[i])+'" width="120">'
                        +'<div class="overlay">'
                    	+'<a href="javascript:void(0)" class="edit_product_image" data-id=""><i class="fa fa-trash fa-2x icon "></i></a>'
                    	+'</div></div></div>');
                  
            }
        }else{
             jQuery('.append-img-edit').css('display','block');
             jQuery('.append-img-edit').find('.appended-div').html('Three files are allowed').css("color", "red");
        }
    }
    jQuery(document).on('click','.edit_product_image',function(e){
    	e.preventDefault();
   		var id = 		jQuery(this).attr('data-id');
        if(id != '')
        {
             Swal.fire({
                title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
             })
            .then(function(isConfirm){
              console.log(isConfirm);
              if (Object.keys(isConfirm)[0] == "dismiss") {
                 swal("Cancelled", "Your imaginary file is safe :)", "error");
              } else {
                  jQuery.ajax({
                      type: 'get',
                      url: '/admin/productattach/delete',
                      data: {
                          'id': id
                      },
                       success: function (data) {
                            if(data ==true)
                            {
                              swal("Done!","It was succesfully deleted!","success");    
                              jQuery(this).parent('div').parent('.preview-img-edit').remove();
                            }
                        }
                  });   
              } 
            })  
            
        }else{
            jQuery(this).parent('div').parent('.preview-img-edit').remove();
        }
	});

   