 function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                jQuery('.append-img').css('display','block');
                jQuery('#preview-img').find('img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    jQuery("#cat_image").change(function(){
        readURL(this);
    });

// <--------- for edit----

 function readimageURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {

                jQuery('.append-img-edit').append('<div class="offset-3 col-md-9 appended-div">'
                    +'<div id="preview-img-edit" class="preview-img-edit">'
                    +'<img src="'+ e.target.result+'" width="120">'
                    +'<div class="overlay">'
                    +'<a href="javascript:void(0)" class="category_delete_image" data-id=""><i class="fa fa-trash fa-2x icon "></i></a>'
                    +'</div></div></div>');
                // jQuery('#preview-img-edit').find('img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    jQuery("#edit_cat_image").change(function(){
        readimageURL(this);
    });

 jQuery(document).on('click','.category_delete_image',function(e){
   jQuery(this).parents('.appended-div').remove();
   jQuery('#edit_cat_image').val('');
});