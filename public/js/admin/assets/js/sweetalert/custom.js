jQuery(function(){
	
	if(window.success){
		swal("Success", window.success, "success")
	}
	

});

// swal for delete 

jQuery( document ).on('click','.confirm_delete',function( event ) {
	event.preventDefault();
	$link = jQuery(this);
	Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				window.location.href = $link.attr('href');
		  }
		  
		})
		
	});

